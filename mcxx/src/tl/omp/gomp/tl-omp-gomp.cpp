/*--------------------------------------------------------------------
  (C) Copyright 2006-2014 Barcelona Supercomputing Center
                          Centro Nacional de Supercomputacion
  
  This file is part of Mercurium C/C++ source-to-source compiler.
  
  See AUTHORS file in the top level directory for information
  regarding developers and contributors.
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.
  
  Mercurium C/C++ source-to-source compiler is distributed in the hope
  that it will be useful, but WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the GNU Lesser General Public License for more
  details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with Mercurium C/C++ source-to-source compiler; if
  not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.
--------------------------------------------------------------------*/

#include <inttypes.h>

#include "tl-builtin.hpp"
#include "tl-omp-gomp.hpp"
#include "tl-lowering-visitor.hpp"
#include "tl-lowering-utils.hpp"
#include "tl-symbol-utils.hpp"

namespace TL { namespace GOMP {

    Lowering::Lowering()
        : _openmp_dry_run(""), _tdg_enabled_str(""), _tdg_enabled(false),
          _preallocate_tasks_str(""), _preallocate_tasks_enabled(false),
          _lazy_task_creation_str(""), _lazy_task_creation_enabled(false),
          _tdg_width_str(""), _tdg_width(0)
    {
        set_phase_name("GOMP lowering");
        set_phase_description("This phase lowers from Mercurium parallel IR into calls to the "
                "GNU Offloading and Multi Processing Library");

        register_parameter("omp_dry_run",
                "Disables OpenMP transformation",
                _openmp_dry_run,
                "0");

        register_parameter("tdg_enabled",
                    "If set to '1' enables expanded-tdg lowering, otherwise it is disabled",
                    _tdg_enabled_str,
                    "0").connect(std::bind(&Lowering::set_tdg, this, std::placeholders::_1));

        register_parameter("prealloc_tasks_enabled",
                    "If set to '1' enables preallocating runtime tasks from the compiler, otherwise it is disabled",
                    _preallocate_tasks_str,
                    "0").connect(std::bind(&Lowering::set_preallocate_tasks, this, std::placeholders::_1));

        register_parameter("lazy_task_creation_enabled",
                    "If set to '1' enables storing statically known task data from the compiler to enable lazy task creation at runtime, otherwise it is disabled",
                    _lazy_task_creation_str,
                    "0").connect(std::bind(&Lowering::set_lazy_task_creation, this, std::placeholders::_1));

        register_parameter("tdg_width",
                    "Defines the maximum parallelism (in number of tasks) of the application. Otherwise, the number of nodes of the TDG is considered.",
                    _tdg_width_str,
                    "0").connect(std::bind(&Lowering::set_tdg_width, this, std::placeholders::_1));
    }

    void Lowering::set_tdg(const std::string& tdg_enabled_str)
    {
        if (tdg_enabled_str == "1")
            _tdg_enabled = true;
    }

    bool Lowering::is_tdg_enabled() const
    {
        return _tdg_enabled;
    }

    void Lowering::set_preallocate_tasks(const std::string& preallocate_tasks_str)
    {
        if (preallocate_tasks_str == "1")
            _preallocate_tasks_enabled = true;
    }

    bool Lowering::is_preallocate_tasks_enabled() const
    {
        return _preallocate_tasks_enabled;
    }

    void Lowering::set_lazy_task_creation(const std::string& lazy_task_creation_str)
    {
        if (lazy_task_creation_str == "1")
            _lazy_task_creation_enabled = true;
    }

    bool Lowering::is_lazy_task_creation_enabled() const
    {
        return _lazy_task_creation_enabled;
    }

    void Lowering::set_tdg_width(const std::string& tdg_width_str)
    {
        uintmax_t width = strtoumax(tdg_width_str.c_str(), NULL, 10);
        ERROR_CONDITION(width == UINTMAX_MAX && errno == ERANGE,
                        "The parameter expressed in variable tdg_width cannot be converted to a number.", 0);
        _tdg_width = width;
    }

    int Lowering::get_tdg_width() const
    {
        return _tdg_width;
    }

    void Lowering::pre_run(DTO& dto)
    {
    }

    void Lowering::run(DTO& dto)
    {
        if (_openmp_dry_run != "0")
        {
            std::cerr << "Not running GOMP phase (by request)" << std::endl;
            return;
        }

        std::cerr << "GOMP phase" << std::endl;

        // Do some checks for consistent code generation
        ERROR_CONDITION ((!_tdg_enabled && (_preallocate_tasks_enabled || _lazy_task_creation_enabled || _tdg_width != 0)),
                         "For using task preallocation (--prealloc-tasks) and lazy task creation (--lazy-task-creation), "
                         "and setting TDG width (--variable=\"tdg_width\":X), tdg must be enabled. Add flag --tdg.\n", 0);

        // Compute the width of the graph in case it has not been set
        if (_preallocate_tasks_enabled && _tdg_width == 0)
        {
            ObjectList<std::string> dto_keys = dto.get_keys();
            ERROR_CONDITION(!dto_keys.contains("tdg_width"),
                            "Variable tdg_width is not set, and dto does not contain tdg_width key. ", 0);
            _tdg_width = *std::static_pointer_cast<Integer>(dto["tdg_width"]);
            WARNING_MESSAGE("TDG width not set (--variable=\"tdg_width\"). Using the maximum number of nodes of a TDG: '%d'.\n", _tdg_width);
        }

        Nodecl::NodeclBase n = *std::static_pointer_cast<Nodecl::NodeclBase>(dto["nodecl"]);
        LoweringVisitor lowering_visitor(this);
        lowering_visitor.walk(n);
    }

    void Lowering::phase_cleanup(DTO& data_flow)
    {
    }
} }


EXPORT_PHASE(TL::GOMP::Lowering);
