/*--------------------------------------------------------------------
  (C) Copyright 2006-2013 Barcelona Supercomputing Center
                          Centro Nacional de Supercomputacion
  
  This file is part of Mercurium C/C++ source-to-source compiler.
  
  See AUTHORS file in the top level directory for information
  regarding developers and contributors.
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.
  
  Mercurium C/C++ source-to-source compiler is distributed in the hope
  that it will be useful, but WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the GNU Lesser General Public License for more
  details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with Mercurium C/C++ source-to-source compiler; if
  not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.
--------------------------------------------------------------------*/

#ifndef TL_OMP_GOMP_HPP
#define TL_OMP_GOMP_HPP

#include "tl-compilerphase.hpp"
#include "tl-nodecl.hpp"

namespace TL { namespace GOMP {

    class Lowering : public TL::CompilerPhase
    {
        public:
            Lowering();

            virtual void phase_cleanup(DTO& data_flow);

            virtual void run(DTO& dto);
            virtual void pre_run(DTO& dto);

            bool is_tdg_enabled() const;
            bool is_preallocate_tasks_enabled() const;
            bool is_lazy_task_creation_enabled() const;
            int get_tdg_width() const;

        private:
            std::string _openmp_dry_run;

            std::string _tdg_enabled_str;
            bool _tdg_enabled;
            std::string _preallocate_tasks_str;
            bool _preallocate_tasks_enabled;
            std::string _lazy_task_creation_str;
            bool _lazy_task_creation_enabled;
            std::string _tdg_width_str;
            int _tdg_width;

            void set_tdg(const std::string& tdg_enabled_str);
            void set_preallocate_tasks(const std::string& str);
            void set_lazy_task_creation(const std::string& str);
            void set_tdg_width(const std::string& tdg_with_str);
    };

} }

#endif // TL_OMP_GOMP_HPP
