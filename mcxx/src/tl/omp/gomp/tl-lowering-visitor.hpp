/*--------------------------------------------------------------------
  (C) Copyright 2006-2014 Barcelona Supercomputing Center
                          Centro Nacional de Supercomputacion
  
  This file is part of Mercurium C/C++ source-to-source compiler.
  
  See AUTHORS file in the top level directory for information
  regarding developers and contributors.
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.
  
  Mercurium C/C++ source-to-source compiler is distributed in the hope
  that it will be useful, but WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the GNU Lesser General Public License for more
  details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with Mercurium C/C++ source-to-source compiler; if
  not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.
--------------------------------------------------------------------*/

#include "tl-omp-gomp.hpp"
#include "tl-nodecl-visitor.hpp"
#include "tl-nodecl-utils.hpp"
#include "tl-omp-core.hpp"

#include <set>
#include <stdio.h>

namespace TL { namespace GOMP {

struct TaskStaticData {
    Symbol _func_ptr;
    Symbol _cpy_fn;
    long _arg_size;
    long _arg_align;
    int _if_clause;
    unsigned _flags;

    TaskStaticData(Symbol func_ptr, Symbol cpy_fn,
                     long arg_size, long arg_align,
                     int if_clause, unsigned flags);
};

class LoweringVisitor : public Nodecl::ExhaustiveVisitor<void>
{
    public:
        LoweringVisitor(Lowering*);
        ~LoweringVisitor();

        virtual void visit(const Nodecl::ForStatement& statement);
        virtual void visit(const Nodecl::OpenMP::Atomic& construct);
        virtual void visit(const Nodecl::OpenMP::BarrierFull& construct);
        virtual void visit(const Nodecl::OpenMP::Critical& construct);
        virtual void visit(const Nodecl::OpenMP::FlushMemory& construct);
        virtual void visit(const Nodecl::OpenMP::For& construct);
        virtual void visit(const Nodecl::OpenMP::Master& construct);
        virtual void visit(const Nodecl::OpenMP::Parallel& construct);
        virtual void visit(const Nodecl::OpenMP::Sections& construct);
        virtual void visit(const Nodecl::OpenMP::Single& construct);
        virtual void visit(const Nodecl::OpenMP::Workshare& construct);
        virtual void visit(const Nodecl::OmpSs::TargetDeclaration& construct);
        virtual void visit(const Nodecl::OpenMP::Task& construct);
        virtual void visit(const Nodecl::OmpSs::TaskCall& construct);
        virtual void visit(const Nodecl::OmpSs::TaskExpression& task_expr);
        virtual void visit(const Nodecl::OpenMP::Taskwait& construct);
        virtual void visit(const Nodecl::OpenMP::Taskgraph& construct);

    private:

        void lower_for(const Nodecl::OpenMP::For& construct, const Nodecl::NodeclBase& prependix,
                const Nodecl::NodeclBase& appendix);

        Nodecl::NodeclBase emit_barrier(const Nodecl::NodeclBase& construct);

        Lowering* _lowering;

        std::map<int, struct TaskStaticData> _task_static_data_list;

         unsigned _next_loop_iterator;
         std::stack<unsigned> _loop_iterators;

         bool _is_taskgraph_static;
};

} }
