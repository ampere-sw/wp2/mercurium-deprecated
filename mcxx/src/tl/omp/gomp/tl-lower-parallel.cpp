/*--------------------------------------------------------------------
  (C) Copyright 2006-2014 Barcelona Supercomputing Center
                          Centro Nacional de Supercomputacion
  
  This file is part of Mercurium C/C++ source-to-source compiler.
  
  See AUTHORS file in the top level directory for information
  regarding developers and contributors.
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.
  
  Mercurium C/C++ source-to-source compiler is distributed in the hope
  that it will be useful, but WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the GNU Lesser General Public License for more
  details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with Mercurium C/C++ source-to-source compiler; if
  not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.
--------------------------------------------------------------------*/

#include "cxx-cexpr.h"

#include "tl-counters.hpp"
#include "tl-omp-lowering-directive-environment.hpp"
#include "tl-lowering-visitor.hpp"
#include "tl-lowering-utils.hpp"
#include "tl-symbol-utils.hpp"
#include "tl-lower-reductions.hpp"


namespace {
    void add_field_to_class(
            TL::Symbol class_symbol,
            TL::Scope class_scope,
            const std::string& name,
            TL::Type field_type,
            const locus_t* class_locus)
    {
        TL::Type class_type = class_symbol.get_user_defined_type();

        std::string orig_field_name = name;

        TL::Symbol field = class_scope.new_symbol(orig_field_name);
        field.get_internal_symbol()->kind = SK_VARIABLE;
        symbol_entity_specs_set_is_user_declared(field.get_internal_symbol(), 1);

        field.set_type(field_type);
        field.get_internal_symbol()->locus = class_locus;

        symbol_entity_specs_set_is_member(field.get_internal_symbol(), 1);
        symbol_entity_specs_set_class_type(field.get_internal_symbol(),
                class_type.get_internal_type());
        symbol_entity_specs_set_access(field.get_internal_symbol(), AS_PUBLIC);
        class_type_add_member(
                class_type.get_internal_type(),
                field.get_internal_symbol(),
                class_scope.get_decl_context(),
                /* is_definition */ 1);
    }

    // (void (*)(void *))
    TL::Type create_outlined_function_type(TL::Scope sc)
    {
        TL::Symbol func_sym = SymbolUtils::new_function_symbol(
            /*scope*/ sc,
            /*function name*/ "tmp_outlined_func_sym",
            /*return type*/ TL::Type::get_void_type(),
            /*parameter names*/ TL::ObjectList<std::string>(1, "ol_args"),
            /*parameter types*/ TL::ObjectList<TL::Type>(1, TL::Type::get_void_type().get_pointer_to()));
        return func_sym.get_type().get_pointer_to();
    }

    TL::Type create_copy_function_type(TL::Scope sc)
    {
        TL::ObjectList<std::string> parameter_names;
        parameter_names.append("args");
        parameter_names.append("data");
        TL::ObjectList<TL::Type> parameter_types;
        parameter_types.append(TL::Type::get_void_type().get_pointer_to());
        parameter_types.append(TL::Type::get_void_type().get_pointer_to());
        TL::Symbol func_sym = SymbolUtils::new_function_symbol(
            /*scope*/ sc,
            /*function name*/ "tmp_copy_func_sym",
            /*return type*/ TL::Type::get_void_type(),
            parameter_names,
            parameter_types);
        return func_sym.get_type().get_pointer_to();
    }

    // struct TaskPreallocData {
    //     (void (*)(void *)) _func_ptr;
    //     (void (*)(void *)) _cpy_fn;
    //     long _arg_size;
    //     long _arg_align;
    //     int _if_clause;
    //     unsigned _flags;
    // }
    TL::Type create_prealloc_arg_type(std::string class_name, TL::Scope sc, const locus_t* class_locus)
    {
        // Create the symbol
        TL::Symbol class_symbol = sc.new_symbol(class_name);
        class_symbol.get_internal_symbol()->kind = SK_CLASS;
        type_t* class_type = get_new_class_type(sc.get_decl_context(), TT_STRUCT);
        symbol_entity_specs_set_is_user_declared(class_symbol.get_internal_symbol(), 1);
        const decl_context_t* class_context = new_class_context(class_symbol.get_scope().get_decl_context(),
                class_symbol.get_internal_symbol());
        TL::Scope class_scope(class_context);
        class_type_set_inner_context(class_type, class_context);
        class_symbol.get_internal_symbol()->type_information = class_type;

        // Add fields
        add_field_to_class(class_symbol,
                class_scope,
                "_func_ptr",
                create_outlined_function_type(sc),
                class_locus);
        add_field_to_class(class_symbol,
                class_scope,
                "_cpy_fn",
                create_copy_function_type(sc),
                class_locus);
        add_field_to_class(class_symbol,
                class_scope,
                "_arg_size",
                TL::Type::get_long_int_type(),
                class_locus);
        add_field_to_class(class_symbol,
                class_scope,
                "_arg_align",
                TL::Type::get_long_int_type(),
                class_locus);
        add_field_to_class(class_symbol,
                class_scope,
                "_if_clause",
                TL::Type::get_int_type(),
                class_locus);
        add_field_to_class(class_symbol,
                class_scope,
                "_flags",
                TL::Type::get_unsigned_int_type(),
                class_locus);

        // Finish the class type
        nodecl_t nodecl_output = nodecl_null();
        finish_class_type(class_type,
                ::get_user_defined_type(class_symbol.get_internal_symbol()),
                sc.get_decl_context(),
                class_locus,
                &nodecl_output);
        set_is_complete_type(class_type, /* is_complete */ 1);
        set_is_complete_type(get_actual_class_type(class_type), /* is_complete */ 1);

        return TL::Type(::get_user_defined_type(class_symbol.get_internal_symbol())).get_pointer_to();
    }
}

namespace TL { namespace GOMP {

void LoweringVisitor::visit(const Nodecl::OpenMP::Parallel& construct)
{
    Nodecl::NodeclBase statements = construct.get_statements();
    walk(statements);
    statements = construct.get_statements(); // Should not be necessary

    Nodecl::NodeclBase num_threads = construct.get_num_replicas();

    TL::OpenMP::Lowering::DirectiveEnvironment env(construct.get_environment());

    // Get variables in data-sharing clauses
    TL::ObjectList<TL::Symbol> firstprivate_symbols = env.captured_value;
    TL::ObjectList<TL::Symbol> private_symbols = env.private_;
    private_symbols.insert(firstprivate_symbols);
    TL::ObjectList<TL::Symbol> shared_symbols = env.shared;

    // Get reduction symbols
    TL::ObjectList<TL::OpenMP::Lowering::ReductionItem> reduction_items = env.reduction;
    TL::ObjectList<TL::Symbol> reduction_symbols;
    if (!reduction_items.empty())
    {
        reduction_symbols =
            reduction_items.map<TL::Symbol>(&TL::OpenMP::Lowering::ReductionItem::get_symbol);
    }

    // Gather all symbols
    TL::ObjectList<TL::Symbol> all_symbols_passed; // Set of all symbols passed in the outline
    all_symbols_passed.insert(private_symbols);
    all_symbols_passed.insert(shared_symbols);
    all_symbols_passed.insert(reduction_symbols);

    TL::Symbol enclosing_function = Nodecl::Utils::get_enclosing_function(construct);
    std::string outline_function_name;
    {
        TL::Counter &outline_num = TL::CounterManager::get_counter("gomp-omp-outline");
        std::stringstream ss;
        ss << "_ol_" << enclosing_function.get_name() << "_" << (int)outline_num;
        outline_function_name = ss.str();
        outline_num++;
    }

    TL::Scope current_scope = construct.retrieve_context();

    TL::Type outline_struct = GOMP::create_outline_struct(
            all_symbols_passed,
            firstprivate_symbols,
            enclosing_function,
            construct.get_locus());

    CXX_LANGUAGE()
    {
        Nodecl::Utils::prepend_to_enclosing_top_level_location(
                construct,
                Nodecl::CxxDef::make(
                    /* context */ Nodecl::NodeclBase::null(),
                    outline_struct.get_symbol()
                    )
                );
    }

    std::string ol_data_name;
    {
        TL::Counter &outline_num = TL::CounterManager::get_counter("gomp-omp-outline-data");

        std::stringstream ss;
        ss << "ol_data_" << (int)outline_num;
        ol_data_name = ss.str();

        outline_num++;
    }

    TL::Symbol outline_data = current_scope.new_symbol(ol_data_name);
    outline_data.get_internal_symbol()->kind = SK_VARIABLE;
    outline_data.get_internal_symbol()->type_information = outline_struct.get_internal_type();
    symbol_entity_specs_set_is_user_declared(outline_data.get_internal_symbol(), 1);

    TL::ObjectList<std::string> parameter_names;
    TL::ObjectList<TL::Type> parameter_types;

    parameter_names.append(ol_data_name);
    parameter_types.append(outline_struct.get_pointer_to());

    TL::Symbol outline_function = SymbolUtils::new_function_symbol(
            enclosing_function,
            outline_function_name,
            TL::Type::get_void_type(),
            parameter_names,
            parameter_types);

    Nodecl::NodeclBase outline_function_code, outline_function_stmt;
    Nodecl::List reduction_code_list;
    SymbolUtils::build_empty_body_for_function(outline_function,
            outline_function_code,
            outline_function_stmt);

    Nodecl::Utils::SimpleSymbolMap symbol_map;

    TL::Scope block_scope = outline_function_stmt.retrieve_context();

    TL::Symbol ol_data_in_outline = block_scope.get_symbol_from_name(ol_data_name);
    ERROR_CONDITION(!ol_data_in_outline.is_valid(), "Invalid symbol", 0);

    for (TL::ObjectList<TL::Symbol>::iterator it = all_symbols_passed.begin();
            it != all_symbols_passed.end();
            it++)
    {
        TL::Symbol new_shared_sym = block_scope.new_symbol(it->get_name());
        new_shared_sym.get_internal_symbol()->kind = SK_VARIABLE;

        // Computing the type of this new variable
        new_shared_sym.get_internal_symbol()->type_information = it->get_internal_symbol()->type_information;
        if (!firstprivate_symbols.contains(*it))
        {
            // The type of a fp variable isn't an lvalue reference
            new_shared_sym.get_internal_symbol()->type_information =
                lvalue_ref(it->get_internal_symbol()->type_information);
        }

        // We may need to rewrite the type
        new_shared_sym.get_internal_symbol()->type_information =
            ::type_deep_copy(
                    new_shared_sym.get_internal_symbol()->type_information,
                    outline_function_stmt.retrieve_context().get_decl_context(),
                    symbol_map.get_symbol_map());


        symbol_entity_specs_set_is_user_declared(
                new_shared_sym.get_internal_symbol(),
                1);

        Source init_ref_src;
        if (firstprivate_symbols.contains(*it))
            init_ref_src << as_symbol(ol_data_in_outline) << "->" << it->get_name();
        else
        {
            Source extra_cast;
            // VLAs are represented as void * in our arguments structure
            if (is_vla_symbol(*it))
                extra_cast << "("<< as_type(it->get_type().get_pointer_to()) << ")";

            init_ref_src << "*(" << extra_cast << as_symbol(ol_data_in_outline) << "->" << it->get_name() << ")";
        }

        Nodecl::NodeclBase init_ref =
            init_ref_src.parse_expression(block_scope);
        new_shared_sym.set_value(init_ref);

        symbol_map.add_map(*it, new_shared_sym);

        CXX_LANGUAGE()
        {
            outline_function_stmt.prepend_sibling(
                    Nodecl::CxxDef::make(
                        /* context */ Nodecl::NodeclBase::null(),
                        new_shared_sym));
        }
    }

    for (TL::ObjectList<TL::Symbol>::iterator it = private_symbols.begin();
            it != private_symbols.end();
            it++)
    {
        // They are to be found in the struct
        if (firstprivate_symbols.contains(*it))
            continue;

        TL::Symbol new_private_sym = GOMP::new_private_symbol(*it, block_scope);

        new_private_sym.get_internal_symbol()->type_information = ::type_deep_copy(
                new_private_sym.get_internal_symbol()->type_information,
                outline_function_stmt.retrieve_context().get_decl_context(),
                symbol_map.get_symbol_map());

        if (firstprivate_symbols.contains(*it))
        {
            if (!new_private_sym.get_type().is_array())
            {
                new_private_sym.set_value(
                        symbol_map.map(*it).make_nodecl(/* set_ref_type */ true)
                        );
            }
            else
            {
                Source init_array;

                // FIXME - Use assignment instead
                init_array
                    << "__builtin_memcpy(" << as_symbol(new_private_sym) << ","
                    <<                        as_symbol(symbol_map.map(*it))
                    <<                        ", sizeof(" << as_symbol(*it) << "));"
                    ;

                Nodecl::NodeclBase init_array_tree = init_array.parse_statement(outline_function_stmt);
                outline_function_stmt.prepend_sibling(init_array_tree);
            }
        }


        // Will override privates.
        // Do not move before the if (firstprivate_symbols.contains(*it))
        // or the __builtin_memcpy will not work
        symbol_map.add_map(*it, new_private_sym);

        CXX_LANGUAGE()
        {
            outline_function_stmt.prepend_sibling(
                    Nodecl::CxxDef::make(
                        /* context */ Nodecl::NodeclBase::null(),
                        new_private_sym));
        }
    }

    Nodecl::NodeclBase parallel_body = Nodecl::Utils::deep_copy(statements,
            outline_function_stmt,
            symbol_map);
    outline_function_stmt.prepend_sibling(parallel_body);
    Nodecl::Utils::prepend_to_enclosing_top_level_location(construct, outline_function_code);

    // Spawn threads
    Source num_threads_src;
    if (num_threads.is_null())
    {
        num_threads_src << "0";
    }
    else
    {
        num_threads_src << as_expression(num_threads.shallow_copy());
    }

    Source setup_data;

    CXX_LANGUAGE()
    {
        setup_data << as_statement(
                Nodecl::CxxDef::make(
                    /* context */ Nodecl::NodeclBase::null(),
                    outline_data))
            ;
    }

    for (TL::ObjectList<TL::Symbol>::iterator it = all_symbols_passed.begin();
            it != all_symbols_passed.end();
            it++)
    {
        if (firstprivate_symbols.contains(*it))
        {
            setup_data
                << as_symbol(outline_data) << "." << it->get_name() << " = " << as_symbol(*it) << ";"
                ;
        }
        else
        {
            setup_data
                << as_symbol(outline_data) << "." << it->get_name() << " = &" << as_symbol(*it) << ";"
                ;
        }
    }

    Source prealloc_data;
    if (_lowering->is_preallocate_tasks_enabled() || _lowering->is_lazy_task_creation_enabled())
    {
        // 1.- Create the data structure necessary for the static allocation
        // Create array where the preallocation data will be stored (une element per task construct)
        prealloc_data << "struct TaskStaticData task_static_data_table[" << _task_static_data_list.size() << "] = {\n";

        // Initialize all fields of each element
        for (std::map<int, TaskStaticData>::iterator it = _task_static_data_list.begin();
             it != _task_static_data_list.end(); )
        {
            prealloc_data << "   {._func_ptr = (void (*)(void *))" << as_symbol(it->second._func_ptr) << ","
                          << "._cpy_fn = (void (*)(void *, void *))" << (it->second._cpy_fn.is_valid() ? as_symbol(it->second._cpy_fn) : "0") << ", "
                          << "._arg_size = " << it->second._arg_size << ", "
                          << "._arg_align = " << it->second._arg_align << ", "
                          << "._if_clause = " << it->second._if_clause << ", "
                          << "._flags = " << it->second._flags
                          << "}";

            ++it;
            if (it != _task_static_data_list.end())
                prealloc_data << ",\n";
            else
                prealloc_data << "\n";
        }
        prealloc_data << "};\n";

        // 2.- Create the function (defined in the runtime) that will allocate the data
        if (_lowering->is_lazy_task_creation_enabled())
        {
            // Create the function declaration (make sure we only create the symbol once)
            //     void GOMP_prealloc_tasks_data(TaskStaticData* task_static_data_table, unsigned n_task_constructs);
            Scope global_sc = Scope::get_global_scope();
            std::string func_name = "GOMP_prealloc_tasks_data";
            TL::Symbol func_sym = global_sc.get_symbol_from_name(func_name);
            ERROR_CONDITION(func_sym.is_valid(),
                            "More than one parallel construct is not supported when preallocating tasks data. Aborting compilation.\n",
                            0);
            ObjectList<std::string> prealloc_tasks_data_parameter_names;
                prealloc_tasks_data_parameter_names.append("task_static_data");
                prealloc_tasks_data_parameter_names.append("n_task_constructs");
            ObjectList<Type> prealloc_tasks_data_parameter_types;
                prealloc_tasks_data_parameter_types.append(create_prealloc_arg_type("struct TaskStaticData",
                                                                                    global_sc, construct.get_locus()));
                prealloc_tasks_data_parameter_types.append(Type::get_unsigned_int_type());
            func_sym = SymbolUtils::new_function_symbol(
                    /*scope*/ global_sc,
                    /*function name*/ func_name,
                    /*return symbol name*/ "",
                    /*return type*/ TL::Type::get_void_type(),
                    /*parameter names*/ prealloc_tasks_data_parameter_names,
                    /*parameter types*/ prealloc_tasks_data_parameter_types);
                // Make it external
            symbol_entity_specs_set_is_extern(func_sym.get_internal_symbol(), 1);
            symbol_entity_specs_set_is_static(func_sym.get_internal_symbol(), 0);
            prealloc_data << "GOMP_prealloc_tasks_data(task_static_data_table, "
                            << _task_static_data_list.size() << ");\n";
        }
        else    // _lowering->is_preallocate_tasks_enabled()
        {
            // Create the function declaration (make sure we only create the symbol once)
            //     void GOMP_prealloc_tasks(TaskStaticData* task_static_data_table, unsigned n_task_constructs, unsigned max_concurrent_tasks);
            Scope global_sc = Scope::get_global_scope();
            std::string func_name = "GOMP_prealloc_tasks";
            TL::Symbol func_sym = global_sc.get_symbol_from_name(func_name);
            ERROR_CONDITION(func_sym.is_valid(),
                            "More than one parallel construct is not supported when preallocating tasks. Aborting compilation.\n",
                            0);
            ObjectList<std::string> prealloc_tasks_parameter_names;
                prealloc_tasks_parameter_names.append("task_static_data");
                prealloc_tasks_parameter_names.append("n_task_constructs");
                prealloc_tasks_parameter_names.append("max_concurrent_tasks");
            ObjectList<Type> prealloc_tasks_parameter_types;
                prealloc_tasks_parameter_types.append(create_prealloc_arg_type("struct TaskStaticData",
                                                                               global_sc, construct.get_locus()));
                prealloc_tasks_parameter_types.append(Type::get_unsigned_int_type());
                prealloc_tasks_parameter_types.append(Type::get_unsigned_int_type());
            func_sym = SymbolUtils::new_function_symbol(
                    /*scope*/ global_sc,
                    /*function name*/ func_name,
                    /*return symbol name*/ "",
                    /*return type*/ TL::Type::get_void_type(),
                    /*parameter names*/ prealloc_tasks_parameter_names,
                    /*parameter types*/ prealloc_tasks_parameter_types);
                // Make it external
            symbol_entity_specs_set_is_extern(func_sym.get_internal_symbol(), 1);
            symbol_entity_specs_set_is_static(func_sym.get_internal_symbol(), 0);
            prealloc_data << "GOMP_prealloc_tasks(task_static_data_table, "
                            << _task_static_data_list.size() << ", "
                            << _lowering->get_tdg_width() << ");\n";
        }
    }

    Source fork_call;
    fork_call
        << prealloc_data
        << setup_data
        << "GOMP_parallel_start((void(*)(void*))"
        <<     as_symbol(outline_function) << ", &" << as_symbol(outline_data) << ", " << num_threads_src << ");"
        << as_symbol(outline_function) << "(&" << as_symbol(outline_data) << ");"
        << "GOMP_parallel_end();"
        ;

    Nodecl::NodeclBase fork_call_tree = fork_call.parse_statement(construct);

    construct.replace(fork_call_tree);
}

} }
