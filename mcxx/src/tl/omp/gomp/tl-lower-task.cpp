/*--------------------------------------------------------------------
  (C) Copyright 2006-2014 Barcelona Supercomputing Center
                          Centro Nacional de Supercomputacion
  
  This file is part of Mercurium C/C++ source-to-source compiler.
  
  See AUTHORS file in the top level directory for information
  regarding developers and contributors.
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.
  
  Mercurium C/C++ source-to-source compiler is distributed in the hope
  that it will be useful, but WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the GNU Lesser General Public License for more
  details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with Mercurium C/C++ source-to-source compiler; if
  not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.
--------------------------------------------------------------------*/


#include "tl-counters.hpp"
#include "tl-omp-lowering-directive-environment.hpp"
#include "tl-lowering-visitor.hpp"
#include "tl-lowering-utils.hpp"
#include "tl-symbol-utils.hpp"
#include "tl-lower-reductions.hpp"
#include "cxx-diagnostic.h"


namespace {
    enum GOMP_task_flags
    {
        GOMP_TASK_UNTIED = 1,
        GOMP_TASK_FINAL = 2,
        GOMP_TASK_MERGEABLE = 4,
        GOMP_TASK_DEPEND = 8
    };
}

const std::string TASK_INSTANCE_ID_COUNTER("gomp.task_instance_id");

namespace TL { namespace GOMP {

void LoweringVisitor::visit(const Nodecl::OpenMP::Task& construct)
{
    // Loop iterators are only passed as argument to a task, when the task contains nested tasks
    // Since the lowering is performed from inside out, hence the task construct is transformed into a GOMP_task call,
    // we first check whether the task contains other tasks, and then we perform the lowering
    bool task_contains_tasks = Nodecl::Utils::nodecl_contains_nodecl_of_kind<Nodecl::OpenMP::Task>(construct.get_statements());

    Nodecl::NodeclBase statements = construct.get_statements();
    walk(statements);
    statements = construct.get_statements(); // Should not be necessary

    // Keep using the environment for clauses that are not managed in the DirectiveEnvironment class
    // (taskid and depcheck)
    Nodecl::List environment = construct.get_environment().as<Nodecl::List>();
    TL::OpenMP::Lowering::DirectiveEnvironment env(construct.get_environment());

    // Get variables in data-sharing clauses
    TL::ObjectList<TL::Symbol> firstprivate_symbols = env.captured_value;
    TL::ObjectList<TL::Symbol> private_symbols = env.private_;
    private_symbols.insert(firstprivate_symbols);
    TL::ObjectList<TL::Symbol> shared_symbols = env.shared;

    // Get reduction symbols
    TL::ObjectList<TL::OpenMP::Lowering::ReductionItem> reduction_items = env.reduction;
    TL::ObjectList<TL::Symbol> reduction_symbols;
    if (!reduction_items.empty())
    {
        reduction_symbols =
            reduction_items.map<TL::Symbol>(&TL::OpenMP::Lowering::ReductionItem::get_symbol);
    }

    // Gather all symbols
    TL::ObjectList<TL::Symbol> all_symbols_passed; // Set of all symbols passed in the outline
    all_symbols_passed.insert(private_symbols);
    all_symbols_passed.insert(shared_symbols);
    all_symbols_passed.insert(reduction_symbols);

    // Add iterators included to compute the task instance id, only if the current task contains nested tasks
    if (task_contains_tasks)
    {
        std::stack<unsigned> current_loop_iterators(_loop_iterators);
        while (!current_loop_iterators.empty())
        {
            unsigned current_id = current_loop_iterators.top();
            current_loop_iterators.pop();

            char gomp_iter_str[100];
            sprintf(gomp_iter_str, "_gomp_iter_%u", current_id);
            Symbol gomp_iter_sym = construct.retrieve_context().get_symbol_from_name(gomp_iter_str);
            ERROR_CONDITION(!gomp_iter_sym.is_valid(), "Symbol '%s' not found during task lowering.\n", gomp_iter_str);

            firstprivate_symbols.insert(gomp_iter_sym);
            private_symbols.insert(gomp_iter_sym);
            all_symbols_passed.insert(gomp_iter_sym);
        }
    }

    TL::Symbol enclosing_function = Nodecl::Utils::get_enclosing_function(construct);
    std::string outline_function_name;
    {
        TL::Counter &outline_num = TL::CounterManager::get_counter("gomp-omp-outline");
        std::stringstream ss;
        ss << "_ol_" << enclosing_function.get_name() << "_" << (int)outline_num;
        outline_function_name = ss.str();
        outline_num++;
    }

    TL::Scope current_scope = construct.retrieve_context();

    TL::Type outline_struct = GOMP::create_outline_struct(
            all_symbols_passed,
            firstprivate_symbols,
            enclosing_function,
            construct.get_locus());

    CXX_LANGUAGE()
    {
        Nodecl::Utils::prepend_to_enclosing_top_level_location(
                construct,
                Nodecl::CxxDef::make(
                    /* context */ Nodecl::NodeclBase::null(),
                    outline_struct.get_symbol()
                    )
                );
    }

    std::string ol_data_name;
    {
        TL::Counter &outline_num = TL::CounterManager::get_counter("gomp-omp-outline-data");

        std::stringstream ss;
        ss << "ol_data_" << (int)outline_num;
        ol_data_name = ss.str();

        outline_num++;
    }

    TL::Symbol outline_data = current_scope.new_symbol(ol_data_name);
    outline_data.get_internal_symbol()->kind = SK_VARIABLE;
    outline_data.get_internal_symbol()->type_information = outline_struct.get_internal_type();
    symbol_entity_specs_set_is_user_declared(outline_data.get_internal_symbol(), 1);

    TL::ObjectList<std::string> parameter_names;
    TL::ObjectList<TL::Type> parameter_types;

    parameter_names.append(ol_data_name);
    parameter_types.append(outline_struct.get_pointer_to());

    TL::Symbol outline_function = SymbolUtils::new_function_symbol(
            enclosing_function,
            outline_function_name,
            TL::Type::get_void_type(),
            parameter_names,
            parameter_types);

    Nodecl::NodeclBase outline_function_code, outline_function_stmt;
    Nodecl::List reduction_code_list;
    SymbolUtils::build_empty_body_for_function(outline_function,
            outline_function_code,
            outline_function_stmt);

    Nodecl::Utils::SimpleSymbolMap symbol_map;

    TL::Scope block_scope = outline_function_stmt.retrieve_context();

    TL::Symbol ol_data_in_outline = block_scope.get_symbol_from_name(ol_data_name);
    ERROR_CONDITION(!ol_data_in_outline.is_valid(), "Invalid symbol", 0);

    for (TL::ObjectList<TL::Symbol>::iterator it = all_symbols_passed.begin();
            it != all_symbols_passed.end();
            it++)
    {
        TL::Symbol new_shared_sym = block_scope.new_symbol(it->get_name());
        new_shared_sym.get_internal_symbol()->kind = SK_VARIABLE;

        // Computing the type of this new variable
        new_shared_sym.get_internal_symbol()->type_information = it->get_internal_symbol()->type_information;
        if (!firstprivate_symbols.contains(*it))
        {
            // The type of a fp variable isn't an lvalue reference
            new_shared_sym.get_internal_symbol()->type_information =
                lvalue_ref(it->get_internal_symbol()->type_information);
        }

        // We may need to rewrite the type
        new_shared_sym.get_internal_symbol()->type_information =
            ::type_deep_copy(
                    new_shared_sym.get_internal_symbol()->type_information,
                    outline_function_stmt.retrieve_context().get_decl_context(),
                    symbol_map.get_symbol_map());

        symbol_entity_specs_set_is_user_declared(
                new_shared_sym.get_internal_symbol(),
                1);

        Source init_ref_src;
        if (firstprivate_symbols.contains(*it))
            init_ref_src << as_symbol(ol_data_in_outline) << "->" << it->get_name();
        else
        {
            Source extra_cast;
            // VLAs are represented as void * in our arguments structure
            if (is_vla_symbol(*it))
                extra_cast << "("<< as_type(it->get_type().get_pointer_to()) << ")";

            init_ref_src << "*(" << extra_cast << as_symbol(ol_data_in_outline) << "->" << it->get_name() << ")";
        }

        Nodecl::NodeclBase init_ref = init_ref_src.parse_expression(block_scope);
        new_shared_sym.set_value(init_ref);

        symbol_map.add_map(*it, new_shared_sym);

        CXX_LANGUAGE()
        {
            outline_function_stmt.prepend_sibling(
                    Nodecl::CxxDef::make(
                        /* context */ Nodecl::NodeclBase::null(),
                        new_shared_sym));
        }
    }

    for (TL::ObjectList<TL::Symbol>::iterator it = private_symbols.begin();
            it != private_symbols.end();
            it++)
    {
        // They are to be found in the struct
        if (firstprivate_symbols.contains(*it))
            continue;

        TL::Symbol new_private_sym = GOMP::new_private_symbol(*it, block_scope);

        new_private_sym.get_internal_symbol()->type_information = ::type_deep_copy(
                new_private_sym.get_internal_symbol()->type_information,
                outline_function_stmt.retrieve_context().get_decl_context(),
                symbol_map.get_symbol_map());

        symbol_map.add_map(*it, new_private_sym);

        CXX_LANGUAGE()
        {
            outline_function_stmt.prepend_sibling(
                    Nodecl::CxxDef::make(
                        /* context */ Nodecl::NodeclBase::null(),
                        new_private_sym));
        }
    }

    Nodecl::NodeclBase parallel_body = Nodecl::Utils::deep_copy(statements,
            outline_function_stmt,
            symbol_map);


    outline_function_stmt.prepend_sibling(parallel_body);
    Nodecl::Utils::prepend_to_enclosing_top_level_location(construct, outline_function_code);

    Source setup_data;
    CXX_LANGUAGE()
    {
        setup_data << as_statement(
                Nodecl::CxxDef::make(
                    /* context */ Nodecl::NodeclBase::null(),
                    outline_data))
            ;
    }

    for (TL::ObjectList<TL::Symbol>::iterator it = all_symbols_passed.begin();
            it != all_symbols_passed.end();
            it++)
    {
        if (firstprivate_symbols.contains(*it))
        {
            setup_data
                << as_symbol(outline_data) << "." << it->get_name() << " = " << as_symbol(*it) << ";"
                ;
        }
        else
        {
            setup_data
                << as_symbol(outline_data) << "." << it->get_name() << " = &" << as_symbol(*it) << ";"
                ;
        }
    }

    Source arg_size, arg_align, if_value, copy_function;

    arg_size << outline_struct.get_size();
    arg_align << outline_struct.get_alignment_of();

    Nodecl::OpenMP::If if_clause = env.if_clause.as<Nodecl::OpenMP::If>();
    if (if_clause.is_null())
    {
        if_value << "1";
    }
    else
    {
        if_value << as_expression(if_clause.get_condition().shallow_copy());
    }

    // Task flags
    unsigned task_flags = 0;    // Redundant because of the pre-allocation feature
    std::string task_flags_name;
    {
        TL::Counter &c = TL::CounterManager::get_counter("gomp-omp-task-flags");

        std::stringstream ss;
        ss << "gomp_task_flags_" << (int)c;
        task_flags_name = ss.str();

        c++;
    }

    Source set_task_flags;
    if (!env.is_tied)
    {
        set_task_flags << task_flags_name << " |= GOMP_TASK_UNTIED;"
            ;

        task_flags |= GOMP_TASK_UNTIED;
    }
    Nodecl::OpenMP::Final final_clause = env.final_clause.as<Nodecl::OpenMP::Final>();
    if (!final_clause.is_null())
    {
        set_task_flags
            << "if (" << as_expression(final_clause.get_condition().shallow_copy()) << ")"
            <<    task_flags_name << " |= GOMP_TASK_FINAL;" 
            ;

            task_flags |= GOMP_TASK_FINAL;
    }
    if (env.is_mergeable)
    {
        set_task_flags << task_flags_name << " |= GOMP_TASK_MERGEABLE;"
            ;

        task_flags |= GOMP_TASK_MERGEABLE;
    }

    Source dependence_addresses;
    ObjectList<Nodecl::NodeclBase> dep_in = env.dep_in;
    ObjectList<Nodecl::NodeclBase> dep_out = env.dep_out;
    ObjectList<Nodecl::NodeclBase> dep_inout = env.dep_inout;
    if ( (!dep_in.empty()
            || !dep_out.empty()
            || !dep_inout.empty())
            && !this->_is_taskgraph_static)
    {
        set_task_flags << task_flags_name << " |= GOMP_TASK_DEPEND;";

        TL::Counter &c = TL::CounterManager::get_counter("gomp-omp-task-deps")++;

        std::stringstream ss;
        ss << "gomp_task_deps_" << (int)c;
        Source deps_buffer_name;
        deps_buffer_name << ss.str();

        Source size, outs, addresses;

        // The order is important: input dependences must go the last
        ObjectList<Nodecl::NodeclBase> all_deps = dep_out;
        all_deps.append(dep_inout);
        all_deps.append(dep_in);
        for (ObjectList<Nodecl::NodeclBase>::iterator it = all_deps.begin();
             it != all_deps.end(); ++it)
        {
            DataReference data_ref(*it);
            Nodecl::NodeclBase address_of_object = data_ref.get_base_address();
            Nodecl::NodeclBase offset_of_object = data_ref.get_offsetof_dependence();

            addresses << "(void *)" << as_expression(address_of_object) << " + " << as_expression(offset_of_object)
                      << ",";
        }

        size << "(void*)" << (dep_in.size() + dep_out.size() + dep_inout.size()) << "U,";
        outs << "(void*)" << dep_out.size() + dep_inout.size() << "U,";
        dependence_addresses << deps_buffer_name;

        setup_data << "void *" << deps_buffer_name << "[] = {" << size << outs
                   << addresses << "};";

        task_flags |= GOMP_TASK_DEPEND;
    }
    else
    {
        dependence_addresses << "0";
    }

    Source priority_expr;
    Nodecl::OpenMP::Priority priority = env.priority_clause.as<Nodecl::OpenMP::Priority>();
    if (!priority.is_null())
    {
        priority_expr << as_expression(priority.shallow_copy());
    }
    else
    {
        priority_expr << "0";
    }

    Nodecl::NodeclBase task_id
        = environment.find_first<Nodecl::OpenMP::TaskId>();
    Source task_id_expr;
    if (!task_id.is_null())
    {
        task_id_expr << as_expression(task_id.as<Nodecl::OpenMP::TaskId>().get_id());
    }
    else
    {
        warn_printf_at(
            construct.get_locus(),
            "internal task id for P/Socrates not available, using 0 instead\n");
        task_id_expr << "0";
    }

    Nodecl::NodeclBase dep_check = environment.find_first<Nodecl::OpenMP::DepCheck>();
    Source dep_check_expr;
    if (!dep_check.is_null())
    {
        std::string dep_check_str = dep_check.as<Nodecl::OpenMP::DepCheck>().get_text();
        if (dep_check_str == "static")
        {
            dep_check_expr << "GOMP_STATIC_TDG";
        }
        else if (dep_check_str == "dynamic")
        {
            dep_check_expr << "GOMP_DYNAMIC_TDG";
        }
        else
        {
            internal_error("Unexpected dependence check type '%s in clause dep_check\n", dep_check_str.c_str());
        }
    }
    else
    {
        // warn_printf_at(
        //     construct.get_locus(),
        //     "dep_check clause not provided, using dynamic by default\n");
        warn_printf_at(
            construct.get_locus(),
            "dep_check clause not provided, set it according to visitor::_is_taskgraph_static\n");
        if (this->_is_taskgraph_static)
          dep_check_expr << "GOMP_STATIC_TDG";
        else
          dep_check_expr << "GOMP_DYNAMIC_TDG";
    }

    // Create symbol where to store the computation of the task instance id
    Source task_instance_id;
    int instance_id_counter = CounterManager::get_counter(TASK_INSTANCE_ID_COUNTER);
    CounterManager::get_counter(TASK_INSTANCE_ID_COUNTER)++;    // Prepare for the next task
    if (_lowering->is_tdg_enabled())
    {
        std::stack<unsigned> current_loop_iterators(_loop_iterators);
        std::string task_instance_id_str = "0";
        while (!current_loop_iterators.empty())
        {
            unsigned current_id = current_loop_iterators.top();
            current_loop_iterators.pop();

            char gomp_iter_str[100];
            sprintf(gomp_iter_str, "_gomp_iter_%u", current_id);

            task_instance_id_str = "((" + task_instance_id_str + " + " + gomp_iter_str + ") * _gomp_maxI)";
        }

        task_instance_id << "unsigned _gomp_task_instance_id_" << instance_id_counter << " = "
                         << "(" << task_instance_id_str << " * _gomp_maxT) + " << task_id_expr << ";";
    }
    else
    {
        task_instance_id << "unsigned _gomp_task_instance_id_" << instance_id_counter << " = 0;";
    }

    // FIXME: we do not use the copy function because it seems to involve
    // another struct type. We do the copy here instead.
    // Check this though
    copy_function << "0";

    // Generate call to GOMP_task
    Source task_code;
    task_code
        << setup_data
        << "unsigned long " << task_flags_name << " = 0;"
        << set_task_flags
        << task_instance_id
    ;
    // Check which header of GOMP_task are we targeting
    ObjectList<Symbol> GOMP_task_syms = Scope::get_global_scope().get_symbols_from_name("GOMP_task");
    ERROR_CONDITION(GOMP_task_syms.size() != 1,
                    "Number of symbols found for GOMP_task is '%d. 1 expected.\n",
                    GOMP_task_syms.size());
    ObjectList<TL::Symbol> GOMP_task_params = GOMP_task_syms[0].get_function_parameters();
    if (GOMP_task_params.size() == 11) {
        task_code
            << "GOMP_task((void(*)(void*))"
            <<     as_symbol(outline_function) << ", &" << as_symbol(outline_data) << ", "
            <<     "(void(*)(void*,void*))" << copy_function << ","
            <<     arg_size << ", "  << arg_align << ", "  << if_value << "," << task_flags_name << ","
            <<     dependence_addresses << ", " << task_id_expr<< ", "
            <<     "_gomp_task_instance_id_" << instance_id_counter << ", " << dep_check_expr
            << ");"
        ;
    } else if (GOMP_task_params.size() == 12) {
        task_code
            << "GOMP_task((void(*)(void*))"
            <<     as_symbol(outline_function) << ", &" << as_symbol(outline_data) << ", "
            <<     "(void(*)(void*,void*))" << copy_function << ","
            <<     arg_size << ", "  << arg_align << ", "  << if_value << "," << task_flags_name << ","
            <<     dependence_addresses << ", " << priority_expr << ","
            <<     task_id_expr << ", _gomp_task_instance_id_" << instance_id_counter << ", " << dep_check_expr
            << ");"
        ;
    } else {
        internal_error("Incorrect number of parameters '%d' in function GOMP_task. \n", GOMP_task_params.size());
    }

    Nodecl::NodeclBase task_code_tree = task_code.parse_statement(construct);

    construct.replace(task_code_tree);

    if (_lowering->is_preallocate_tasks_enabled() || _lowering->is_lazy_task_creation_enabled())
    {
        // Insert the calls to preallocate tasks structures
        TaskStaticData task_static_data(outline_function,
                                        Symbol(),
                                        (long) atoi(arg_size.get_source().c_str()),
                                        (long) atoi(arg_align.get_source().c_str()),
                                        atoi(if_value.get_source().c_str()),
                                        task_flags);
        int task_id_int = atoi(task_id.as<Nodecl::OpenMP::TaskId>().get_id().prettyprint().c_str());
        ERROR_CONDITION(_task_static_data_list.find(task_id_int) != _task_static_data_list.end(),
                        "There is data already allocated for task id '%d'. Each task instance can be allocated just once.\n",
                        task_id_int)
        _task_static_data_list.insert(std::pair<int, TaskStaticData>(task_id_int, task_static_data));
    }
}

} }
