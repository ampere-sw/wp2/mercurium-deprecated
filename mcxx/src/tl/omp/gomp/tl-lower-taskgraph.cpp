/*--------------------------------------------------------------------
  (C) Copyright 2006-2014 Barcelona Supercomputing Center
                          Centro Nacional de Supercomputacion
  
  This file is part of Mercurium C/C++ source-to-source compiler.
  
  See AUTHORS file in the top level directory for information
  regarding developers and contributors.
  
  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.
  
  Mercurium C/C++ source-to-source compiler is distributed in the hope
  that it will be useful, but WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the GNU Lesser General Public License for more
  details.
  
  You should have received a copy of the GNU Lesser General Public
  License along with Mercurium C/C++ source-to-source compiler; if
  not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.
--------------------------------------------------------------------*/

#include "tl-omp-lowering-directive-environment.hpp"
#include "tl-lowering-visitor.hpp"
#include "tl-lowering-utils.hpp"
#include "tl-symbol-utils.hpp"
#include "tl-counters.hpp"


namespace TL { 
    namespace GOMP {

        void LoweringVisitor::visit(const Nodecl::OpenMP::Taskgraph &construct)
        {
            Nodecl::NodeclBase statements = construct.get_statements();

            Nodecl::List environment = construct.get_environment().as<Nodecl::List>();
            TL::OpenMP::Lowering::DirectiveEnvironment env(construct.get_environment());
            
            // Parsing taskgraph clause to set _is_taskgraph_static, so the inner tasks are accordingly lowered
            Nodecl::OpenMP::Tdg tdg_mode = environment.find_first<Nodecl::OpenMP::Tdg>();
            Nodecl::OpenMP::Record record = environment.find_first<Nodecl::OpenMP::Record>();
            ERROR_CONDITION( (!tdg_mode.is_null()) && (!record.is_null()), "taskgraph takes at most one clause", 0);
            if (tdg_mode.is_null())
            {
                this->_is_taskgraph_static = record.is_null(); //if record doesn't exist, _is_taskgraph_static is true and vice-versa
            }else{
                this->_is_taskgraph_static = (tdg_mode.get_text () == "static");
            }

            /* we must set the boolean before continuing parsing */
            walk(statements);

            // Get variables in data-sharing clauses
            TL::ObjectList<TL::Symbol> firstprivate_symbols = env.captured_value;
            TL::ObjectList<TL::Symbol> private_symbols = env.private_;  // private clause + taskgraph not allowed so far, might not need this statement
            private_symbols.insert(firstprivate_symbols);
            TL::ObjectList<TL::Symbol> shared_symbols = env.shared;

            // Gather all symbols
            TL::ObjectList<TL::Symbol> all_symbols_passed; // Set of all symbols passed in the outline
            all_symbols_passed.insert(private_symbols);
            all_symbols_passed.insert(shared_symbols);

            std::stack<unsigned> current_loop_iterators(_loop_iterators);
            while (!current_loop_iterators.empty())
            {
                unsigned current_id = current_loop_iterators.top();
                current_loop_iterators.pop();

                char gomp_iter_str[100];
                sprintf(gomp_iter_str, "_gomp_iter_%u", current_id);
                Symbol gomp_iter_sym = construct.retrieve_context().get_symbol_from_name(gomp_iter_str);
                ERROR_CONDITION(!gomp_iter_sym.is_valid(), "Symbol '%s' not found during task lowering.\n", gomp_iter_str);

                firstprivate_symbols.insert(gomp_iter_sym);
                private_symbols.insert(gomp_iter_sym);
                all_symbols_passed.insert(gomp_iter_sym);
            }

            TL::Symbol enclosing_function = Nodecl::Utils::get_enclosing_function(construct);
            std::string outline_function_name;
            {
                TL::Counter &outline_num = TL::CounterManager::get_counter("gomp-omp-outline");
                std::stringstream ss;
                ss << "_ol_" << enclosing_function.get_name() << "_" << (int)outline_num;
                outline_function_name = ss.str();
                outline_num++;
            }

            TL::Scope current_scope = construct.retrieve_context();
            
            /* creating the outline_structure */  
            TL::Type outline_struct = GOMP::create_outline_struct(
                    all_symbols_passed,
                    firstprivate_symbols,
                    enclosing_function,
                    construct.get_locus());

            CXX_LANGUAGE()
            {
                Nodecl::Utils::prepend_to_enclosing_top_level_location(
                        construct,
                        Nodecl::CxxDef::make(
                            /* context */ Nodecl::NodeclBase::null(),
                            outline_struct.get_symbol()
                            )
                        );
            }

            // setting new data, including first_taskgraph
            std::string ol_data_name;
            {
                TL::Counter &outline_num = TL::CounterManager::get_counter("gomp-omp-outline-data");

                std::stringstream ss;
                ss << "ol_data_" << (int)outline_num;
                ol_data_name = ss.str();

                outline_num++;
            }

            TL::Symbol outline_data = current_scope.new_symbol(ol_data_name);

            outline_data.get_internal_symbol()->kind = SK_VARIABLE;
            outline_data.get_internal_symbol()->type_information = outline_struct.get_internal_type();

            symbol_entity_specs_set_is_user_declared(outline_data.get_internal_symbol(), 1);

            TL::ObjectList<std::string> parameter_names;
            TL::ObjectList<TL::Type> parameter_types;

            parameter_names.append(ol_data_name);
            parameter_types.append(outline_struct.get_pointer_to());

            TL::Symbol outline_function = SymbolUtils::new_function_symbol(
                    enclosing_function,
                    outline_function_name,
                    TL::Type::get_void_type(),
                    parameter_names,
                    parameter_types);

            Nodecl::NodeclBase outline_function_code, outline_function_stmt;
            SymbolUtils::build_empty_body_for_function(outline_function,
                    outline_function_code,
                    outline_function_stmt);

            Nodecl::Utils::SimpleSymbolMap symbol_map;

            TL::Scope block_scope = outline_function_stmt.retrieve_context();

            TL::Symbol ol_data_in_outline = block_scope.get_symbol_from_name(ol_data_name);
            ERROR_CONDITION(!ol_data_in_outline.is_valid(), "Invalid symbol", 0);

            for (TL::ObjectList<TL::Symbol>::iterator it = all_symbols_passed.begin();
                        it != all_symbols_passed.end();
                        it++)
            {
                TL::Symbol new_shared_sym = block_scope.new_symbol(it->get_name());
                new_shared_sym.get_internal_symbol()->kind = SK_VARIABLE;

                // Computing the type of this new variable
                new_shared_sym.get_internal_symbol()->type_information = it->get_internal_symbol()->type_information;
                if (!firstprivate_symbols.contains(*it))
                {
                    // The type of a fp variable isn't an lvalue reference
                    new_shared_sym.get_internal_symbol()->type_information =
                        lvalue_ref(it->get_internal_symbol()->type_information);
                }

                // We may need to rewrite the type
                new_shared_sym.get_internal_symbol()->type_information =
                    ::type_deep_copy(
                            new_shared_sym.get_internal_symbol()->type_information,
                            outline_function_stmt.retrieve_context().get_decl_context(),
                            symbol_map.get_symbol_map());

                symbol_entity_specs_set_is_user_declared(
                        new_shared_sym.get_internal_symbol(),
                        1);

                Source init_ref_src;
                if (firstprivate_symbols.contains(*it))
                    init_ref_src << as_symbol(ol_data_in_outline) << "->" << it->get_name();
                else
                {
                    Source extra_cast;
                    // VLAs are represented as void * in our arguments structure
                    if (is_vla_symbol(*it))
                        extra_cast << "("<< as_type(it->get_type().get_pointer_to()) << ")";

                    init_ref_src << "*(" << extra_cast << as_symbol(ol_data_in_outline) << "->" << it->get_name() << ")";
                }

                Nodecl::NodeclBase init_ref = init_ref_src.parse_expression(block_scope);
                new_shared_sym.set_value(init_ref);

                symbol_map.add_map(*it, new_shared_sym);

                CXX_LANGUAGE()
                {
                    outline_function_stmt.prepend_sibling(
                            Nodecl::CxxDef::make(
                                /* context */ Nodecl::NodeclBase::null(),
                                new_shared_sym));
                }
            }

            // private clause + taskgraph not allowed so far, might not need this loop
            for (TL::ObjectList<TL::Symbol>::iterator it = private_symbols.begin();
                    it != private_symbols.end();
                    it++)
            {
                // They are to be found in the struct
                if (firstprivate_symbols.contains(*it))
                    continue;

                TL::Symbol new_private_sym = GOMP::new_private_symbol(*it, block_scope);

                new_private_sym.get_internal_symbol()->type_information = ::type_deep_copy(
                        new_private_sym.get_internal_symbol()->type_information,
                        outline_function_stmt.retrieve_context().get_decl_context(),
                        symbol_map.get_symbol_map());

                symbol_map.add_map(*it, new_private_sym);

                CXX_LANGUAGE()
                {
                    outline_function_stmt.prepend_sibling(
                            Nodecl::CxxDef::make(
                                /* context */ Nodecl::NodeclBase::null(),
                                new_private_sym));
                }
            }
         
            // what are these instructions doing?
            Nodecl::NodeclBase taskgraph_body = Nodecl::Utils::deep_copy(statements,
                      outline_function_stmt,
                      symbol_map);
            outline_function_stmt.prepend_sibling(taskgraph_body);
            Nodecl::Utils::prepend_to_enclosing_top_level_location(construct, outline_function_code);

            Source setup_data;
            CXX_LANGUAGE()
            {
                setup_data << as_statement(
                        Nodecl::CxxDef::make(
                            /* context */ Nodecl::NodeclBase::null(),
                            outline_data))
                    ;
            }

            for (TL::ObjectList<TL::Symbol>::iterator it = all_symbols_passed.begin();
                    it != all_symbols_passed.end();
                    it++)
            {
                if (firstprivate_symbols.contains(*it))
                {
                    setup_data
                        << as_symbol(outline_data) << "." << it->get_name() << " = " << as_symbol(*it) << ";"
                        ;
                }
                else
                {
                    setup_data
                        << as_symbol(outline_data) << "." << it->get_name() << " = &" << as_symbol(*it) << ";"
                        ;
                }
            }

            // building the actual code to lower
            Source taskgraph_lower;
            taskgraph_lower
                << setup_data
                << "GOMP_exec_tdg ((void(*)(void*))"
                <<     as_symbol(outline_function) << ", &" << as_symbol(outline_data) << ");"
                << "GOMP_taskwait();"

                ;

            Nodecl::NodeclBase taskgraph_lower_tree = taskgraph_lower.parse_statement(construct);
            construct.replace(taskgraph_lower_tree);

            //deb_print_ast(construct);
            this->_is_taskgraph_static = false;
        
        } // LoweringVisitor::visit
    } // namespace GOMP
} // namespace TL

