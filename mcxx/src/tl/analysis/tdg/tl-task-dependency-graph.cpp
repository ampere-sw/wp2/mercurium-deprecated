/*--------------------------------------------------------------------
 (C) Copyright 2006-2014 Barcelona Supercomputing Center             *
 Centro Nacional de Supercomputacion

 This file is part of Mercurium C/C++ source-to-source compiler.

 See AUTHORS file in the top level directory for information
 regarding developers and contributors.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.

 Mercurium C/C++ source-to-source compiler is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even the
 implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU Lesser General Public License for more
 details.

 You should have received a copy of the GNU Lesser General Public
 License along with Mercurium C/C++ source-to-source compiler; if
 not, write to the Free Software Foundation, Inc., 675 Mass Ave,
 Cambridge, MA 02139, USA.
 --------------------------------------------------------------------*/

#include <iomanip>

#include "cxx-cexpr.h"
#include "tl-counters.hpp"
#include "tl-analysis-utils.hpp"
#include "tl-expression-reduction.hpp"
#include "tl-task-dependency-graph.hpp"
#include "tl-induction-variables-data.hpp"

namespace TL {
namespace Analysis {

    static unsigned nt = 0;

namespace {
    std::map<FTDGNode*, std::set<TDGNode*> > ftdg_to_tdg_nodes;
    unsigned task_id = 0;

    std::map<FTDGNode*, LoopInfo*> ftdgnode_to_loop_info;
    std::map<FTDGNode*, unsigned> ftdgnode_to_task_id;

    const_value_t* get_constant(Node* n, NBase st)
    {
        if (st.is_constant())
            return st.get_constant();

        NBase malleable_st = st.shallow_copy();
        NodeclList mem_accesses = Nodecl::Utils::get_all_memory_accesses(st);
        NodeclMap& rd_in = n->get_reaching_definitions_in();
        for (NodeclList::iterator it = mem_accesses.begin(); it != mem_accesses.end(); ++it)
        {
            NBase var = *it;
            ERROR_CONDITION(rd_in.count(var) < 1,
                            "No reaching definition for variable %s in node %d.\n",
                            var.prettyprint().c_str(), n->get_id());
            ERROR_CONDITION(rd_in.count(var) > 1,
                            "More than one reaching definition for variable %s in node %d.\n",
                            var.prettyprint().c_str(), n->get_id());

            NodeclMap::iterator var_rd_in_it = rd_in.find(var);
            NBase var_rd_in = var_rd_in_it->second.first.shallow_copy();
            ERROR_CONDITION(var_rd_in.is<Nodecl::Unknown>(),
                            "Unknown reaching definition for variable %s in node %d.\n",
                            var.prettyprint().c_str(), n->get_id());
            const_value_t* var_rd_in_const;
            if (var_rd_in.is_constant())
            {
                var_rd_in_const = var_rd_in.get_constant();
            }
            else
            {
                var_rd_in_const = get_constant(n, var_rd_in);
            }
            nodecl_free(var_rd_in.get_internal_nodecl());
            NBase var_rd_in_const_n(const_value_to_nodecl(var_rd_in_const));
            Nodecl::Utils::nodecl_replace_nodecl_by_structure(malleable_st, var, var_rd_in_const_n);
        }

        Optimizations::ReduceExpressionVisitor rev;
        rev.walk(malleable_st);
        ERROR_CONDITION(!malleable_st.is_constant(),
                        "Expression %s could not be reduced to a constant.\n",
                        st.prettyprint().c_str());
        const_value_t* const_value = malleable_st.get_constant();
        nodecl_free(malleable_st.get_internal_nodecl());

        return const_value;
    }

    Nodecl::NodeclBase reduce_to_constant_as_possible(Node* n, NBase st)
    {
        if (st.is_constant())
            return st;

        NBase malleable_st = st.shallow_copy();
        NodeclList mem_accesses = Nodecl::Utils::get_all_memory_accesses(st);
        NodeclMap& rd_in = n->get_reaching_definitions_in();
        for (NodeclList::iterator it = mem_accesses.begin(); it != mem_accesses.end(); ++it)
        {
            NBase var = *it;
            ERROR_CONDITION(rd_in.count(var) < 1,
                            "No reaching definition for variable %s in node %d.\n",
                            var.prettyprint().c_str(), n->get_id());

            // This memory access cannot be reduced at this point
            // because it has more than one possible value
            if (rd_in.count(var) > 1)
                continue;

            NodeclMap::iterator var_rd_in_it = rd_in.find(var);
            NBase var_rd_in = var_rd_in_it->second.first.shallow_copy();
            ERROR_CONDITION(var_rd_in.is<Nodecl::Unknown>(),
                            "Unknown reaching definition for variable %s in node %d.\n",
                            var.prettyprint().c_str(), n->get_id());
            if (!var_rd_in.is_constant())
            {
                var_rd_in = reduce_to_constant_as_possible(n, var_rd_in);
            }
            Nodecl::Utils::nodecl_replace_nodecl_by_structure(malleable_st, var, var_rd_in);
        }

        Optimizations::ReduceExpressionVisitor rev;
        rev.walk(malleable_st);

        return malleable_st;
    }

    void propagate_constant_values(
        Nodecl::NodeclBase v,
        std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less>& relevant_vars)
    {
        for (std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less>::iterator itf = relevant_vars.begin();
             itf != relevant_vars.end(); ++itf)
        {   // The replacement will take place only if the needle exists
            Nodecl::Utils::nodecl_replace_nodecl_by_structure(/*haystack*/v, /*needle*/itf->first,
                                                              /*replacement*/Nodecl::NodeclBase(const_value_to_nodecl(itf->second)));
        }
        Optimizations::ReduceExpressionVisitor rev;
        rev.walk(v);
    }

    void print_relevant_vars(
        const std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less>& relevant_vars,
        const std::string& indent)
    {
        std::cerr << indent << "   Relevant vars : " << std::endl;
        for (auto& it: relevant_vars)
        {
            std::cerr << indent
                      << "      -> " << it.first.prettyprint()
                      << " = " << const_value_to_str(it.second) << std::endl;
        }
    }

    unsigned tdg_id = 0;
    std::map<FTDGNode*, unsigned> _ftdg_task_to_tdg_id;

    std::map<FTDGNode*, SubFTDG*> _ftdgnode_to_subftdg;
    void map_ftdgnodes_to_subftdg(const std::vector<SubFTDG*>& ftdgnodes)
    {
        for (std::vector<SubFTDG*>::const_iterator it_ftdg = ftdgnodes.begin();
             it_ftdg != ftdgnodes.end(); ++it_ftdg)
        {
            std::vector<FTDGNode*> ftdg_inner_nodes = (*it_ftdg)->get_inner_nodes();
            for (std::vector<FTDGNode*>::iterator itn = ftdg_inner_nodes.begin();
                 itn != ftdg_inner_nodes.end(); ++itn)
            {
                std::deque<FTDGNode*> worklist;
                worklist.push_back(*itn);
                while (!worklist.empty())
                {
                    FTDGNode* n = worklist.front();
                    worklist.pop_front();
                    _ftdgnode_to_subftdg[n] = *it_ftdg;
                    const ObjectList<FTDGNode*>& n_inner = n->get_inner();
                    for (ObjectList<FTDGNode*>::const_iterator iti = n_inner.begin();
                         iti != n_inner.end(); ++iti)
                    {
                        worklist.push_back(*iti);
                    }
                }
            }
        }
    }

    void fix_relevant_vars(
            const NodeclSet& new_relevant_vars,
            std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less>& all_relevant_vars,
            Node* pcfg_n)
    {
        NodeclMap& reach_defs_in = pcfg_n->get_reaching_definitions_in();
        for (auto& v : new_relevant_vars)
        {
            // Variable has already been fixed before
            if (all_relevant_vars.find(v) != all_relevant_vars.end())
                continue;

            // Variable is pointer. This analysis assumes there is no aliasing
            if (v.get_type().no_ref().is_pointer() || v.get_type().no_ref().is_array())
                continue;

            // Otherwise, fix it now!
            NodeclMap::const_iterator var_reach_def_it = reach_defs_in.find(v);
            ERROR_CONDITION(var_reach_def_it == reach_defs_in.end(),
                            "No reaching definition found for variable %s in node %d.\n",
                            v.prettyprint().c_str(), pcfg_n->get_id());
            NBase var_reach_def = var_reach_def_it->second.first.shallow_copy();
            propagate_constant_values(var_reach_def, all_relevant_vars);
            if (var_reach_def.is_constant())
            {
                all_relevant_vars.insert(std::pair<NBase, const_value_t*>(v, var_reach_def.get_constant()));
            }
            else
            {
                const_value_t* var_reach_def_cnst = get_constant(pcfg_n, var_reach_def);
                all_relevant_vars.insert(std::pair<NBase, const_value_t*>(v, var_reach_def_cnst));
            }
        }
    }
}

    SubTDG::SubTDG(unsigned maxI, unsigned maxT,
                   unsigned parent_tdg_id, const std::vector<FTDGNode*>& outermost_nodes)
        : _ftdg_outermost_nodes(outermost_nodes), _tdg_id(tdg_id++),
          _parent_tdg_id(parent_tdg_id), _maxI(maxI), _maxT(maxT), _n_nodes(0),
          _roots(), _leafs(), _tasks(), _source_to_tdg_nodes()
    {}

    void SubTDG::expand_subtdg(
            std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> relevant_vars,
            std::deque<unsigned> loops_ids)
    {
        if (TDG_DEBUG)
            std::cerr << "****************** Expanding TDG " << _tdg_id << " ******************" << std::endl;
        for (std::vector<FTDGNode*>::const_iterator it = _ftdg_outermost_nodes.begin();
             it != _ftdg_outermost_nodes.end(); ++it)
        {
            expand_node(*it, relevant_vars, loops_ids, "   ");
        }

        if (nt > 0)
            std::cerr << std::endl; // This print sets a line break after printing the TDG progress

        if (TDG_DEBUG)
            std::cerr << "**************** END expanding TDG " << _tdg_id << " ****************" << std::endl;
    }

    void SubTDG::expand_node(
            FTDGNode* n,
            std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less>& relevant_vars,
            std::deque<unsigned> loops_ids,
            const std::string& indent)
    {
        switch (n->get_type())
        {
            case FTDGLoop:
            {
                expand_loop(n, relevant_vars, loops_ids, indent);
                break;
            }
            case FTDGCondition:
            {
                expand_condition(n, relevant_vars, loops_ids, indent);
                break;
            }
            case FTDGTaskwait:
            case FTDGBarrier:
            {
                sync_create_and_connect(n, indent);
                break;
            }
            case FTDGTarget:
            {
                internal_error("Unsupported node Target while expanding TDG.\n", 0);
                break;
            }
            case FTDGTask:
            {
                expand_task(n, relevant_vars, loops_ids, indent);
                break;
            }
            default:
            {
                internal_error("Unexpected node of type '%d' while expanding TDG.\n", n->get_type());
            }
        };
    }

    void SubTDG::expand_loop(
            FTDGNode* n,
            std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> relevant_vars,
            std::deque<unsigned>& loops_ids,
            const std::string& indent)
    {
        LoopInfo* current_loop_info = ftdgnode_to_loop_info.find(n)->second;
        Utils::InductionVar* iv = current_loop_info->_iv;
        Nodecl::NodeclBase var = iv->get_variable();
        Nodecl::NodeclBase lb = current_loop_info->_lb.shallow_copy();
        Nodecl::NodeclBase ub = current_loop_info->_ub.shallow_copy();
        Nodecl::NodeclBase incr = current_loop_info->_incr.shallow_copy();

        if (TDG_DEBUG)
        {
            std::cerr << indent << "Expanding loop " << n->get_pcfg_node()->get_graph_related_ast().get_locus_str() << std::endl;
            std::cerr << indent << "   IV = " << var.prettyprint() << std::endl;
            std::cerr << indent << "   LB = " << lb.prettyprint() << std::endl;
            std::cerr << indent << "   UB = " << ub.prettyprint() << std::endl;
            std::cerr << indent << "   INCR = " << incr.prettyprint() << std::endl;
            std::cerr << indent << "   NITER = " << current_loop_info->_niter << std::endl;
        }

        // Compute the constant values of the relevant vars not fixed yet
        Node* pcfg_n = n->get_pcfg_node();
        NodeclSet loop_relevant_vars = n->get_relevant_vars();
        loop_relevant_vars.erase(var);  // Cannot fix the induction variable yet,
                                        // it will be the last thing to fix in this function
        fix_relevant_vars(loop_relevant_vars, relevant_vars, pcfg_n);

        if (TDG_DEBUG)
            print_relevant_vars(relevant_vars, indent+"   ");

        // Replace, if necessary, the current values in the loop boundaries
        bool recompute_niter = false;
        if (!lb.is_constant())
        {
            propagate_constant_values(lb, relevant_vars);
            ERROR_CONDITION(!lb.is_constant(),
                            "Lower bound of loop %s could not be reduced to a constant",
                            n->get_pcfg_node()->get_graph_related_ast().get_locus_str().c_str());
            recompute_niter = true;
        }
        if (!ub.is_constant())
        {
            propagate_constant_values(ub, relevant_vars);
            ERROR_CONDITION(!ub.is_constant(),
                            "Upper bound of loop %s could not be reduced to a constant",
                            n->get_pcfg_node()->get_graph_related_ast().get_locus_str().c_str());
            recompute_niter = true;
        }
        if (!incr.is_constant())
        {
            propagate_constant_values(incr, relevant_vars);
            ERROR_CONDITION(!incr.is_constant(),
                            "Increment of loop %s could not be reduced to a constant",
                            n->get_pcfg_node()->get_graph_related_ast().get_locus_str().c_str());
            recompute_niter = true;
        }
        if (recompute_niter)
        {
            const_value_t* niterc =
                    const_value_div(
                        const_value_add(
                            const_value_sub(
                                ub.get_constant(),
                                lb.get_constant()),
                            const_value_get_one(4, 1)),
                        incr.get_constant());
            unsigned niter = const_value_cast_to_unsigned_int(niterc);
            if (_maxI < niter)
                _maxI = niter;
        }

        const_value_t* c = lb.get_constant();
        unsigned iter = 1;
        loops_ids.push_back(iter);
        const ObjectList<FTDGNode*>& inner = n->get_inner();
        while (const_value_is_zero(const_value_gt(c, ub.get_constant())))
        {
            if (TDG_DEBUG)
            {
                const char* cn = const_value_to_str(c);
                std::cerr << indent << "   * IV "
                          << var.prettyprint()
                          << " = " << cn << std::endl;
            }

            for (ObjectList<FTDGNode*>::const_iterator it = inner.begin();
                 it != inner.end(); ++it)
            {
                std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> inner_relevant_vars = relevant_vars;
                // FIXME Do we need to add variables that depend on the induction variable.
                //       I think not, otherwise will have already been calculated??
                inner_relevant_vars.insert(std::pair<NBase, const_value_t*>(var, c));
                expand_node(*it, inner_relevant_vars, loops_ids, indent+"      ");
            }

            ++iter;
            loops_ids.pop_back();
            loops_ids.push_back(iter);
            c = const_value_add(c, incr.get_constant());
        }
        loops_ids.pop_back();

        if (TDG_DEBUG)
            std::cerr << indent << "END expanding loop" << std::endl;
    }

    void SubTDG::expand_condition(
            FTDGNode* n,
            std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> relevant_vars,
            std::deque<unsigned>& loops_ids,
            const std::string& indent)
    {
        if (TDG_DEBUG)
            std::cerr << indent << "Expanding condition " << n->get_pcfg_node()->get_graph_related_ast().get_locus_str() << std::endl;

        // Evaluate the condition
        Node* pcfg_n = n->get_pcfg_node();
        Node* cond_node = pcfg_n->get_condition_node();
        NBase cond = ExtensibleGraph::get_condition_stmts(cond_node);
        ReplaceAndEvalVisitor rev(/*lhs*/ relevant_vars, /*rhs*/ relevant_vars);
        NBase cond_shallow = cond.shallow_copy();
        bool res = rev.walk(cond_shallow);
        nodecl_free(cond_shallow.get_internal_nodecl());
        if (TDG_DEBUG)
            std::cerr << indent << "   IfElse node " << pcfg_n->get_id() << " with codition '"
                      << cond.prettyprint() << "' evaluates to " << res << std::endl;

        // Compute the constant values of the relevant vars not fixed yet
        NodeclMap& reach_defs_in = pcfg_n->get_reaching_definitions_in();
        const NodeclSet& cond_relevant_vars = n->get_relevant_vars();
        for (auto& v : cond_relevant_vars)
        {
            // Variable has already been fixed before
            if (relevant_vars.find(v) != relevant_vars.end())
                continue;

            if (v.get_type().no_ref().is_pointer() || v.get_type().no_ref().is_array())
                continue;

            // Otherwise, fix it now!
            NodeclMap::const_iterator var_reach_def_it = reach_defs_in.find(v);
            ERROR_CONDITION(var_reach_def_it == reach_defs_in.end(),
                            "No reaching definition found for variable %s in node %d.\n",
                            v.prettyprint().c_str(), pcfg_n->get_id());
            NBase var_reach_def = var_reach_def_it->second.first;
            propagate_constant_values(var_reach_def, relevant_vars);
            if (var_reach_def.is_constant())
            {
                relevant_vars.insert(std::pair<NBase, const_value_t*>(v, var_reach_def.get_constant()));
            }
            else
            {
                const_value_t* var_reach_def_cnst = get_constant(pcfg_n, var_reach_def);
                relevant_vars.insert(std::pair<NBase, const_value_t*>(v, var_reach_def_cnst));
            }
        }
        if (TDG_DEBUG)
            print_relevant_vars(relevant_vars, indent+"   ");

        // Expand the nodes in the branch taken based on the evaluation of the condition
        ObjectList<FTDGNode*> inner = (res ? n->get_inner_true() : n->get_inner_false());
        for (auto& it: inner)
        {
            expand_node(it, relevant_vars, loops_ids, indent+"      ");
        }

        if (TDG_DEBUG)
            std::cerr << indent << "END expanding condition" << std::endl;
    }

    void SubTDG::expand_task(
        FTDGNode* ftdg_n,
        std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> relevant_vars,
        const std::deque<unsigned>& loops_ids,
        const std::string& indent)
    {
        // Add relevant vars of the current node
        Node* pcfg_n = ftdg_n->get_pcfg_node();
        NodeclMap& reach_defs_in = pcfg_n->get_reaching_definitions_in();
        const NodeclSet& task_relevant_vars = ftdg_n->get_relevant_vars();
        for (auto& v : task_relevant_vars)
        {
            // Variable has already been fixed before
            if (relevant_vars.find(v) != relevant_vars.end())
                continue;

            // Variable is pointer. This analysis assumes there is no aliasing
            if (v.get_type().no_ref().is_pointer() || v.get_type().no_ref().is_array())
                continue;

            // Otherwise, fix it now!
            NodeclMap::const_iterator var_reach_def_it = reach_defs_in.find(v);
            ERROR_CONDITION(var_reach_def_it == reach_defs_in.end(),
                            "No reaching definition found for variable %s in node %d.\n",
                            v.prettyprint().c_str(), pcfg_n->get_id());
            NBase var_reach_def = var_reach_def_it->second.first.shallow_copy();    // Shallow copy, or we modify the reaching definition!
            propagate_constant_values(var_reach_def, relevant_vars);
            const_value_t* const_val;
            if (var_reach_def.is_constant())
            {
                const_val = var_reach_def.get_constant();
            }
            else
            {
                const_val = get_constant(pcfg_n, var_reach_def);
            }
            relevant_vars.insert(std::pair<NBase, const_value_t*>(v, const_val));
        }

        // Create the node and connect it
        TDGNode* tdg_task = task_create_and_connect(ftdg_n, relevant_vars, loops_ids, indent);
        FTDGNode* ftdg_n_child = ftdg_n->get_child();

        // Expand nested TDGs, if needed
        if (ftdg_n_child != NULL)
        {
            // Create the subTDG from current subftdg
            ERROR_CONDITION(_ftdgnode_to_subftdg.find(ftdg_n) == _ftdgnode_to_subftdg.end(),
                            "FTDG node %d does not have any SubFTDG related.\n",
                            ftdg_n->get_id());
            SubFTDG* itchild_subftdg = _ftdgnode_to_subftdg[ftdg_n_child];
            SubTDG* tdg = new SubTDG(_maxI, _maxT, /*default parent_tdg_id*/ 0, itchild_subftdg->get_inner_nodes());
            std::deque<unsigned> nested_loops_ids(loops_ids);
            tdg->expand_subtdg(relevant_vars, nested_loops_ids);
            _tdgs.push_back(tdg);

            // Connect created SubTDG with its parent SubTDG
            tdg->set_parent_tdg_id(_tdg_id);
            tdg_task->set_child(tdg);
        }
    }

    void SubTDG::connect_nodes(TDGNode* source, TDGNode* target, const std::string& indent)
    {
        source->add_output(target);
        target->add_input(source);
        if (TDG_DEBUG)
            std::cerr << indent << "Connecting " << source->get_id() << " -> " << target->get_id() << std::endl;
    }

    void SubTDG::disconnect_nodes(TDGNode* source, TDGNode* target, const std::string& indent)
    {
        source->remove_output(target);
        target->remove_input(source);
        if (TDG_DEBUG)
            std::cerr << indent << "Disconnecting " << source->get_id() << " -> " << target->get_id() << std::endl;
    }

    TDGNode* SubTDG::create_task_node(FTDGNode* ftdg_n, const std::deque<unsigned>& loops_ids, const std::string& indent)
    {
        ERROR_CONDITION(ftdg_n->get_type() != FTDGTask,
                        "Unsuported type %d for an TDGNode. Only tasks accepted\n",
                        ftdg_n->get_type());

        Node* source_node = ftdg_n->get_pcfg_node();
        Nodecl::NodeclBase source_task = source_node->get_graph_related_ast();
        ERROR_CONDITION(!source_task.is<Nodecl::OpenMP::Task>(),
                        "The extensible graph node %d related with an TDG task node has wrong type %s",
                        ftdg_n->get_pcfg_node()->get_id(),
                        ast_print_node_type(source_task.get_kind()));
        unsigned etask_id = get_tdg_node_id(ftdgnode_to_task_id.find(ftdg_n)->second, loops_ids);
        TDGNode* tdg_n = new TDGNode(etask_id, source_node);
        _n_nodes++;
        _source_to_tdg_nodes[source_task].append(tdg_n);

        if (TDG_DEBUG)
            std::cerr << indent << "Created task node " << tdg_n->get_id() << " with related pcfg node " << ftdg_n->get_pcfg_node()->get_id() << std::endl;

        _tasks.insert(std::pair<unsigned, TDGNode*>(etask_id, tdg_n));
        _leafs.insert(tdg_n);

        return tdg_n;
    }

    unsigned SubTDG::get_tdg_node_id(unsigned task_id, std::deque<unsigned> loops_ids)
    {
//         if (TDG_DEBUG)
//             std::cerr << "TASK " << task_id << "(";
        unsigned sum = 0;
        while (!loops_ids.empty())
        {
//             if (TDG_DEBUG)
//                 std::cerr << loops_ids.back() << ", ";
            sum = (sum + loops_ids.back()) * _maxI;
            loops_ids.pop_back();
        }
//         if (TDG_DEBUG)
//             std::cerr << ")  ->  " << task_id + (_maxT * sum) << std::endl;
        return task_id + (_maxT * sum);
    }

namespace {
    void remove_ancestors_from_set(TDGNode* n, std::set<TDGNode*>& s)
    {
        std::set<TDGNode*> n_ins = n->get_inputs();
        for (std::set<TDGNode*>::iterator it = n_ins.begin(); it != n_ins.end(); ++it)
        {
            if (s.find(*it) != s.end())
            {
                s.erase(*it);
                remove_ancestors_from_set(*it, s);
            }
        }
    }
}

    bool SubTDG::compute_task_connections(
            TDGNode* possible_source,
            TDGNode* target,
            std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> target_vars_map,
            std::set<TDGNode*>& all_possible_ancestors,
            const std::string& indent)
    {
        // @possible_source is not a candidate to be an ancestor of @target
        if (all_possible_ancestors.find(possible_source) == all_possible_ancestors.end())
            return false;

        // Check the dependency replacing all variables with the corresponding constant values in the dependency expression
        bool res = false;
        Node* possible_source_pcfg_node = possible_source->get_pcfg_node();
        Node* target_pcfg_node = target->get_pcfg_node();
        ObjectList<Node*> children = possible_source_pcfg_node->get_children();
        Edge* edge = NULL;
        NBase cond;
        if (!children.find(target_pcfg_node).empty())
        {
            edge = ExtensibleGraph::get_edge_between_nodes(possible_source_pcfg_node, target_pcfg_node);
            cond = edge->get_condition();
        }
        if (edge == NULL /*this a fabricated edge between nodes from different nesting regions*/
            || cond.is_null() /*the edge is unconditional*/)
        {
            res = true;
        }
        else
        {
            std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> possible_source_vars_map = possible_source->get_vars_map();
            ReplaceAndEvalVisitor rev(/*lhs*/ possible_source_vars_map, /*rhs*/ target_vars_map);
            NBase cond_cp = cond.shallow_copy();
            res = rev.walk(cond_cp);
            nodecl_free(cond_cp.get_internal_nodecl());
        }

        // If the condition evaluates to true, then
        //   - connect the nodes
        //   - remove all @possible_source ancestors from the list of possible ancestors
        // otherwise, keep traversing bottom-top the TDG
        if (res)
        {
            connect_nodes(possible_source, target, indent);
            remove_ancestors_from_set(possible_source, all_possible_ancestors);
            _leafs.erase(possible_source);
        }

        return res;
    }

    void SubTDG::connect_task_node(TDGNode* tdg_n, FTDGNode* ftdg_n, const std::string& indent)
    {
        // Gather all possible ancestors
        std::set<TDGNode*> current_tdg_possible_ancestors;
        std::set<TDGNode*> other_tdg_possible_ancestors;
        const ObjectList<FTDGNode*> predecessors = ftdg_n->get_predecessors();
        for (ObjectList<FTDGNode*>::const_iterator it = predecessors.begin(); it != predecessors.end(); ++it)
        {
            std::map<FTDGNode*, std::set<TDGNode*> >::iterator itm = ftdg_to_tdg_nodes.find(*it);
            // Its parents have not been created yet
            if (itm == ftdg_to_tdg_nodes.end())
                continue;

            std::set<TDGNode*> it_possible_ancestors = itm->second;
            if (ftdg_n->get_parent() == (*it)->get_parent())
            {
                current_tdg_possible_ancestors.insert(it_possible_ancestors.begin(),
                                                    it_possible_ancestors.end());
            }
            else
            {
                other_tdg_possible_ancestors.insert(it_possible_ancestors.begin(),
                                                    it_possible_ancestors.end());
            }
        }

        // Connect the node with previous dependences/synchronizations
        std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> tdg_n_vars_map = tdg_n->get_vars_map();
            // Connect it with other nodes from the same TDG depending on the predicates
        std::deque<TDGNode*> nlist(_leafs.begin(), _leafs.end());
        std::set<TDGNode*> already_visited;
        while (!nlist.empty())
        {
            TDGNode* possible_source = nlist.front();
            nlist.pop_front();
            if (already_visited.find(possible_source) != already_visited.end())
                continue;
            already_visited.insert(possible_source);

            // Do not connect a TDG node with itself
            if (possible_source == tdg_n)
                continue;

            bool connected = compute_task_connections(possible_source, tdg_n, tdg_n_vars_map, current_tdg_possible_ancestors, indent);
            if (!connected)
            {
                std::set<TDGNode*> inputs = possible_source->get_inputs();
                for (std::set<TDGNode*>::iterator it = inputs.begin(); it != inputs.end(); ++it)
                {
                    nlist.push_back(*it);
                }
            }
        }
        // Connect it with nodes from other TDGs depending on the predicates
        for (std::set<TDGNode*>::iterator it = other_tdg_possible_ancestors.begin();
             it != other_tdg_possible_ancestors.end(); ++it)
        {
            compute_task_connections(*it, tdg_n, tdg_n_vars_map, other_tdg_possible_ancestors, indent);
        }

        // Connect the node with previous synchronizations from the same TDG
        if (tdg_n->get_inputs().empty())
        {
            // The only entry node is the creation of the task
            _roots.insert(tdg_n);
        }
    }

    void SubTDG::remove_task_transitive_inputs(TDGNode* n)
    {
        const std::set<TDGNode*>& inputs = n->get_inputs();
        for (std::set<TDGNode*>::const_iterator its = inputs.begin(); its != inputs.end(); ++its)
        {
            for (std::set<TDGNode*>::const_iterator itt = inputs.begin(); itt != inputs.end(); ++itt)
            {
                if (*its == *itt)
                    continue;

                if (is_ancestor(*its, *itt))
                {
                    disconnect_nodes(*its, n, "   ");
                }
            }
        }
    }

    TDGNode* SubTDG::task_create_and_connect(
            FTDGNode* ftdg_n,
            const std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less>& relevant_vars,
            const std::deque<unsigned>& loops_ids,
            const std::string& indent)
    {
        if (nt == 0)
            std::cerr << indent << "Tasks expansion progress (this may take some time)" << std::endl;
        std::cerr << '\r' << std::setw(6) << ++nt << std::flush;
        TDGNode* tdg_n = create_task_node(ftdg_n, loops_ids, indent);
        tdg_n->set_vars_map(relevant_vars);

        ftdg_to_tdg_nodes[ftdg_n].insert(tdg_n);
        connect_task_node(tdg_n, ftdg_n, indent);
        remove_task_transitive_inputs(tdg_n);

        _ftdg_task_to_tdg_id[ftdg_n] = _tdg_id;

        return tdg_n;
    }

    static int sync_id = -1;
    TDGNode* SubTDG::create_sync_node(
            FTDGNode* ftdg_n,
            const std::string& indent)
    {
        ERROR_CONDITION(ftdg_n->get_type() != FTDGTaskwait && ftdg_n->get_type() != FTDGBarrier,
                        "Unsuported type %d for an TDGNode. Only tasks accepted\n",
                        ftdg_n->get_type());
        if (TDG_DEBUG)
            std::cerr << indent << "Created sync node " << sync_id << " with related pcfg node " << ftdg_n->get_pcfg_node()->get_id() << std::endl;
        return new TDGNode(sync_id--, ftdg_n->get_pcfg_node());
    }

    void SubTDG::connect_sync_node(TDGNode* tdg_n, const std::string& indent)
    {
        for (std::set<TDGNode*>::iterator it = _leafs.begin(); it != _leafs.end(); ++it)
        {
            connect_nodes(*it, tdg_n, indent);
        }
        _leafs.clear();
        _leafs.insert(tdg_n);
    }

    void SubTDG::sync_create_and_connect(
            FTDGNode* ftdg_n,
            const std::string& indent)
    {
        TDGNode* tdg_n = create_sync_node(ftdg_n, indent);

        ftdg_to_tdg_nodes[ftdg_n].insert(tdg_n);

        connect_sync_node(tdg_n, indent);
    }

    bool SubTDG::is_ancestor(TDGNode* source, TDGNode* target)
    {
        const std::set<TDGNode*>& outputs = source->get_outputs();
        if (outputs.find(target) != outputs.end())
            return true;

        for (std::set<TDGNode*>::const_iterator it = outputs.begin(); it != outputs.end(); ++it)
        {
            if (is_ancestor(*it, target))
                return true;
        }

        return false;
    }

    std::set<TDGNode*> remove_visits;
    void SubTDG::remove_synchronizations_rec(TDGNode* n)
    {
        if (remove_visits.find(n) != remove_visits.end())
            return;
        remove_visits.insert(n);

        // Note: get here the list of outputs, before the node may be removed and disconnected
        const std::set<TDGNode*>& outputs = n->get_outputs();
        if (n->get_id() < 0)
        {
            // Disconnect the node from its parents and children and remove it
            const std::set<TDGNode*>& inputs = n->get_inputs();
            for (std::set<TDGNode*>::const_iterator it = inputs.begin(); it != inputs.end(); ++it)
            {
                disconnect_nodes(*it, n, "   ");
            }
            for (std::set<TDGNode*>::const_iterator it = outputs.begin(); it != outputs.end(); ++it)
            {
                disconnect_nodes(n, *it, "   ");
            }

            // Note that a node may be a leaf and have children at the same time
            // This happens only if the children are from a different TDG

            if (_leafs.find(n) != _leafs.end())
            {   // Restore the list of leafs
                _leafs.erase(n);
                for (std::set<TDGNode*>::const_iterator it = inputs.begin(); it != inputs.end(); ++it)
                {
                    _leafs.insert(*it);
                }
            }

            // Connect all parents with all children
            for (std::set<TDGNode*>::const_iterator its = inputs.begin(); its != inputs.end(); ++its)
            {
                for (std::set<TDGNode*>::const_iterator itt = outputs.begin(); itt != outputs.end(); ++itt)
                {
                    connect_nodes(*its, *itt, "   ");
                }
            }

            // Remove the node
            delete n;
        }

        for (std::set<TDGNode*>::const_iterator it = outputs.begin(); it != outputs.end(); ++it)
            remove_synchronizations_rec(*it);
    }

    void SubTDG::remove_synchronizations()
    {
        for (ObjectList<TDGNode*>::iterator it = _roots.begin(); it != _roots.end(); ++it)
            remove_synchronizations_rec(*it);
    }

    void SubTDG::purge_subtdg()
    {
        if (TDG_DEBUG)
            std::cerr << "****************** Removing synchronizations from TDG " << _tdg_id << " ******************" << std::endl;
        remove_synchronizations();
        if (TDG_DEBUG)
            std::cerr << "**************** END removing synchronizations from TDG " << _tdg_id << " ****************" << std::endl;
    }

    unsigned SubTDG::get_tdg_id() const
    {
        return _tdg_id;
    }

    unsigned SubTDG::get_parent_tdg_id() const
    {
        return _parent_tdg_id;
    }

    void SubTDG::set_parent_tdg_id(unsigned parent_tdg_id)
    {
        _parent_tdg_id = parent_tdg_id;
    }

    unsigned SubTDG::get_maxI() const
    {
        return _maxI;
    }

    unsigned SubTDG::get_maxT() const
    {
        return _maxT;
    }

    unsigned SubTDG::get_n_nodes() const
    {
        return _n_nodes;
    }

    const ObjectList<TDGNode*>& SubTDG::get_roots() const
    {
        return _roots;
    }

    const std::set<TDGNode*>& SubTDG::get_leafs() const
    {
        return _leafs;
    }

    const std::map<unsigned, TDGNode*>& SubTDG::get_tasks() const
    {
        return _tasks;
    }

    unsigned SubTDG::get_num_tasks() const
    {
        return _tasks.size();
    }

    const std::map<Nodecl::NodeclBase, ObjectList<TDGNode*> >&
             SubTDG::get_source_to_tdg_nodes() const
    {
        return _source_to_tdg_nodes;
    }

    const std::vector<SubTDG*> SubTDG::get_nested_tdgs() const
    {
        return _tdgs;
    }

    TaskDependencyGraph::TaskDependencyGraph(ExtensibleGraph* pcfg)
        : _ftdg(NULL), _tdgs(), _maxI(0), _maxT(0)
    {
        _ftdg = new FlowTaskDependencyGraph(pcfg);
        if (TDG_DEBUG)
            _ftdg->print_tdg_to_dot();
        map_ftdgnodes_to_subftdg(_ftdg->get_outermost_nodes());
        compute_constants();
        expand_tdg();
        add_global_variables();
    }

namespace {
    bool node_contains_nested_tasks(const std::vector<FTDGNode*>& outermost_nodes)
    {
        bool contains_nested_tasks = false;
        for (std::vector<FTDGNode*>::const_iterator it = outermost_nodes.begin();
             it != outermost_nodes.end() && !contains_nested_tasks; ++it)
        {
            switch ((*it)->get_type())
            {
                case FTDGLoop:
                case FTDGCondition:
                {
                    const ObjectList<FTDGNode*>& inner = (*it)->get_inner();
                    contains_nested_tasks = node_contains_nested_tasks(inner);
                    break;
                }
                case FTDGTarget:
                {
                    internal_error("Unsupported node Target while expanding TDG.\n", 0);
                }
                case FTDGTaskwait:
                case FTDGBarrier:
                {
                    break;
                }
                case FTDGTask:
                {
                    if ((*it)->get_parent() != NULL)
                        contains_nested_tasks = true;
                }
            };
        }
        return contains_nested_tasks;
    }
}

    void TaskDependencyGraph::compute_constants_rec(FTDGNode* n)
    {
        switch (n->get_type())
        {
            case FTDGLoop:
            {
                Node* pcfg_n = n->get_pcfg_node();
                Utils::InductionVarList& ivs = pcfg_n->get_induction_variables();
                ERROR_CONDITION(ivs.size() != 1,
                                "TDG does not support loops with more than one Induction Variable.\n",
                                0);

                Utils::InductionVar* iv = ivs[0];
                NodeclSet lbs = iv->get_lb();
                NodeclSet ubs = iv->get_ub();
                ERROR_CONDITION(lbs.size()!=1 || ubs.size()!=1,
                                "Induction variable %s has an unsupported behavior\n",
                                iv->get_variable().prettyprint().c_str());

                Nodecl::NodeclBase lb = reduce_to_constant_as_possible(pcfg_n, *lbs.begin());
                Nodecl::NodeclBase ub = reduce_to_constant_as_possible(pcfg_n, *ubs.begin());
                Nodecl::NodeclBase incr = reduce_to_constant_as_possible(pcfg_n, iv->get_increment());
                unsigned niter = 0;
                if (lb.is_constant() && ub.is_constant() && incr.is_constant())
                {
                    const_value_t* niterc = const_value_div(const_value_add(const_value_sub(ub.get_constant(),
                                                                                            lb.get_constant()),
                                                                            const_value_get_one(4, 1)),
                                                            incr.get_constant());
                    niter = const_value_cast_to_unsigned_int(niterc);
                }
                if (_maxI < niter)
                    _maxI = niter;

                LoopInfo* li = new LoopInfo(
                        iv, lb, ub, incr, 0);
                ftdgnode_to_loop_info[n] = li;

                // NOTE: No break here because we still have to traverse inner nodes
            }
            case FTDGCondition:
            {
                const ObjectList<FTDGNode*>& inner = n->get_inner();
                for (ObjectList<FTDGNode*>::const_iterator it = inner.begin(); it != inner.end(); ++it)
                    compute_constants_rec(*it);
                break;
            }
            case FTDGTask:
            {
                ftdgnode_to_task_id[n] = ++task_id;

                if (_maxT < task_id)
                    _maxT = task_id;

                // Traverse nested TDGs, if needed, to preserve the order of the task_id
                FTDGNode* n_child = n->get_child();
                if (n_child != NULL)
                {
                    SubFTDG* nchild_subftdg = _ftdgnode_to_subftdg[n_child];
                    std::vector<FTDGNode*> inner_nodes = nchild_subftdg->get_inner_nodes();
                    for (std::vector<FTDGNode*>::iterator it = inner_nodes.begin();
                         it != inner_nodes.end(); ++it)
                    {
                        compute_constants_rec(*it);
                    }
                }

                break;
            }
            default:
            {}  // Nothing to do
        };
    }
    void TaskDependencyGraph::compute_constants()
    {
        const std::vector<SubFTDG*>& ftdg_outermost_nodes = _ftdg->get_outermost_nodes();
        for (std::vector<SubFTDG*>::const_iterator it = ftdg_outermost_nodes.begin();
             it != ftdg_outermost_nodes.end(); ++it)
        {
            std::vector<FTDGNode*> current_outermost_nodes = (*it)->get_inner_nodes();
            if (node_contains_nested_tasks(current_outermost_nodes))
                continue;

            for (std::vector<FTDGNode*>::const_iterator itt = current_outermost_nodes.begin();
                itt != current_outermost_nodes.end(); ++itt)
            {
                compute_constants_rec(*itt);
            }
        }

        if (TDG_DEBUG)
            std::cerr << "_maxT = " << _maxT << ". _maxI = " << _maxI << std::endl;
    }

    void TaskDependencyGraph::expand_tdg()
    {
        const std::vector<FTDGNode*>& ftdg_parents = _ftdg->get_parents();
        const std::vector<SubFTDG*>& ftdg_outermost_nodes = _ftdg->get_outermost_nodes();
        ERROR_CONDITION(ftdg_parents.size() != ftdg_outermost_nodes.size(),
                        "FTDG corrupted: The number of parents (%d) is different from the number of sets of outermost nodes (%d).\n",
                        ftdg_parents.size(), ftdg_outermost_nodes.size());

        // Perform reverse iteration, so nested regions are computed before outer regions
        // Otherwise some predecessors may not have been created
//         std::vector<std::vector<FTDGNode*> >::const_reverse_iterator ito = ftdg_outermost_nodes.rbegin();
//         for (; ito != ftdg_outermost_nodes.rend(); ++ito)
        // FIXME Nested regions must be created after their parent, otherwise variables may have unknown values when depending from parent's expansion
        //       Removing revers traversal may break some codes...
        std::vector<SubFTDG*>::const_iterator ito = ftdg_outermost_nodes.begin();
        std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> relevant_vars;
        std::deque<unsigned> loops_ids;
        for (; ito != ftdg_outermost_nodes.end(); ++ito)
        {
            // Check whether the outermost node contains a task with nested tasks
            // In this case, the subgraph is not expanded from here
            // but launched during the expansion of the parent task,
            // so the proper variable values are propagated
            // Example:
            // for (int i = 0; i < N; i++)
            //     #pragma omp task
            //     for (int j = 0; j < M; j++)
            //         #pragma omp task
            // FIXME Sara: We are actually avoiding expanding the outer task??? Check this behavior
            std::vector<FTDGNode*> inner_nodes = (*ito)->get_inner_nodes();
            if (node_contains_nested_tasks(inner_nodes))
                continue;

            // NOTE If this 'default' SubTDG is not the first in the list of _tdgs,
            // then mapper will not work when computing gomp_tdg_ntasks
            SubTDG* it_tdg = new SubTDG(_maxI, _maxT, /*default parent_tdg_id*/ 0, inner_nodes);
            it_tdg->expand_subtdg(relevant_vars, loops_ids);
            _tdgs.push_back(it_tdg);
            // A SubTDG may have expanded nested TDGs. Attach them here to the complete list of TDGs
            const std::vector<SubTDG*>& nested_tdgs = it_tdg->get_nested_tdgs();
            for (std::vector<SubTDG*>::const_iterator it = nested_tdgs.begin();
                 it != nested_tdgs.end(); ++it)
            {
                _tdgs.push_back(*it);
            }
        }

        // Purge all TDGs from their synchronization nodes
        for (std::vector<SubTDG*>::reverse_iterator it_tdg = _tdgs.rbegin();
             it_tdg != _tdgs.rend(); ++it_tdg)
        {
            (*it_tdg)->purge_subtdg();

            if (TDG_DEBUG)
            {
                std::cerr << "****************** Summary of TDG " << (*it_tdg)->get_tdg_id() << " ******************" << std::endl;
                std::cerr << "      List of roots:" << std::endl;
                const ObjectList<TDGNode*>& roots = (*it_tdg)->get_roots();
                for (ObjectList<TDGNode*>::const_iterator it = roots.begin(); it != roots.end(); ++it)
                {
                    std::cerr << "         - " << (*it)->get_id() << std::endl;
                }
                std::cerr << "      List of leafs:" << std::endl;
                const std::set<TDGNode*>& leafs = (*it_tdg)->get_leafs();
                for (std::set<TDGNode*>::const_iterator it = leafs.begin(); it != leafs.end(); ++it)
                {
                    std::cerr << "         - " << (*it)->get_id() << std::endl;
                }
                std::cerr << "**************** END summary of TDG " << (*it_tdg)->get_tdg_id() << " ****************" << std::endl;
            }
        }
    }

    void TaskDependencyGraph::add_global_variables()
    {
        TL::Scope global_sc = Scope::get_global_scope();

        // Create maxI variable
        TL::Symbol gomp_maxI = global_sc.new_symbol("_gomp_maxI");
        symbol_entity_specs_set_is_user_declared(gomp_maxI.get_internal_symbol(), 1);
        gomp_maxI.get_internal_symbol()->kind = SK_VARIABLE;
        gomp_maxI.set_type(get_unsigned_int_type());
        gomp_maxI.set_value(const_value_to_nodecl(const_value_get_integer(_maxI, /*num_bytes*/4, /*sign*/0)));

        // Create maxT variable
        TL::Symbol gomp_maxT = global_sc.new_symbol("_gomp_maxT");
        symbol_entity_specs_set_is_user_declared(gomp_maxT.get_internal_symbol(), 1);
        gomp_maxT.get_internal_symbol()->kind = SK_VARIABLE;
        gomp_maxT.set_type(get_unsigned_int_type());
        gomp_maxT.set_value(const_value_to_nodecl(const_value_get_integer(_maxT, /*num_bytes*/4, /*sign*/0)));
    }

    FlowTaskDependencyGraph* TaskDependencyGraph::get_ftdg() const
    {
        return _ftdg;
    }

    const std::vector<SubTDG*>& TaskDependencyGraph::get_tdgs() const
    {
        return _tdgs;
    }

    unsigned TaskDependencyGraph::get_maxI() const
    {
        return _maxI;
    }

    unsigned TaskDependencyGraph::get_maxT() const
    {
        return _maxT;
    }

    unsigned TaskDependencyGraph::get_number_of_nodes() const
    {
        unsigned n_nodes = 0;
        for (std::vector<SubTDG*>::const_iterator it = _tdgs.begin(); it != _tdgs.end(); ++it)
        {
            n_nodes += (*it)->get_n_nodes();
        }
        return n_nodes;
    }

    std::string TaskDependencyGraph::get_name() const
    {
        return _ftdg->get_pcfg()->get_name();
    }
}
}
