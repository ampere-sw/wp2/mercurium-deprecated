/*--------------------------------------------------------------------
 ( C) Copyright 2006-2014 Barcelona Supercomputing Center             * *
 Centro Nacional de Supercomputacion
 
 This file is part of Mercurium C/C++ source-to-source compiler.
 
 See AUTHORS file in the top level directory for information
 regarding developers and contributors.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 
 Mercurium C/C++ source-to-source compiler is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even the
 implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU Lesser General Public License for more
 details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with Mercurium C/C++ source-to-source compiler; if
 not, write to the Free Software Foundation, Inc., 675 Mass Ave,
 Cambridge, MA 02139, USA.
 
 --------------------------------------------------------------------*/



#include "tl-counters.hpp"
#include "tl-task-dependency-graph.hpp"


namespace TL { 
namespace Analysis {

    // ******************************************************************* //
    // **************** Flow and Expanded TDG components ***************** //

    FTDGNode::FTDGNode(unsigned id, Node* n, FTDGNodeType type)
        : _id(id), _n(n), _parent(NULL), _child(NULL), _predecessors(),
          _type(type), _inner_true(), _inner_false(), _relevant_vars()
    {
        pcfg_to_ftdg.insert(std::pair<Node*, FTDGNode*>(n, this));
    }

    unsigned FTDGNode::get_id() const
    {
        return _id;
    }

    Node* FTDGNode::get_pcfg_node() const
    {
        return _n;
    }

    FTDGNode* FTDGNode::get_parent() const
    {
        return _parent;
    }

    void FTDGNode::set_parent(FTDGNode* parent)
    {
        _parent = parent;
    }

    FTDGNode* FTDGNode::get_child() const
    {
        return _child;
    }

    void FTDGNode::set_child(FTDGNode* child)
    {
        _child = child;
    }

    const ObjectList<FTDGNode*>& FTDGNode::get_predecessors() const
    {
        return _predecessors;
    }

    void FTDGNode::add_predecessor(FTDGNode* predecessor)
    {
        _predecessors.push_back(predecessor);
    }

    FTDGNodeType FTDGNode::get_type() const
    {
        return _type;
    }

    ObjectList<FTDGNode*> FTDGNode::get_inner() const
    {
        ObjectList<FTDGNode*> res = _inner_true;
        res.append(_inner_false);
        return res;
    }

    const ObjectList<FTDGNode*>& FTDGNode::get_inner_true() const
    {
        return _inner_true;
    }

    const ObjectList<FTDGNode*>& FTDGNode::get_inner_false() const
    {
        return _inner_false;
    }

    const ObjectList<FTDGNode*>& FTDGNode::get_outer() const
    {
        return _outer;
    }

    const NodeclSet& FTDGNode::get_relevant_vars() const
    {
        return _relevant_vars;
    }

    void FTDGNode::add_inner(FTDGNode* n)
    {
        _inner_true.insert(n);
    }

    void FTDGNode::add_inner_true(FTDGNode* n)
    {
        _inner_true.insert(n);
    }

    void FTDGNode::add_inner_false(FTDGNode* n)
    {
        _inner_false.insert(n);
    }

    void FTDGNode::add_outer(FTDGNode* n)
    {
        _outer.insert(n);
    }

    void FTDGNode::add_relevant_vars(std::set<NBase, Nodecl::Utils::Nodecl_structural_less> relevant_vars)
    {
        _relevant_vars.insert(relevant_vars.begin(), relevant_vars.end());
    }

    TDGNode::TDGNode(int id, Node* pcfg_node)
        : _id(id), _var_to_value(), _inputs(), _outputs(),
          _child(NULL), _pcfg_node(pcfg_node)
    {}

    int TDGNode::get_id() const
    {
        return _id;
    }

    std::set<TDGNode*> TDGNode::get_inputs() const
    {
        return _inputs;
    }

    void TDGNode::add_input(TDGNode* n)
    {
        _inputs.insert(n);
    }

    void TDGNode::remove_input(TDGNode* n)
    {
        _inputs.erase(n);
    }

    std::set<TDGNode*> TDGNode::get_outputs() const
    {
        return _outputs;
    }

    void TDGNode::add_output(TDGNode* n)
    {
        _outputs.insert(n);
    }

    void TDGNode::remove_output(TDGNode* n)
    {
        _outputs.erase(n);
    }

    SubTDG* TDGNode::get_child() const
    {
        return _child;
    }

    void TDGNode::set_child(SubTDG* child)
    {
        _child = child;
    }

    std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> TDGNode::get_vars_map() const
    {
        return _var_to_value;
    }

    void TDGNode::set_vars_map(std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> var_to_value)
    {
        _var_to_value = var_to_value;
    }

    Node* TDGNode::get_pcfg_node() const
    {
        return _pcfg_node;
    }

    Nodecl::NodeclBase TDGNode::get_source_task() const
    {
        ERROR_CONDITION(!_pcfg_node->get_graph_related_ast().is<Nodecl::OpenMP::Task>(),
                        "Cannot retrieve the source task of a node which does not represent a Task, but a %s instead.\n",
                        (_pcfg_node->is_graph_node() ? _pcfg_node->get_graph_type_as_string().c_str()
                                                     : _pcfg_node->get_type_as_string().c_str()));
        return _pcfg_node->get_graph_related_ast();
    }

    // **************** Flow and Expanded TDG components ***************** //
    // ******************************************************************* //

}
}
