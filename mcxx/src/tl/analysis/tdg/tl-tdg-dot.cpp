/*--------------------------------------------------------------------
 ( C) Copyright 2006-2014 Barcelona Supercomputing Center             * *
 Centro Nacional de Supercomputacion
 
 This file is part of Mercurium C/C++ source-to-source compiler.
 
 See AUTHORS file in the top level directory for information
 regarding developers and contributors.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 
 Mercurium C/C++ source-to-source compiler is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even the
 implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU Lesser General Public License for more
 details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with Mercurium C/C++ source-to-source compiler; if
 not, write to the Free Software Foundation, Inc., 675 Mass Ave,
 Cambridge, MA 02139, USA.
 
 --------------------------------------------------------------------*/


#include <fstream>
#include <sys/stat.h>
#include <unistd.h>

#include "tl-task-dependency-graph.hpp"


namespace TL { 
namespace Analysis {

    const char* color_names[] = {
        "aquamarine3", "crimson", "chartreuse", "blue2", "darkorchid3", "darkgoldenrod1",
        "deeppink4", "gray19", "indigo", "indianred", "forestgreen", "navy", "orangered2",
        "slateblue3", "yellowgreen", "salmon", "purple", "mediumturquoise", "slategray3"
    };
    unsigned next_color_i = 0;
    std::map<Nodecl::NodeclBase, std::string> color_to_node_map;
    std::map<TDGNode*, SubTDG*> tdg_node_to_child_subtdg;
    std::multimap<unsigned, unsigned> tdg_connections;
    std::set<TDGNode*> printed_nodes;
    void TaskDependencyGraph::print_tdg_to_dot_rec(TDGNode* n, std::ofstream& dot_tdg)
    {
        if (printed_nodes.find(n) != printed_nodes.end())
            return;
        printed_nodes.insert(n);

        // Print the node
        Nodecl::NodeclBase source_n = n->get_source_task();
        std::string color;
        if (color_to_node_map.find(source_n) != color_to_node_map.end()) {
            color = color_to_node_map[source_n];
        } else {
            color = color_names[++next_color_i];
            color_to_node_map[source_n] = color;
        }
        dot_tdg << "      " << n->get_id() << "[color=" << color << ",style=bold]\n";

         // Print the entry edges
        const std::set<TDGNode*>& inputs = n->get_inputs();
        for (std::set<TDGNode*>::const_iterator it = inputs.begin(); it != inputs.end(); ++it)
        {
            tdg_connections.insert(std::pair<unsigned, unsigned>((*it)->get_id(), n->get_id()));
        }

        // Store children edges to be printed later
        SubTDG* child = n->get_child();
        if (child != NULL)
        {
            tdg_node_to_child_subtdg[n] = child;
        }

        // Keep iterating
        const std::set<TDGNode*>& outputs = n->get_outputs();
        for (std::set<TDGNode*>::const_iterator it = outputs.begin(); it != outputs.end(); ++it)
        {
            print_tdg_to_dot_rec(*it, dot_tdg);
        }
    }
    
    std::map<unsigned, SubTDG*> tdg_id_to_tdg;
    void TaskDependencyGraph::print_tdg_to_dot()
    {
        // Create the directory of dot files if it has not been previously created
        char buffer[1024];
        char* err = getcwd(buffer, 1024);
        if(err == NULL)
            internal_error ("An error occurred while getting the path of the current directory", 0);
        struct stat st;
        std::string directory_name = std::string(buffer) + "/dot/";
        if(stat(directory_name.c_str(), &st) != 0)
        {
            int dot_directory = mkdir(directory_name.c_str(), S_IRWXU);
            if(dot_directory != 0)
                internal_error ("An error occurred while creating the dot directory in '%s'",
                                directory_name.c_str());
        }

        // Create the file where we will store the DOT TDG
        std::string dot_file_name = directory_name + _ftdg->get_pcfg()->get_name() + "_tdg.dot";
        std::ofstream dot_tdg;
        dot_tdg.open(dot_file_name.c_str());
        if(!dot_tdg.good())
            internal_error ("Unable to open the file '%s' to store the TDG.", dot_file_name.c_str());

        // Create the DOT graphs
        if(VERBOSE)
            std::cerr << "- TDG DOT file '" << dot_file_name << "'" << std::endl;
        dot_tdg << "digraph TDG {\n";
        dot_tdg << "   compound=true\n";
            // Perform reverse iteration, so the nodes get printed within their corresponding cluster
            // Otherwise, nested clusters get out of the cluster and print nodes from the parent's cluster
        for (std::vector<SubTDG*>::reverse_iterator it = _tdgs.rbegin(); it != _tdgs.rend(); ++it)
        {
            tdg_id_to_tdg[(*it)->get_tdg_id()] = *it;
            dot_tdg << "   subgraph cluster_" << (*it)->get_tdg_id() << " {\n";
                dot_tdg << "      label=TDG_" << (*it)->get_tdg_id() << "\n";
                const ObjectList<TDGNode*>& roots = (*it)->get_roots();
                for (ObjectList<TDGNode*>::const_iterator itr = roots.begin(); itr != roots.end(); ++itr)
                    print_tdg_to_dot_rec(*itr, dot_tdg);
            dot_tdg << "   }\n";
        }
            // Print connections in the most outer level, so we avoid printing nodes withing clusters they do not belong to
        for (std::multimap<unsigned, unsigned>::iterator it = tdg_connections.begin();
             it != tdg_connections.end(); ++it)
        {
            dot_tdg << "   " << it->first << " -> " << it->second << "\n";
        }
            // Print creation edges
        for (std::map<TDGNode*, SubTDG*>::iterator it = tdg_node_to_child_subtdg.begin();
             it != tdg_node_to_child_subtdg.end(); ++it)
        {
            const std::map<unsigned, TDGNode*>& child_tasks = it->second->get_tasks();
            ERROR_CONDITION(child_tasks.empty(),
                            "No tasks found for TDG %d, cannot connect nested regions",
                            it->second->get_tdg_id());
            dot_tdg << "   " << it->first->get_id() << " -> " << child_tasks.begin()->second->get_id()
                        << "[style=\"dashed\", lhead=cluster_" << it->second->get_tdg_id() << "]\n";
        }
            // Print the legend
        dot_tdg << "  node [shape=plaintext];\n";
        dot_tdg << "   subgraph cluster_1000 {\n";
        dot_tdg << "      label=\"User functions:\"; style=\"rounded\";\n";
        dot_tdg << "      user_funcs [label=<<table border=\"0\" cellspacing=\"10\" cellborder=\"0\">\n";
        for (std::map<Nodecl::NodeclBase, std::string>::iterator it = color_to_node_map.begin();
             it != color_to_node_map.end(); ++it)
        {
            dot_tdg << "      <tr>\n";
            dot_tdg << "         <td bgcolor=\"" << it->second << "\" width=\"15px\" border=\"1\"></td>\n";
            dot_tdg << "         <td>" << it->first.get_locus_str() << "</td>\n";
            dot_tdg << "      </tr>\n";
        }
        dot_tdg << "      </table>>]\n";
        dot_tdg << "   }";

            // Close the file
        dot_tdg << "}\n";
        dot_tdg.close();
        if(!dot_tdg.good())
            internal_error ("Unable to close the file '%s' where TDG has been stored.", dot_file_name.c_str());
    }

    static unsigned ftdg_cluster_id = 1;
    // We store the connections and print them at the end so the nodes are enclosed in their corresponding clusters
    // In we print on the fly, some nodes may change cluster because the edge is printed within a cluster which is not that of the source
    static std::multimap<unsigned, unsigned> ftdg_connections;
    void FlowTaskDependencyGraph::print_tdg_node_to_dot(
            FTDGNode* n,
            std::string indent, std::string color,
            std::ofstream& dot_tdg,
            FTDGNode*& parent,
            FTDGNode*& head)
    {
        FTDGNodeType nt = n->get_type();
        switch (nt)
        {
            case FTDGLoop:
            {
                dot_tdg << indent << "subgraph cluster_" << ftdg_cluster_id++ << " {\n";
                    dot_tdg << indent << "   label=Loop_" << n->get_id() << "\n";
                    dot_tdg << indent << "   color=" << color << "\n";
                    ObjectList<FTDGNode*> inner = n->get_inner();
                    for (ObjectList<FTDGNode*>::const_iterator it = inner.begin();
                         it != inner.end(); ++it)
                    {
                        print_tdg_node_to_dot(*it, indent+"   ", color, dot_tdg, parent, head);
                    }
                dot_tdg << indent << "}\n";
                break;
            }
            case FTDGCondition:
            {
                dot_tdg << indent << "subgraph cluster_" << ftdg_cluster_id++ << " {\n";
                    dot_tdg << indent << "   label=IfElse_" << n->get_id() << "\n";
                    dot_tdg << indent << "   color=" << color << "\n";
                    const ObjectList<FTDGNode*>& inner_true = n->get_inner_true();
                    for (ObjectList<FTDGNode*>::const_iterator it = inner_true.begin();
                         it != inner_true.end(); ++it)
                    {
                        print_tdg_node_to_dot(*it, indent+"   ", "deepskyblue", dot_tdg, parent, head);
                    }
                    const ObjectList<FTDGNode*>& inner_false = n->get_inner_false();
                    for (ObjectList<FTDGNode*>::const_iterator it = inner_false.begin();
                         it != inner_false.end(); ++it)
                    {
                        print_tdg_node_to_dot(*it, indent+"   ", "deeppink1", dot_tdg, parent, head);
                    }
                dot_tdg << indent << "}\n";
                break;
            }
            case FTDGTarget:
            {
                dot_tdg << indent << n->get_id()
                        << " [label=\"Target " << n->get_id()
                        << "\", color=" << color << "]\n";
                break;
            }
            case FTDGTask:
            {
                dot_tdg << indent << n->get_id()
                        << " [label=\"Task " << n->get_id()
                        << "\", color=" << color << "]\n";
                break;
            }
            case FTDGTaskwait:
            {
                dot_tdg << indent << n->get_id()
                        << " [label=\"Taskwait " << n->get_id()
                        << "\", color=" << color << "]\n";
                break;
            }
            case FTDGBarrier:
            {
                dot_tdg << indent << n->get_id()
                        << " [label=\"Barrier " << n->get_id()
                        << "\", color=" << color << "]\n";
                break;
            }
            default:
            {
                internal_error("Unexpected node type %d\n.", n->get_type());
            }
        };

        if (nt == FTDGTarget || nt == FTDGTask || nt == FTDGTaskwait || nt == FTDGBarrier)
        {
            parent = n->get_parent();
            if (head == NULL)
                head = n;

            const ObjectList<FTDGNode*>& predecessors = n->get_predecessors();
            for (ObjectList<FTDGNode*>::const_iterator it = predecessors.begin();
                    it != predecessors.end(); ++it)
            {
                ftdg_connections.insert(std::pair<unsigned, unsigned>((*it)->get_id(), n->get_id()));
            }
        }
    }

    void FlowTaskDependencyGraph::print_tdg_to_dot()
    {
        // Create the directory of dot files if it has not been previously created
        char buffer[1024];
        char* err = getcwd(buffer, 1024);
        if(err == NULL)
            internal_error ("An error occurred while getting the path of the current directory", 0);
        struct stat st;
        std::string directory_name = std::string(buffer) + "/dot/";
        if(stat(directory_name.c_str(), &st) != 0)
        {
            int dot_directory = mkdir(directory_name.c_str(), S_IRWXU);
            if(dot_directory != 0)
                internal_error ("An error occurred while creating the dot directory in '%s'",
                                directory_name.c_str());
        }

        // Create the file where we will store the DOT TDG
        std::string dot_file_name = directory_name + _pcfg->get_name() + "_ftdg.dot";
        std::ofstream dot_tdg;
        dot_tdg.open(dot_file_name.c_str());
        if(!dot_tdg.good())
            internal_error ("Unable to open the file '%s' to store the FTDG.", dot_file_name.c_str());

        // Create the DOT graph
        if(VERBOSE)
            std::cerr << "- FTDG DOT file '" << dot_file_name << "'" << std::endl;
        dot_tdg << "digraph FTDG {\n";
        dot_tdg << "   compound=true\n";

        unsigned tdg_id = 0;
        std::string indent = "   ";
        for (std::vector<SubFTDG*>::iterator it = _outermost_nodes.begin();
             it != _outermost_nodes.end(); ++it)
        {
            std::vector<FTDGNode*> inner_nodes = (*it)->get_inner_nodes();
            // Outermostnodes without any node inside do not need to be printed
            // These nodes appear because the generation of the FTDG is naïve
            // and creates an outernode each time a task appears, in case it contains nested tasks
            if (inner_nodes.empty())
                continue;

            unsigned current_cluster_id = ftdg_cluster_id;
            dot_tdg << indent << "subgraph cluster_" << ftdg_cluster_id++ << " {\n";
                dot_tdg << indent << "   label=TDG_" << tdg_id++ << "\n";
                dot_tdg << indent << "   color=goldenrod1\n";
                FTDGNode* parent = NULL;
                FTDGNode* head = NULL;
                for (std::vector<FTDGNode*>::iterator itt = inner_nodes.begin(); itt != inner_nodes.end(); ++itt)
                    print_tdg_node_to_dot(*itt, indent+"   ", /*color*/"black", dot_tdg, parent, head);
                if (parent != NULL)
                {
                    dot_tdg << indent << "   " << parent->get_id() << " -> " << head->get_id()
                            << " [style=\"dashed\", lhead=cluster_" << current_cluster_id << "]\n";
                }
            dot_tdg << indent << "}\n";
        }

        // Print all connections here
        for (std::map<unsigned, unsigned>::iterator it = ftdg_connections.begin();
             it != ftdg_connections.end(); ++it)
        {
            dot_tdg << indent << it->first << " -> " << it->second << "\n";
        }

        dot_tdg << "}\n";
        dot_tdg.close();
        if(!dot_tdg.good())
            internal_error ("Unable to close the file '%s' where FTDG has been stored.", dot_file_name.c_str());
    }
}
}
