/*--------------------------------------------------------------------
 ( C) Copyright 2006-2014 Barcelona Supe*rcomputing Center             *
 Centro Nacional de Supercomputacion
 
 This file is part of Mercurium C/C++ source-to-source compiler.
 
 See AUTHORS file in the top level directory for information
 regarding developers and contributors.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 
 Mercurium C/C++ source-to-source compiler is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even the
 implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU Lesser General Public License for more
 details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with Mercurium C/C++ source-to-source compiler; if
 not, write to the Free Software Foundation, Inc., 675 Mass Ave,
 Cambridge, MA 02139, USA.
 --------------------------------------------------------------------*/

#include <fstream>
#include <unistd.h>

#include "cxx-driver-utils.h"
#include "cxx-profile.h"
#include "tl-compilerpipeline.hpp"
#include "tl-task-dependency-graph.hpp"

namespace TL {
namespace Analysis {

    TaskDependencyGraphMapper::TaskDependencyGraphMapper(
        ObjectList<TaskDependencyGraph*> tdgs)
        : _tdgs(tdgs)
    {}

    void TaskDependencyGraphMapper::generate_runtime_tdg(bool tomp_lower_enabled)
    {
        if (_tdgs.empty())
            return;

        // Get the current directory
        char buffer[1024];
        char* err = getcwd(buffer, 1024);
        if (err == NULL)
            internal_error("An error occurred while getting the path of the current directory", 0);
        std::string directory_name = std::string(buffer);

        // Create the file where we will store the TDG
        std::string source_filename_with_extension = (*_tdgs.begin())->get_ftdg()->get_pcfg()->get_graph()->get_graph_related_ast().get_filename();
        std::size_t filename_extension_position = source_filename_with_extension.find_last_of(".");
        std::string source_filename_without_extension = source_filename_with_extension.substr(0, filename_extension_position);
        std::string file_name = directory_name + "/" + source_filename_without_extension + "_tdg.c";
        std::ofstream rt_tdg;
        rt_tdg.open(file_name.c_str());
        if(!rt_tdg.good())
            internal_error ("Unable to open the file '%s' to store the runtime TDG.", file_name.c_str());

        std::string rtl_name = (tomp_lower_enabled ? "tomp" : "gomp");
        rt_tdg << "// File automatically generated\n";
        rt_tdg << "#include <stdio.h>\n";
        if (tomp_lower_enabled)
            rt_tdg << "#pragma omp declare target\n\n";
        // Declare the data structure that holds the TDG
        if (!tomp_lower_enabled)
            rt_tdg << "struct gomp_task;\n";
        rt_tdg << "struct " << rtl_name << "_tdg {\n";
            rt_tdg << "    unsigned long id;                   // Task instance ID\n";
            if (!tomp_lower_enabled)
            {
                rt_tdg << "    struct gomp_task *task;\n";
                rt_tdg << "    unsigned short offin;               // Starting position within the tomp_tdg_ins structure\n";
                rt_tdg << "    unsigned short offout;              // Starting position within the tomp_tdg_outs structure\n";
            }
            else
            {
                rt_tdg << "    void *task;\n";
                rt_tdg << "    unsigned short offin;               // Starting position within the gomp_tdg_ins structure\n";
                rt_tdg << "    unsigned short offout;              // Starting position within the gomp_tdg_outs structure\n";
            }
            rt_tdg << "    unsigned char nin;                  // Number of input dependencies\n";
            rt_tdg << "    unsigned char nout;                 // Number of output dependencies\n";
            rt_tdg << "    signed char cnt;                    // Number of dependent tasks:\n";
            rt_tdg << "                                        // (1) cnt == -1 Task executed or not created\n";
            rt_tdg << "                                        // (2) cnt == 0 task being executed\n";
            rt_tdg << "                                        // (3) cnt >0 task waiting for 'cnt' dependent tasks\n";
            rt_tdg << "    int map;                            // Thread assigned to the task by the static scheduler\n";
            if (!tomp_lower_enabled)
                rt_tdg << "    long task_counter;                  // Private counter assigned to each task instance to compute execution time while executing in parallel\n";
            else
                rt_tdg << "    double task_counter;                // Private counter assigned to each task instance to compute execution time while executing in parallel\n";
            rt_tdg << "    long task_counter_end;              // Private counter assigned to each task instance that contains the final time of a task\n";
            if (!tomp_lower_enabled)
                rt_tdg << "    long runtime_counter;               // Private counter assigned to each task instance to compute the overhead of the runtime\n";
            else
                rt_tdg << "    double runtime_counter;             // Private counter assigned to each task instance to compute the overhead of the runtime\n";
            rt_tdg << "    long taskpart_counter;              // Private counter assigned to each task part executed just before the corresponding task\n";
            rt_tdg << "    struct gomp_tdg *next_waiting_tdg;  // Private counter assigned to each task part executed just before the corresponding task\n";
            rt_tdg << "    unsigned int pragma_id;             // Identifier of the task contruct from which this task instance has been created\n";
            rt_tdg << "    void* data;                         // Data structure containing the parameters to be passed to the task outlined function\n";
        rt_tdg << "};\n";
        rt_tdg << "\n";

        unsigned n_tdg = 0;
        for (ObjectList<TaskDependencyGraph*>::iterator it = _tdgs.begin(); it != _tdgs.end(); ++it)
        {
            // Get all the tasks from all SubTDGs. they are to be merges in the same TDG structure for the runtime
            std::map<unsigned, TDGNode*> tasks;
            const std::vector<SubTDG*>& subTDGs = (*it)->get_tdgs();
            for (std::vector<SubTDG*>::const_iterator its = subTDGs.begin(); its != subTDGs.end(); ++its)
            {
                SubTDG* tdg = *its;
                std::map<unsigned, TDGNode*> current_tasks = tdg->get_tasks();
                std::swap(tasks, current_tasks);
                tasks.insert(current_tasks.begin(), current_tasks.end());
            }

            // Map tasks to their position in the data structure (this is needed to fill inputs and outputs fields)
            std::map<unsigned, unsigned> task_to_position;
            unsigned current_position = 0;
            for (std::map<unsigned, TDGNode*>::const_iterator itt = tasks.begin(); itt != tasks.end(); ++itt)
            {
                task_to_position[itt->second->get_id()] = current_position++;
            }

            // Create the TDG data structure
            rt_tdg << "struct " << rtl_name  << "_tdg " << rtl_name << "_tdg_" << n_tdg << "[" << tasks.size() << "] = {\n";
            unsigned next_offin = 0;
            unsigned next_offout = 0;
            for (std::map<unsigned, TDGNode*>::const_iterator itt = tasks.begin(); itt != tasks.end(); )
            {
                unsigned in_size = itt->second->get_inputs().size();
                unsigned out_size = itt->second->get_outputs().size();
                Nodecl::OpenMP::Task task_directive = itt->second->get_pcfg_node()->get_graph_related_ast().as<Nodecl::OpenMP::Task>();
                Nodecl::List task_environ = task_directive.get_environment().as<Nodecl::List>();
                Nodecl::IntegerLiteral pragma_id =
                        task_environ.find_first<Nodecl::OpenMP::TaskId>().as<Nodecl::OpenMP::TaskId>().get_id().as<Nodecl::IntegerLiteral>();

                rt_tdg << "{";
                    rt_tdg << ".id = " << itt->second->get_id() << ",";
                    rt_tdg << ".task = 0,";
                    rt_tdg << ".offin = " << next_offin << ",";
                    rt_tdg << ".offout = " << next_offout << ",";
                    rt_tdg << ".nin = " << in_size << ",";
                    rt_tdg << ".nout = " << out_size << ",";
                    rt_tdg << ".cnt = -1,";
                    rt_tdg << ".map = -1,";
                    rt_tdg << ".task_counter = 0,";
                    rt_tdg << ".task_counter_end = 0,";
                    rt_tdg << ".runtime_counter = 0,";
                    rt_tdg << ".taskpart_counter = 0,";
                    rt_tdg << ".next_waiting_tdg = NULL,";
                    rt_tdg << ".pragma_id = " << const_value_cast_to_unsigned_int(pragma_id.get_constant());
                    // rt_tdg << ".data";   // This member is initialized from the runtime
                rt_tdg << "}";

                ++itt;
                if (itt != tasks.end())
                    rt_tdg << ",";
                rt_tdg << "\n";

                next_offin += in_size;
                next_offout += out_size;
            }
            rt_tdg << "};\n";
            rt_tdg << "\n";

            // Create input/output dependencies data structures
            rt_tdg << "unsigned short " << rtl_name << "_tdg_ins_" << n_tdg << "[] = {\n    ";
            char first_in = 1;
            for (std::map<unsigned, TDGNode*>::const_iterator itt = tasks.begin(); itt != tasks.end(); ++itt)
            {
                std::set<TDGNode*> inputs = itt->second->get_inputs();
                for (std::set<TDGNode*>::iterator iti = inputs.begin(); iti != inputs.end(); ++iti)
                {
                    if (first_in)
                        first_in = 0;
                    else
                        rt_tdg << ", ";
                    rt_tdg << task_to_position[(*iti)->get_id()];
                }
            }
            rt_tdg << "};\n";
            // The next field is not unsigned because the runtime is currently not using the information passed by the compiler.
            // This is so because the order in which the compiler passes the tasks is not the order of creation,
            // and this introduces overhead in the execution due to exploiting less locality in many cases.
            rt_tdg << "unsigned short " << rtl_name << "_tdg_outs_" << n_tdg << "[] = {\n    ";
            char first_out = 1;
            for (std::map<unsigned, TDGNode*>::const_iterator itt = tasks.begin(); itt != tasks.end(); ++itt)
            {
                std::set<TDGNode*> outputs = itt->second->get_outputs();
                for (std::set<TDGNode*>::iterator ito = outputs.begin(); ito != outputs.end(); ++ito)
                {
                    if (first_out)
                        first_out = 0;
                    else
                        rt_tdg << ", ";
                    rt_tdg << task_to_position[(*ito)->get_id()];
                }
            }
            rt_tdg << "};\n";
            rt_tdg << "\n";

            n_tdg++;
        }

        // Create global data structures that contain all TDGs
        unsigned n_tdgs = _tdgs.size();
        rt_tdg << "// All TDGs are stored in a single data structure\n";
        rt_tdg << "unsigned " << rtl_name << "_num_tdgs = " << n_tdgs << ";\n";
        rt_tdg << "struct " << rtl_name << "_tdg *" << rtl_name << "_tdg[" << n_tdgs << "] = {\n";
            n_tdg = 0;
            for (ObjectList<TaskDependencyGraph*>::iterator it = _tdgs.begin(); it != _tdgs.end(); )
            {
                rt_tdg << "    " << rtl_name << "_tdg_" << n_tdg << "\n";
                ++it;
                if (it != _tdgs.end())
                    rt_tdg << ",";
                n_tdg++;
            }
        rt_tdg << "};\n";
        rt_tdg << "unsigned short *" << rtl_name << "_tdg_ins[" << n_tdgs << "] = {\n";
            n_tdg = 0;
            for (ObjectList<TaskDependencyGraph*>::iterator it = _tdgs.begin(); it != _tdgs.end(); )
            {
                rt_tdg << "    " << rtl_name << "_tdg_ins_" << n_tdg;
                ++it;
                if (it != _tdgs.end())
                    rt_tdg << ",";
                rt_tdg << "\n";
                n_tdg++;
            }
        rt_tdg << "};\n";
        // Since field Xomp_tdg_outs_Y is not an unsigned anymore, the next field is not unsigned either
        rt_tdg << "unsigned short *" << rtl_name << "_tdg_outs[" << n_tdgs << "] = {\n";
            n_tdg = 0;
            for (ObjectList<TaskDependencyGraph*>::iterator it = _tdgs.begin(); it != _tdgs.end(); )
            {
                rt_tdg << "    " << rtl_name << "_tdg_outs_" << n_tdg;
                ++it;
                if (it != _tdgs.end())
                    rt_tdg << ",";
                rt_tdg << "\n";
                n_tdg++;
            }
        rt_tdg << "};\n";
        // The size of gomp_tdg_ntasks is the total number of tasks regions, considering all tdgs
        rt_tdg << "unsigned " << rtl_name << "_tdg_ntasks[" << n_tdgs << "] = {\n";
            for (ObjectList<TaskDependencyGraph*>::iterator it = _tdgs.begin(); it != _tdgs.end(); )
            {
                // NOTE This code relies in that the first SubTDG of the list, is the default ETDG
                const std::vector<SubTDG*>& tdgs = (*it)->get_tdgs();
                int n_tasks = 0;
                for (std::vector<SubTDG*>::const_iterator itt = tdgs.begin(); itt != tdgs.end(); ++itt)
                {
                    n_tasks += (*itt)->get_num_tasks();
                }
                rt_tdg << "    " << n_tasks;
                ++it;
                if (it != _tdgs.end())
                    rt_tdg << ",";
                rt_tdg << "\n";
            }
        rt_tdg << "};\n";
        rt_tdg << "unsigned " << rtl_name << "_maxI[" << n_tdgs << "] = {\n";
            for (ObjectList<TaskDependencyGraph*>::iterator it = _tdgs.begin(); it != _tdgs.end(); )
            {
                rt_tdg << "    " << (*it)->get_maxI() << "\n";
                ++it;
                if (it != _tdgs.end())
                    rt_tdg << ",";
            }
        rt_tdg << "};\n";
        rt_tdg << "unsigned " << rtl_name << "_maxT[" << n_tdgs << "] = {\n";
            for (ObjectList<TaskDependencyGraph*>::iterator it = _tdgs.begin(); it != _tdgs.end(); )
            {
                rt_tdg << "    " << (*it)->get_maxT() << "\n";
                ++it;
                if (it != _tdgs.end())
                    rt_tdg << ",";
            }
        rt_tdg << "};\n";
        rt_tdg << "\n";

        // Delcare methods that are necessary to communicate the application, the compiler and the runtime
        rt_tdg << "// Initialize runtime data-strucures from here.\n";
        rt_tdg << "// This code is called from the compiler.\n";
        rt_tdg << "extern void GOMP_init_tdg(unsigned num_tdgs, struct " << rtl_name << "_tdg **tdg,\n";
        rt_tdg << "                          unsigned short ** tdg_ins, unsigned short ** tdg_outs,\n";
        rt_tdg << "                          unsigned *tdg_ntasks, unsigned *maxI, unsigned *maxT);\n";
        rt_tdg << "extern void GOMP_set_tdg_id(unsigned int);\n";
        rt_tdg << "void " << rtl_name << "_set_tdg(unsigned int tdg_id) {\n";
        rt_tdg << "    GOMP_init_tdg(" << rtl_name << "_num_tdgs, " << rtl_name << "_tdg,\n";
        rt_tdg << "                  " << rtl_name << "_tdg_ins, " << rtl_name << "_tdg_outs, " << rtl_name << "_tdg_ntasks,\n";
        rt_tdg << "                  " << rtl_name << "_maxI, " << rtl_name << "_maxT);\n";
        rt_tdg << "    GOMP_set_tdg_id(tdg_id);\n";
        rt_tdg << "}\n";

        if (tomp_lower_enabled)
            rt_tdg << "\n#pragma omp end declare target\n";

        // Add the new file to the compilation pipeline
        compilation_configuration_t* configuration = ::get_compilation_configuration("auxcc");
        ERROR_CONDITION (configuration == NULL, "auxcc profile is mandatory when using Safety profile", 0);
        load_compiler_phases(configuration);    // Make sure phases are loaded (this is needed for codegen)
        TL::CompilationProcess::add_file(file_name, "auxcc");
    }

}
}
