/*--------------------------------------------------------------------
 (C) Copyright 2006-2014 Barcelona Supercomputing Center             *
 Centro Nacional de Supercomputacion

 This file is part of Mercurium C/C++ source-to-source compiler.

 See AUTHORS file in the top level directory for information
 regarding developers and contributors.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.

 Mercurium C/C++ source-to-source compiler is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even the
 implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU Lesser General Public License for more
 details.

 You should have received a copy of the GNU Lesser General Public
 License along with Mercurium C/C++ source-to-source compiler; if
 not, write to the Free Software Foundation, Inc., 675 Mass Ave,
 Cambridge, MA 02139, USA.
 --------------------------------------------------------------------*/

#ifndef TL_TASK_DEPENDENCY_GRAPH_HPP
#define TL_TASK_DEPENDENCY_GRAPH_HPP

#include "tl-extensible-graph.hpp"
#include "tl-nodecl-replacer.hpp"

#include <deque>

#define TDG_DEBUG debug_options.tdg_verbose

namespace TL { 
namespace Analysis {

    extern int tdg_node_id;

    class FTDGNode;
    extern std::map<Node*, FTDGNode*> pcfg_to_ftdg;


    // ******************************************************************* //
    // ******************** Flow Task Dependency Graph ******************* //

    enum FTDGNodeType {
        FTDGLoop,
        FTDGCondition,
        FTDGTarget,
        FTDGTask,
        FTDGTaskwait,
        FTDGBarrier
    };

    class FTDGNode {
    private:
        unsigned _id;
        Node* _n;

        FTDGNode* _parent;                      // Nested tasks, parent
        FTDGNode* _child;                       // Nested tasks, child
        ObjectList<FTDGNode*> _predecessors;    // Control and data-flow predecessors (siblings)

        enum FTDGNodeType _type;

        ObjectList<FTDGNode*> _inner_true;  // For true edges in an IfElse conditional and
                                            // for all inner nodes in Loops
        ObjectList<FTDGNode*> _inner_false; // For false edges in an IfElse conditional
        ObjectList<FTDGNode*> _outer;

        NodeclSet _relevant_vars;           // Variables that will be needed during expansion, including
                                            // dependency vars and control-flow vars

    public:
        FTDGNode(unsigned id, Node* n, FTDGNodeType type);

        unsigned get_id() const;
        Node* get_pcfg_node() const;

        FTDGNode* get_parent() const;
        void set_parent(FTDGNode* n);
        FTDGNode* get_child() const;
        void set_child(FTDGNode* n);
        const ObjectList<FTDGNode*>& get_predecessors() const;
        void add_predecessor(FTDGNode* predecessor);

        FTDGNodeType get_type() const;

        ObjectList<FTDGNode*> get_inner() const;
        const ObjectList<FTDGNode*>& get_inner_true() const;
        const ObjectList<FTDGNode*>& get_inner_false() const;
        const ObjectList<FTDGNode*>& get_outer() const;
        const NodeclSet& get_relevant_vars() const;

        void add_inner(FTDGNode* n);
        void add_inner_true(FTDGNode* n);
        void add_inner_false(FTDGNode* n);
        void add_outer(FTDGNode* n);
        void add_relevant_vars(NodeclSet relevant_vars);
    };

    class LIBTL_CLASS SubFTDG
    {
    private:
        std::vector<FTDGNode*> _inner_nodes;

    public:
        SubFTDG();

        std::vector<FTDGNode*> get_inner_nodes() const;
        void push_back(FTDGNode* n);
    };

    class LIBTL_CLASS FlowTaskDependencyGraph
    {
    private:
        ExtensibleGraph* _pcfg;
        std::vector<FTDGNode*> _parents;                        // Parent FTDGNode for each nesting level of parallelism
        std::vector<SubFTDG*> _outermost_nodes;                 // Set of outermost nodes for each nesting level of parallelism

        void build_siblings_flow_tdg(
            Node* n,
            std::stack<Node*> parent);
        void build_flow_tdg_rec(
            Node* n,
            std::stack<Node*> parent,
            std::vector<FTDGNode*>& control,
            NodeclSet& relevant_vars,
            bool conditional,
            bool true_edge);

        void print_tdg_node_to_dot(
                FTDGNode* n,
                std::string indent, std::string color,
                std::ofstream& dot_tdg,
                /*out*/ FTDGNode*& parent,
                /*out*/ FTDGNode*& head);

    public:
        FlowTaskDependencyGraph(ExtensibleGraph* pcfg);

        ExtensibleGraph* get_pcfg() const;
        const std::vector<FTDGNode*>& get_parents() const;
        const std::vector<SubFTDG*>& get_outermost_nodes() const;

        void print_tdg_to_dot();
    };

    // ******************** Flow Task Dependency Graph ******************* //
    // ******************************************************************* //



    // ******************************************************************* //
    // ********************** Task Dependency Graph ********************** //

    class SubTDG;
    class TDGNode {
    private:
        unsigned _id;

        std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> _var_to_value;

        std::set<TDGNode*> _inputs;
        std::set<TDGNode*> _outputs;

        SubTDG* _child;   // Child TDG enclosed in an TDGNode

        Node* _pcfg_node;

    public:
        TDGNode(int id, Node* n);

        int get_id() const;

        std::set<TDGNode*> get_inputs() const;
        void add_input(TDGNode* n);
        void remove_input(TDGNode* n);
        std::set<TDGNode*> get_outputs() const;
        void add_output(TDGNode* n);
        void remove_output(TDGNode* n);

        SubTDG* get_child() const;
        void set_child(SubTDG* child);

        std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> get_vars_map() const;
        void set_vars_map(std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> var_to_value);

        Node* get_pcfg_node() const;
        Nodecl::NodeclBase get_source_task() const;
    };

    class ReplaceAndEvalVisitor : public Nodecl::NodeclVisitor<bool>
    {
    private:
        std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> _lhs_vars;
        std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> _rhs_vars;
        bool _lhs;

        bool visit_comparison(const Nodecl::NodeclBase& n);

    public:
        ReplaceAndEvalVisitor(std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> lhs_vars,
                              std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> rhs_vars);

        bool unhandled_node(const NBase& n);
        bool join_list(ObjectList<bool>& list);

        bool visit(const Nodecl::Add& n);
        bool visit(const Nodecl::Conversion& n);
        bool visit(const Nodecl::Different& n);
        bool visit(const Nodecl::Equal& n);
        bool visit(const Nodecl::GreaterOrEqualThan& n);
        bool visit(const Nodecl::GreaterThan& n);
        bool visit(const Nodecl::IntegerLiteral& n);
        bool visit(const Nodecl::LogicalAnd& n);
        bool visit(const Nodecl::LogicalOr& n);
        bool visit(const Nodecl::LowerThan& n);
        bool visit(const Nodecl::LowerOrEqualThan& n);
        bool visit(const Nodecl::Minus& n);
        bool visit(const Nodecl::Mul& n);
        bool visit(const Nodecl::Symbol& n);
    };

    class LIBTL_CLASS SubTDG
    {
    private:
        std::vector<FTDGNode*> _ftdg_outermost_nodes;
        unsigned _tdg_id;
        unsigned _parent_tdg_id;

        unsigned _maxI;
        unsigned _maxT;
        unsigned _n_nodes;

        ObjectList<TDGNode*> _roots;
        std::set<TDGNode*> _leafs;

        std::map<unsigned, TDGNode*> _tasks;

        std::map<Nodecl::NodeclBase, ObjectList<TDGNode*> > _source_to_tdg_nodes;

        std::vector<SubTDG*> _tdgs; // Nested SubTDG created from current SubTDG

        void expand_node(
                FTDGNode* n,
                std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less>& relevant_vars,
                std::deque<unsigned> loops_ids,
                const std::string& indent);
        void expand_loop(
                FTDGNode* n,
                std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> relevant_vars,
                std::deque<unsigned>& loops_ids,
                const std::string& indent);
        void expand_condition(
                FTDGNode* n,
                std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> relevant_vars,
                std::deque<unsigned>& loops_ids,
                const std::string& indent);
        void expand_task(
                FTDGNode* ftdg_n,
                std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> relevant_vars,
                const std::deque<unsigned>& loops_ids,
                const std::string& indent);

        unsigned get_tdg_node_id(unsigned task_id, std::deque<unsigned> loops_ids);

        TDGNode* create_task_node(FTDGNode* n, const std::deque<unsigned>& loops_ids, const std::string& indent);
        void connect_task_node(TDGNode* tdg_n, FTDGNode* pcfg_n, const std::string& indent);
        bool compute_task_connections(
                TDGNode* possible_source,
                TDGNode* target,
                std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> target_vars_map,
                std::set<TDGNode*>& all_possible_ancestors,
                const std::string& indent);
        TDGNode* task_create_and_connect(
                FTDGNode* ftdg_n,
                const std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less>& relevant_vars,
                const std::deque<unsigned>& loops_ids,
                const std::string& indent);
        TDGNode* create_sync_node(FTDGNode* n, const std::string& indent);
        void connect_sync_node(TDGNode* tdg_n, const std::string& indent);
        void sync_create_and_connect(FTDGNode* n, const std::string& indent);

        void connect_nodes(TDGNode* source, TDGNode* target, const std::string& indent);
        void disconnect_nodes(TDGNode* source, TDGNode* target, const std::string& indent);

        bool is_ancestor(TDGNode* source, TDGNode* target);

        void remove_task_transitive_inputs(TDGNode* n);
        void remove_synchronizations_rec(TDGNode* n);
        void remove_synchronizations();

    public:
        SubTDG(
            unsigned maxI, unsigned maxT,
            unsigned parent_tdg_id, const std::vector<FTDGNode*>& outermost_nodes);

        void purge_subtdg();
        void expand_subtdg(
                std::map<NBase, const_value_t*, Nodecl::Utils::Nodecl_structural_less> relevant_vars,
                std::deque<unsigned> loops_ids);

        unsigned get_tdg_id() const;
        unsigned get_parent_tdg_id() const;
        void set_parent_tdg_id(unsigned parent_tdg_id);

        unsigned get_maxI() const;
        unsigned get_maxT() const;
        unsigned get_n_nodes() const;

        const ObjectList<TDGNode*>& get_roots() const;
        const std::set<TDGNode*>& get_leafs() const;

        const std::map<unsigned, TDGNode*>& get_tasks() const;
        unsigned get_num_tasks() const;


        const std::map<Nodecl::NodeclBase, ObjectList<TDGNode*> >& get_source_to_tdg_nodes() const;

        const std::vector<SubTDG*> get_nested_tdgs() const;
    };

    struct LoopInfo {
        Utils::InductionVar* _iv;
        NBase _lb;
        NBase _ub;
        NBase _incr;
        unsigned _niter;

        LoopInfo(Utils::InductionVar* iv, NBase lb, NBase ub, NBase incr, unsigned niter)
            : _iv(iv), _lb(lb), _ub(ub), _incr(incr), _niter(niter)
        {}
    };

    class LIBTL_CLASS TaskDependencyGraph
    {
    private:
        FlowTaskDependencyGraph* _ftdg;
        std::vector<SubTDG*> _tdgs;

        unsigned _maxI;
        unsigned _maxT;

        void compute_constants_rec(FTDGNode* n);
        void compute_constants();
        void expand_tdg();
        void add_global_variables();

        void print_tdg_to_dot_rec(TDGNode* n, std::ofstream& dot_tdg);

    public:
        // *** Constructor *** //
        TaskDependencyGraph(ExtensibleGraph* pcfg);

        // *** Getters and Setters *** //
        FlowTaskDependencyGraph* get_ftdg() const;
        const std::vector<SubTDG*>& get_tdgs() const;
        unsigned get_maxI() const;
        unsigned get_maxT() const;

        unsigned get_number_of_nodes() const;

        std::string get_name() const;

        // *** Printing methods *** //
        void print_tdg_to_dot();
    };

    // ********************** Task Dependency Graph ********************** //
    // ******************************************************************* //



    // ******************************************************************* //
    // ****************** Runtime Task Dependency Graph ****************** //

    class LIBTL_CLASS TaskDependencyGraphMapper
    {
    private:
        ObjectList<TaskDependencyGraph*> _tdgs;

    public:
        TaskDependencyGraphMapper(ObjectList<TaskDependencyGraph*> tdgs);

        void generate_runtime_tdg(bool tomp_lower_enabled);
    };

    // ****************** Runtime Task Dependency Graph ****************** //
    // ******************************************************************* //
}
}

#endif  // TL_TASK_DEPENDENCY_GRAPH_HPP
