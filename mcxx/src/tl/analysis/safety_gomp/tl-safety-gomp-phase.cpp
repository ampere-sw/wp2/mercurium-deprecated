/*--------------------------------------------------------------------
  (C) Copyright 2006-2013 Barcelona Supercomputing Center
                          Centro Nacional de Supercomputacion

  This file is part of Mercurium C/C++ source-to-source compiler.

  See AUTHORS file in the top level directory for information
  regarding developers and contributors.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3 of the License, or (at your option) any later version.

  Mercurium C/C++ source-to-source compiler is distributed in the hope
  that it will be useful, but WITHOUT ANY WARRANTY; without even the
  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
  PURPOSE.  See the GNU Lesser General Public License for more
  details.

  You should have received a copy of the GNU Lesser General Public
  License along with Mercurium C/C++ source-to-source compiler; if
  not, write to the Free Software Foundation, Inc., 675 Mass Ave,
  Cambridge, MA 02139, USA.
// --------------------------------------------------------------------*/

#include <sys/stat.h>

#include "cxx-cexpr.h"
#include "tl-builtin.hpp"
#include "tl-counters.hpp"
#include "tl-safety-gomp-phase.hpp"
#include "tl-analysis-base.hpp"
#include "tl-analysis-utils.hpp"
#include "tl-symbol-utils.hpp"

namespace TL {
namespace Analysis {

    // *********************************************************************************** //
    // ******************* Code transformations for compiling with GCC ******************* //

namespace {
    TL::Counter &task_id = TL::CounterManager::get_counter("safety-gomp-task-id");
    TL::Counter &tdg_counter = TL::CounterManager::get_counter("safety-gomp-tdg");

    Nodecl::ExpressionStatement create_func_call_stmt(
            std::string func_name,
            const ObjectList<std::string>& param_names,
            const ObjectList<Type>& param_types,
            const Nodecl::List& arguments)
    {
        Scope global_sc = Scope::get_global_scope();

        // 1.- Create a reference to the function (make sure we only create the symbol once)
        TL::Symbol func_sym = global_sc.get_symbol_from_name(func_name);
        if (!func_sym.is_valid())
        {
            func_sym = SymbolUtils::new_function_symbol(
                /*scope*/ global_sc,
                /*function name*/ func_name,
                /*return symbol name*/ "",
                /*return type*/ TL::Type::get_void_type(),
                /*parameter names*/ param_names,
                /*parameter types*/ param_types);
            // Make it external
            symbol_entity_specs_set_is_extern(func_sym.get_internal_symbol(), 1);
            symbol_entity_specs_set_is_static(func_sym.get_internal_symbol(), 0);
        }

        // 2.- Create a reference to the function symbol
        Nodecl::NodeclBase func_ref = Nodecl::Symbol::make(func_sym);
        func_ref.set_type(func_sym.get_type().get_lvalue_reference_to());

        // 3.- Create the call to the function
        Nodecl::FunctionCall func_call_expr = Nodecl::FunctionCall::make(
                func_ref,
                /* arguments */ arguments,
                /* alternate name */ Nodecl::NodeclBase::null(),
                /* function form */ Nodecl::NodeclBase::null(),
                Type::get_void_type());

        // 4.- Insert the call in the AST
        return Nodecl::ExpressionStatement::make(func_call_expr);
    }

    unsigned next_loop_iterator = 0;
    std::stack<unsigned> loop_iterators;
}

    /* **************************** Task Parts **************************** */



    /* *********************** GOMP Transformations *********************** */

namespace {

    bool In_single = false;

    void tokenizer(std::string str, std::set<std::string>& result)
    {
        std::string temporary("");
        for (std::string::const_iterator it = str.begin();
             it != str.end(); ++it)
        {
            const char & c(*it);
            if (c == ',' || c == ' ')
            {
                if (temporary != "")
                {
                    std::cerr << "   -> " << temporary << std::endl;
                    result.insert(temporary);
                    temporary = "";
                }
            }
            else
            {
                temporary += c;
            }
        }
        if (temporary != "")
        {
            result.insert(temporary);
        }
    }

    enum TraceKind {
        TraceRuntime,
        TraceTask,
        TraceTaskpart
    };

    Nodecl::ExpressionStatement create_call_to_tracepoint(
            /*starting point of the task part*/ bool init,
            /*tracepoint in a task*/ TraceKind trace_kind)
    {
        std::string func_call_name;
        if (trace_kind == TraceTask)
        {
            if (init)
                func_call_name = "GOMP_start_tracepoint_task";
            else
                func_call_name = "GOMP_stop_tracepoint_task";

            return create_func_call_stmt(
                    /*func name*/ func_call_name,
                    /*param names*/ ObjectList<std::string>(),
                    /*param type*/ ObjectList<Type>(),
                    /*arguments*/ Nodecl::List());
        }
        else if (trace_kind == TraceTaskpart)
        {
            if (init)
                func_call_name = "GOMP_start_tracepoint_taskpart";
            else
                func_call_name = "GOMP_stop_tracepoint_taskpart";

            return create_func_call_stmt(
                    /*func name*/ func_call_name,
                    /*param names*/ ObjectList<std::string>(),
                    /*param type*/ ObjectList<Type>(),
                    /*arguments*/ Nodecl::List());
        }
        else if (trace_kind == TraceRuntime)
        {
            if (init)
                func_call_name = "GOMP_start_tracepoint_runtime";
            else
                func_call_name = "GOMP_stop_tracepoint_runtime";

            Type t = Type::get_unsigned_int_type();
            Nodecl::IntegerLiteral GOMP_task_id = Nodecl::IntegerLiteral::make(
                    /*type*/ t,
                    /*value*/ const_value_get_integer(task_id, /*num_bytes*/4, /*sign*/0));
            return create_func_call_stmt(
                    /*func name*/ func_call_name,
                    /*param names*/ ObjectList<std::string>(1, "GOMP_task_id"),
                    /*param type*/ ObjectList<Type>(1, t),
                    /*arguments*/ Nodecl::List::make(GOMP_task_id));
        }
        else
        {
            internal_error("Unreachable code", 0);
        }
    }
}


    /* ************************ Transformations previous to Analysis ************************ */

    GOMPtransformationsBeforeAnalysis::GOMPtransformationsBeforeAnalysis(
            Instrumentation instrumentation, bool tdg_enabled, bool tomp_lower_enabled)
        : _instrumentation(instrumentation), _tdg_enabled(tdg_enabled), _tomp_lower_enabled(tomp_lower_enabled)
    {}

    void GOMPtransformationsBeforeAnalysis::unhandled_node(const Nodecl::NodeclBase& n)
    {
        WARNING_MESSAGE("Unhandled node of type '%s' while libGOMP transformations.\n",
                        ast_print_node_type(n.get_kind()));
    }

    // Add GOMP TDG set/unset runtime calls
    void GOMPtransformationsBeforeAnalysis::visit(const Nodecl::FunctionCode& n)
    {
        Nodecl::Context ctx = n.get_statements().as<Nodecl::Context>();
        Nodecl::List stmts = ctx.get_in_context().as<Nodecl::List>();

        // 1.- We only include calls to the GOMP library if the TDG is enabled and the loop contains some task
        if (_tdg_enabled && Nodecl::Utils::nodecl_contains_nodecl_of_kind<Nodecl::OpenMP::Task>(n))
        {
            task_id = 0;

            // 2.- Insert calls to gomp_set_tdg() and GOMP_unset_tdg_id()
            // 2.1.- Traverse the structure of the AST for a C/C++ Function
            Nodecl::CompoundStatement cmp_stmt = stmts.front().as<Nodecl::CompoundStatement>();
            Nodecl::List cmp_stmts = cmp_stmt.get_statements().as<Nodecl::List>();
            // 2.2.- Prepend set_id runtime call as the first statement of the function
            Type t = Type::get_unsigned_int_type();
            Nodecl::IntegerLiteral tdg_id = Nodecl::IntegerLiteral::make(
                    /*type*/ t,
                    /*value*/ const_value_get_integer(tdg_counter, /*num_bytes*/4, /*sign*/0));
            tdg_counter++;
            // gomp_set_tdg performs the following actions:
            // * allocates the TDG for using it in the runtime (only the first time it is called)
            // * sets the id of the tdg to be used in the runtime
            // This function will be defined in a file generated by Mapper
            // which will call the corresponding methods in the runtime.
            // NOTE: If gomp_set_tdg or tomp_set_tdg are to change names,
            //       then the code in tl-omp-gomp.cpp that inserts the call to GOMP_prealloc_tasks
            //       should change. Otherwise, it will not find where to insert that call.
            std::string func_name = (_tomp_lower_enabled ? "tomp_set_tdg" : "gomp_set_tdg");
            Nodecl::ExpressionStatement gomp_set_tdg_stmt = create_func_call_stmt(
                    /*func name*/ func_name,
                    /*param names*/ ObjectList<std::string>(1, "tdg_id"),
                    /*param type*/ ObjectList<Type>(1, t),
                    /*arguments*/ Nodecl::List::make(tdg_id));
            cmp_stmts.prepend(gomp_set_tdg_stmt);
            // 2.3.- Append unset_tdg runtime call as the last statement of the function
            // This call unpops the current TDG of the stack of TDGs (usefull when nested parallelism)
            Nodecl::NodeclBase last_stmt = cmp_stmts.back();
            Nodecl::ExpressionStatement gomp_unset_tdg_id_stmt = create_func_call_stmt(
                    /*func name*/ "GOMP_unset_tdg_id",
                    /*param names*/ ObjectList<std::string>(),
                    /*param type*/ ObjectList<Type>(),
                    /*arguments*/ Nodecl::List());
            if (last_stmt.is<Nodecl::ReturnStatement>())
            {
                last_stmt.prepend_sibling(gomp_unset_tdg_id_stmt);
            }
            else
            {
                last_stmt.append_sibling(gomp_unset_tdg_id_stmt);
            }
        }

        // 3.- Visit inner statements to apply other possible transformation (i.e. target id)
        walk(stmts);
    }

    void GOMPtransformationsBeforeAnalysis::visit(const Nodecl::OpenMP::For& n)
    {
        //internal_error("OpenMP loops are not supported.\n", 0);
    }

    void GOMPtransformationsBeforeAnalysis::visit(const Nodecl::OpenMP::Master& n)
    {
        In_single = true;
        if (_instrumentation == All)
        {
            // 1.- Insert taskpart runtime calls at the beginning and the end of the single list of statements
            Nodecl::Context ctx = n.get_statements().as<Nodecl::List>().front().as<Nodecl::Context>();
            Nodecl::List cmp_stmt = ctx.get_in_context().as<Nodecl::List>();
            Nodecl::List stmts;
            // If the statements of the task are inside a CompoundStatement,
            //    just insert the calls at the beginning and the end of the list
            // Otherwise, we have to wrap the unique statement of the task in a
            //    CompoundStatement before inserting the calls.
            if (cmp_stmt[0].is<Nodecl::CompoundStatement>())
            {
                stmts = cmp_stmt[0].as<Nodecl::CompoundStatement>().get_statements().as<Nodecl::List>();
            }
            else
            {
                stmts = cmp_stmt.shallow_copy().as<Nodecl::List>();
                cmp_stmt.replace(Nodecl::CompoundStatement::make(stmts, Nodecl::NodeclBase::null()));
            }
            if (!stmts.empty())
            {
                stmts.prepend(create_call_to_tracepoint(/*init*/true, /*tracekind*/TraceTaskpart));
                stmts.append(create_call_to_tracepoint(/*init*/false, /*tracekind*/TraceTaskpart));
            }
        }

        walk(n.get_statements());
        In_single = false;
    }

    void GOMPtransformationsBeforeAnalysis::visit(const Nodecl::OpenMP::Parallel& n)
    {
        // TODO So far, implicit tasks are not yet instrumented
        // If so, identifiers will be broken since the parallel construct does not have a task_id associated

//         // 1.- Insert tracing calls before and after the parallel region if runtime instrumentation is requested
//         //     This is the implicit task created in the parallel
//         if (_instrumentation == All)
//         {
//             // 1.- Prepend taskpart runtime call before the parallel
//             n.prepend_sibling(create_call_to_tracepoint(/*init*/false, /*tracekind*/TraceTaskpart));
//             n.prepend_sibling(create_call_to_tracepoint(/*init*/true, /*tracekind*/TraceRuntime));
//             n.append_sibling(create_call_to_tracepoint(/*init*/true, /*tracekind*/TraceTaskpart));
//             n.append_sibling(create_call_to_tracepoint(/*init*/false, /*tracekind*/TraceRuntime));
//         }
// 
//         if (_instrumentation == All)
//         {
//             // 2.- Insert tracing calls at the beginning and the end of the parallel region
//             Nodecl::Context ctx = n.get_statements().as<Nodecl::List>().front().as<Nodecl::Context>();
//             Nodecl::NodeclBase stmt = ctx.get_in_context().as<Nodecl::List>().front();
//             // Case: there is only one statement associated with the directive
//             //       and no CompoundStatement wraps it
//             // Behavior: Insert a CompoundStatement before inserting the calls to taskparts methods
//             if (!stmt.is<Nodecl::CompoundStatement>())
//             {
//                 Nodecl::CompoundStatement cmp_stmt =
//                         Nodecl::CompoundStatement::make(Nodecl::List::make(stmt.shallow_copy()),
//                                                         Nodecl::NodeclBase::null());
//                 stmt.replace(cmp_stmt);
//             }
//             Nodecl::CompoundStatement cmp_stmt = stmt.as<Nodecl::CompoundStatement>();
//             Nodecl::List cmp_stmts = cmp_stmt.get_statements().as<Nodecl::List>();
//             cmp_stmts.prepend(create_call_to_tracepoint(/*init*/true, /*tracekind*/TraceTaskpart));
//             cmp_stmts.append(create_call_to_tracepoint(/*init*/false, /*tracekind*/TraceTaskpart));
//          }

        // 4.- Traverse the statements of the parallel construct
        walk(n.get_statements());
    }

    void GOMPtransformationsBeforeAnalysis::visit(const Nodecl::OpenMP::Sections& n)
    {
        internal_error("OpenMP sections are not supported.\n", 0);
    }

    void GOMPtransformationsBeforeAnalysis::visit(const Nodecl::OpenMP::Single& n)
    {
        In_single = true;
        if (_instrumentation == All)
        {
            // 1.- Insert taskpart runtime calls at the beginning and the end of the single list of statements
            Nodecl::Context ctx = n.get_statements().as<Nodecl::List>().front().as<Nodecl::Context>();
            Nodecl::List cmp_stmt = ctx.get_in_context().as<Nodecl::List>();
            Nodecl::List stmts;
            // If the statements of the task are inside a CompoundStatement,
            //    just insert the calls at the beginning and the end of the list
            // Otherwise, we have to wrap the unique statement of the task in a
            //    CompoundStatement before inserting the calls.
            if (cmp_stmt[0].is<Nodecl::CompoundStatement>())
            {
                stmts = cmp_stmt[0].as<Nodecl::CompoundStatement>().get_statements().as<Nodecl::List>();
            }
            else
            {
                stmts = cmp_stmt.shallow_copy().as<Nodecl::List>();
                cmp_stmt.replace(Nodecl::CompoundStatement::make(stmts, Nodecl::NodeclBase::null()));
            }
            if (!stmts.empty())
            {
                stmts.prepend(create_call_to_tracepoint(/*init*/true, /*tracekind*/TraceTaskpart));
                stmts.append(create_call_to_tracepoint(/*init*/false, /*tracekind*/TraceTaskpart));
            }
        }

        // 2.- Visit single inner statements
        walk(n.get_statements());
        In_single = false;
    }

    void GOMPtransformationsBeforeAnalysis::visit(const Nodecl::OpenMP::TargetData& n)
    {
        internal_error("OpenMP target data is not supported.\n", 0);
    }

    void GOMPtransformationsBeforeAnalysis::visit(const Nodecl::OpenMP::TargetUpdate& n)
    {
        internal_error("OpenMP target update is not supported.\n", 0);
    }

    // Add the id clause
    void GOMPtransformationsBeforeAnalysis::visit(const Nodecl::OpenMP::Task& n)
    {
        if (In_single == false)
        {
            WARNING_MESSAGE("Task outside a Single construct. This is not supported in the Safety GOMP environment.\n", 0);
        }

        // 1.- Add the id clause to the task
        Nodecl::IntegerLiteral t_id = Nodecl::IntegerLiteral::make(
                Type::get_unsigned_int_type(),
                const_value_get_integer(++task_id, /*num_bytes*/4, /*sign*/0));
        Nodecl::OpenMP::TaskId task_id_clause = Nodecl::OpenMP::TaskId::make(t_id);
        n.get_environment().as<Nodecl::List>().append(task_id_clause);

        // 2.- Insert tracing calls before and after the parallel region if runtime instrumentation is requested
        if (_instrumentation == All)
        {
            n.prepend_sibling(create_call_to_tracepoint(/*init*/false, /*tracekind*/TraceTaskpart));
            n.prepend_sibling(create_call_to_tracepoint(/*init*/true, /*tracekind*/TraceRuntime));
            n.append_sibling(create_call_to_tracepoint(/*init*/true, /*tracekind*/TraceTaskpart));
            n.append_sibling(create_call_to_tracepoint(/*init*/false, /*tracekind*/TraceRuntime));
        }

        if (_instrumentation == Tasks || _instrumentation == All)
        {
            // 3.- Insert tracing calls at the beginning and the end of the task region
            Nodecl::Context ctx = n.get_statements().as<Nodecl::List>().front().as<Nodecl::Context>();
            Nodecl::NodeclBase stmt = ctx.get_in_context().as<Nodecl::List>().front();
            // Case: there is only one statement associated with the directive
            //       and no CompoundStatement wraps it
            // Behavior: Insert a CompoundStatement before inserting the calls to the instrumentation
            if (!stmt.is<Nodecl::CompoundStatement>())
            {
                Nodecl::CompoundStatement cmp_stmt =
                        Nodecl::CompoundStatement::make(Nodecl::List::make(stmt.shallow_copy()),
                                                        Nodecl::NodeclBase::null());
                stmt.replace(cmp_stmt);
            }
            Nodecl::CompoundStatement cmp_stmt = stmt.as<Nodecl::CompoundStatement>();
            Nodecl::List cmp_stmts = cmp_stmt.get_statements().as<Nodecl::List>();
            cmp_stmts.prepend(create_call_to_tracepoint(/*init*/true, /*tracekind*/TraceTask));
            cmp_stmts.append(create_call_to_tracepoint(/*init*/false, /*tracekind*/TraceTask));
        }

        // 2.- Traverse the code of the task
        walk(n.get_statements());
    }

    void GOMPtransformationsBeforeAnalysis::visit(const Nodecl::OpenMP::Taskwait& n)
    {
        // 1.- Insert tracing calls before and after the taskwait
        if (_instrumentation == All)
        {
            n.prepend_sibling(create_call_to_tracepoint(/*init*/false, /*tracekind*/TraceTaskpart));
            n.append_sibling(create_call_to_tracepoint(/*init*/true, /*tracekind*/TraceTaskpart));
        }
    }

    void GOMPtransformationsBeforeAnalysis::visit(const Nodecl::OpenMP::Taskyield& n)
    {
        internal_error("OpenMP task yield is not supported.\n", 0);
    }


    /* ********************** END Transformations previous to Analysis ********************** */


    /* ************************ Transformations posterior to Analysis *********************** */

    void GOMPtransformationsAfterAnalysis::unhandled_node(const Nodecl::NodeclBase& n)
    {
        WARNING_MESSAGE("Unhandled node of type '%s' while libGOMP transformations.\n",
                        ast_print_node_type(n.get_kind()));
    }

    // Add variable to count the number of iterations, necessary to compute the task id during OpenMP lowering
    void GOMPtransformationsAfterAnalysis::visit(const Nodecl::ForStatement& n)
    {
        // 1.- We only include calls to the GOMP library if the loop contains some task
        if (!Nodecl::Utils::nodecl_contains_nodecl_of_kind<Nodecl::OpenMP::Task>(n))
            return;

        // 2.- If the for loop is inside a taskgraph region, we insert loop iterators
        if (this->_is_in_taskgraph)
        {
            loop_iterators.push(next_loop_iterator++);

            // 2.1- Visit inner statements to apply transformation from inner to outer scopes
            Nodecl::NodeclBase for_stmt = n.get_statement();
            walk(for_stmt);

            // 2.2- Insert the symbol storing the loop iterator counter initialized to 0
            char gomp_iter_str[100];
            sprintf(gomp_iter_str, "_gomp_iter_%u", loop_iterators.top());
            type_t* gomp_iter_type = get_unsigned_int_type();
            TL::Scope gomp_iter_scope = n.retrieve_context().get_decl_context();

            TL::Symbol gomp_iter_var = gomp_iter_scope.new_symbol(gomp_iter_str);
            symbol_entity_specs_set_is_user_declared(gomp_iter_var.get_internal_symbol(), 1);
            gomp_iter_var.get_internal_symbol()->kind = SK_VARIABLE;
            gomp_iter_var.set_type(gomp_iter_type);
            gomp_iter_var.set_value(const_value_to_nodecl(const_value_get_zero(/*num_bytes*/ 4, /*sign*/ 1)));
            Nodecl::Symbol tmp_sym = Nodecl::Symbol::make(gomp_iter_var, n.get_locus());

            // 2.3- Insert the increment to the loop iterator just at the beginning of the loop
            Nodecl::Preincrement gomp_iter_inc = Nodecl::Preincrement::make(
                tmp_sym, TL::Type(gomp_iter_type), n.get_locus());
            Nodecl::Context for_stmt_ctx = for_stmt.as<Nodecl::List>()[0].as<Nodecl::Context>();
            // Note: we always have a CompoundStatement here because
            // either the input source has it
            // or the frontend introduces it
            Nodecl::CompoundStatement for_stmt_comp = for_stmt_ctx.get_in_context().as<Nodecl::List>()[0].as<Nodecl::CompoundStatement>();
            Nodecl::List for_stmts = for_stmt_comp.get_statements().as<Nodecl::List>();
            if (for_stmts.is_null())
            {
                Nodecl::List new_for_stmts = Nodecl::List::make(gomp_iter_inc);
                for_stmt_ctx.set_in_context(new_for_stmts);
            }
            else
            {
                for_stmts.insert(for_stmts.begin(), Nodecl::ExpressionStatement::make(gomp_iter_inc));
            }

            loop_iterators.pop();
        }else if (Nodecl::Utils::nodecl_contains_nodecl_of_kind<Nodecl::OpenMP::Taskgraph>(n)) {
                // 3.1- If the for loop is not inside a taskgraph region, but it contains a taskgraph, we parse the loop 
                Nodecl::NodeclBase for_stmt = n.get_statement();
                walk (for_stmt);
        }else {
                // 3.2- If the for loop is not inside a taskgraph region and it doesn't contain a taskgraph => warning message 
                // Ideally, this message should only be printed when "--tdg" is passed
                WARNING_MESSAGE("parsing a for loop containing tasks without taskgraph, "
                                "this may cause errors with --tdg compilation flag\n", 0);
        }
    }

    // Remove dependency clauses
    void GOMPtransformationsAfterAnalysis::visit(const Nodecl::OpenMP::Task& n)
    {
        // 1.- Remove the dependency clauses
        /*  Commented because we need the dependency information to record the tdg. This should be modified later to take TDG clause of taskgraph
        TL::ObjectList<Nodecl::NodeclBase> deps_to_remove;
        Nodecl::List environ = n.get_environment().as<Nodecl::List>();
        for (Nodecl::List::iterator it = environ.begin(); it != environ.end(); ++it)
        {
            if (it->is<Nodecl::OpenMP::DepIn>()
                || it->is<Nodecl::OpenMP::DepOut>()
                || it->is<Nodecl::OpenMP::DepInout>())
            {
                deps_to_remove.append(*it);
            }
        }
        for (TL::ObjectList<Nodecl::NodeclBase>::iterator it = deps_to_remove.begin();
             it != deps_to_remove.end(); ++it)
        {
            Nodecl::Utils::remove_from_enclosing_list(*it);
        }
        */

        // 2.- Traverse inner nodes to make further transformations, if needed (consider nested tasks)
        walk(n.get_statements());
    }

    // Remove dependency clauses
    void GOMPtransformationsAfterAnalysis::visit(const Nodecl::OpenMP::Taskgraph& n)
    {
        this->_is_in_taskgraph = true;

        // 1.- Traverse inner nodes to make further transformations, if needed (consider nested tasks)
        walk(n.get_statements());

        this->_is_in_taskgraph = false;
    }

    /* ********************** END Transformations posterior to Analysis ********************* */

    /* ****************************** END GOMP Transformations ****************************** */

    // ***************** END code transformations for compiling with GCC ***************** //
    // *********************************************************************************** //



    // *********************************************************************************** //
    // ********************** Phase for Safety GOMP transformations ********************** //

    SafetyGompPhase::SafetyGompPhase()
            : _tdg_enabled_str(""), _tdg_enabled(false),
              _gomp_lower_enabled_str(""), _gomp_lower_enabled(false),
              _tomp_lower_enabled_str(""), _tomp_lower_enabled(false),
              _ompss_mode_str(""), _ompss_mode_enabled(false),
              _functions_str(""), _call_graph_str(""), _call_graph_enabled(true),
              _instrumentation_str(""), _instrumentation(None)
    {
        set_phase_name("Phase for P-Socrates project transformations");

        register_parameter("tdg_enabled",
                           "If set to '1' enables tdg expansion, otherwise it is disabled",
                           _tdg_enabled_str,
                           "0").connect(std::bind(&SafetyGompPhase::set_tdg, this, std::placeholders::_1));

        register_parameter("gomp_lower_enabled",
                           "Enables lowering for Gomp RTL",
                           _gomp_lower_enabled_str,
                           "0").connect(std::bind(&SafetyGompPhase::set_gomp_lower, this, std::placeholders::_1));

        register_parameter("tomp_lower_enabled",
                           "Enables lowering for Tomp RTL",
                           _tomp_lower_enabled_str,
                           "0").connect(std::bind(&SafetyGompPhase::set_tomp_lower, this, std::placeholders::_1));

        register_parameter("ompss_mode",
                           "Enables OmpSs semantics instead of OpenMP semantics",
                           _ompss_mode_str,
                           "0").connect(std::bind(&SafetyGompPhase::set_ompss_mode, this, std::placeholders::_1));

        register_parameter("functions",
                           "Points out the function that has to be analyzed",
                           _functions_str,
                           "").connect(std::bind(&SafetyGompPhase::set_functions, this, std::placeholders::_1));

        register_parameter("call_graph",
                           "If set to '1' enables analyzing the call graph of all functions specified in parameter 'functions'",
                           _call_graph_str,
                           "1").connect(std::bind(&SafetyGompPhase::set_call_graph, this, std::placeholders::_1));

        register_parameter("instrumentation",
                           "Values accepted are: 'none', 'tasks' and 'all'\n"
                           "  - If set to 'none', no instrumentation is inserted\n"
                           "  - If set to 'tasks', only code within implicit tasks is instrumented\n"
                           "  - If set to 'all', both code in implicit tasks and runtime overhead in implicit tasks are implemented",
                           _instrumentation_str,
                           "none").connect(std::bind(&SafetyGompPhase::set_instrumentation, this, std::placeholders::_1));
    }

    static unsigned get_max_tdg_size(const ObjectList<TaskDependencyGraph*>& tdgs)
    {
        unsigned max_number_of_nodes = 0;
        for (ObjectList<TaskDependencyGraph*>::const_iterator it = tdgs.begin();
             it != tdgs.end(); ++it)
        {
            unsigned current_number_of_nodes = (*it)->get_number_of_nodes();
            if (current_number_of_nodes > max_number_of_nodes)
                max_number_of_nodes = current_number_of_nodes;
        }
        return max_number_of_nodes;
    }

    void SafetyGompPhase::run(TL::DTO& dto)
    {
        ERROR_CONDITION((_tomp_lower_enabled && _gomp_lower_enabled),
                        "Lowering for GOMP and TI cannot be performed at the same time. "
                        "Choose --gomp-lower or --tomp-lower.\n", 0);

        if (!_gomp_lower_enabled && !_tomp_lower_enabled)
        {
            std::cerr << "[Safety Gomp] Nor Gomp neither Tomp lowering specified. Using Gomp by default." << std::endl;
        }
        AnalysisBase analysis(_ompss_mode_enabled);

        Nodecl::NodeclBase ast = *std::static_pointer_cast<Nodecl::NodeclBase>(dto["nodecl"]);

        // 1.- LibGOMP transformations previous to analysis
        GOMPtransformationsBeforeAnalysis gompt_pre(_instrumentation, _tdg_enabled, _tomp_lower_enabled);
        gompt_pre.walk(ast);

        // 2.- Static generation of the Task Dependency Graph
        ObjectList<TaskDependencyGraph*> tdgs;

        // _functions_str is a comma-separated list of function names
        // Transform it into a set of strings
        std::set<std::string> functions;
        tokenizer(_functions_str, functions);

        // debugging purposes
        if (debug_options.print_pcfg ||
            debug_options.print_pcfg_w_context ||
            debug_options.print_pcfg_w_analysis ||
            debug_options.print_pcfg_full)
        {
            if (VERBOSE)
                std::cerr << "==========  Generating and printing PCFG to dot file  ==========" << std::endl;
            analysis.parallel_control_flow_graph(ast, functions, _call_graph_enabled);
            ObjectList<ExtensibleGraph*> pcfgs = analysis.get_pcfgs();
            for (ObjectList<ExtensibleGraph*>::iterator it = pcfgs.begin(); it != pcfgs.end(); ++it)
                (*it)->print_graph_to_dot();
            if (VERBOSE)
                std::cerr << "========  Generating and printing PCFG to dot file done  =======" << std::endl;
        }

        if (_tdg_enabled)
        {
            if (VERBOSE)
                std::cerr << "====================  Testing TDG creation  ====================" << std::endl;
            tdgs = analysis.task_dependency_graph(ast, functions, _call_graph_enabled, _tomp_lower_enabled);
            dto.set_object("tdg_width", std::shared_ptr<Integer>(new Integer(get_max_tdg_size(tdgs))));
            if (VERBOSE)
                std::cerr << "==================  Testing TDG creation done  =================" << std::endl;
        }

        if (debug_options.print_tdg)
        {
            if (!_tdg_enabled)
            {
                WARNING_MESSAGE("TDG cannot be printed. Add --tdg to generate the TDG.", 0);
            }
            else
            {
                if (VERBOSE)
                    std::cerr << "==================  Printing TDG to dot file  =================" << std::endl;
                for (ObjectList<TaskDependencyGraph*>::iterator it = tdgs.begin(); it != tdgs.end(); ++it)
                    analysis.print_tdg((*it)->get_name());
                if (VERBOSE)
                    std::cerr << "===============  Printing TDG to dot file done  ===============" << std::endl;
            }
        }

        // 3.- LibGOMP transformations posterior to analysis
        GOMPtransformationsAfterAnalysis gompt_post;
        gompt_post.walk(ast);

        // 4.- Add declare target pragmas if lowering to TI hardware
        if (_tomp_lower_enabled)
        {
            Nodecl::UnknownPragma begin_declare_target = Nodecl::UnknownPragma::make("omp declare target");
            Nodecl::UnknownPragma end_declare_target = Nodecl::UnknownPragma::make("omp end declare target");
            Nodecl::List top_level_list = ast.as<Nodecl::TopLevel>().get_top_level().as<Nodecl::List>();
            top_level_list.prepend(begin_declare_target);
            top_level_list.append(end_declare_target);
        }
    }

    void SafetyGompPhase::set_tdg(const std::string& tdg_enabled_str)
    {
        if (tdg_enabled_str == "1")
            _tdg_enabled = true;
    }

    void SafetyGompPhase::set_gomp_lower(const std::string& gomp_lower_enabled_str)
    {
        if (gomp_lower_enabled_str == "1")
            _gomp_lower_enabled = true;
    }

    void SafetyGompPhase::set_tomp_lower(const std::string& tomp_lower_enabled_str)
    {
        if (tomp_lower_enabled_str == "1")
            _tomp_lower_enabled = true;
    }

    void SafetyGompPhase::set_ompss_mode(const std::string& ompss_mode_str)
    {
        if (ompss_mode_str == "1")
            _ompss_mode_enabled = true;
    }

    void SafetyGompPhase::set_functions(const std::string& functions_str)
    {
        if (functions_str != "")
            _functions_str = functions_str;
    }

    void SafetyGompPhase::set_call_graph(const std::string& call_graph_enabled_str)
    {
        if (call_graph_enabled_str == "0")
            _call_graph_enabled = false;
    }

    void SafetyGompPhase::set_instrumentation(const std::string& instrumentation_str)
    {
        if (instrumentation_str == "none")
        {
            _instrumentation = None;
        }
        else if (instrumentation_str == "tasks")
        {
            _instrumentation = Tasks;
        }
        else if (instrumentation_str == "all")
        {
            _instrumentation = All;
        }
        else
        {
            WARNING_MESSAGE("Unexpected value '%s' in parameter 'instrumentation'. "
                            "Only 'none', 'tasks' and 'all' are accepted.\n",
                            instrumentation_str.c_str());
        }
    }

    // ******************** END Phase for Safety GOMP transformations ******************** //
    // *********************************************************************************** //
}
}

EXPORT_PHASE(TL::Analysis::SafetyGompPhase);
