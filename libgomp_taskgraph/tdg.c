/* This file along with tdg.h provide TDG recording, static TDG executing features
   that we intent to add in libgomp. */

#ifndef TDG_C
 #define TDG_C

/* Some functions in tdg.h use funcions in stdlib.h and string.h,
    which are not included in all translation units. Hence,
    the preprocessor TDG_C is read in tdg.h to trigger inclusion of those functions.
    TDG_C has to be defined before including libgomp.h, because libgomp.h includes
    tdg.h */
#include <string.h>
#include "libgomp.h"
//IO debugging dynamic tdg
#include <stdio.h>

#endif  //TDG_C

//#include "tdg.h"

/* general TDG related */
int curr_tdg_g = -1;									  // index on current tdg
unsigned int num_tdgs_g;							  // total number of tdgs of the program
struct gomp_tdg_node **tdg_g;						// tdg matrix of the program => 1 row = 1 tdg
unsigned short **tdg_ins_g;						  // input dependency matrix of the tdg matrix
unsigned short **tdg_outs_g;						// output dependecy matrix of the tdg matrix
unsigned int *tdg_ntasks_g;						  // number of tasks in different tdg
unsigned int *maxI_g;									  // maximum number of iterations of any loop containing #pragma omp tasks
unsigned int *maxT_g;									  // number of tasks constructs (#pragma omp tasks) of the program
struct root_list *roots = {0};          // the list of roots in a graph

/* dynamic TDG */
struct htab *depend_hash_g = NULL;             // hash table handling dependency information for dynamic TDG
tdg_hash_table *tdg_htab_g = NULL;             // hash table handling tdg record functions
unsigned int num_output_g = 0;          // size of output array
unsigned int num_tasks_g = 0;           // size of tdg array
struct data_to_free *beginning_g = NULL;
struct data_to_free *prev_g = NULL;
bool record_g = false;


/* ---------- declaration to include hashtab.h (start)---------- */
typedef struct gomp_task_depend_entry *hash_entry_type;

static inline void *
htab_alloc (size_t size)
{
  return gomp_malloc (size);
}

static inline void
htab_free (void *ptr)
{
  free (ptr);
}

#include "hashtab.h"

static hash_entry_type
htab_find (htab_t htab, const hash_entry_type element) __attribute__ ((unused));

static inline hashval_t
htab_hash (hash_entry_type element)
{
  return hash_pointer (element->addr);
}

static inline bool
htab_eq (hash_entry_type x, hash_entry_type y)
{
  return x->addr == y->addr;
}

/* ---------- declaration to include hashtab.h (end)---------- */

void init_tdg_node (struct gomp_tdg_node *tdg_node, struct gomp_tdg_task *tdg_task)
{
  tdg_node->task = tdg_task;
  tdg_node->id = num_tasks_g++;
}

void init_tdg_task (struct gomp_tdg_task *tdg_task, struct gomp_tdg_node *tdg_node,
                    struct gomp_task *task)
{
  tdg_task->priority = task->priority;
  tdg_task->tdg = tdg_node;
  tdg_task->kind = task->kind;
  tdg_task->parent_depends_on = task->parent_depends_on;
  tdg_task->in_tied_task = task->in_tied_task;
  tdg_task->parent = task->parent;
  tdg_task->fn = task->fn;
  tdg_task->final_task = task->final_task;
}

/* free START, a linked list */
static inline void free_data (struct data_to_free *start)
{
    struct data_to_free *tmp;
    if (!start)
      return ;

    while (start)
    {
      free (start->to_free->dependers);
      free (start->to_free);
      tmp = start;
      start = start->next;
      free (tmp);
    }
}

static void free_hash () __attribute__ ((unused));
static void free_hash ()
{
  free_data (beginning_g);
  free (depend_hash_g);
}


/* This function is called when dynamic recording is active, it will
 * capture the dependency information of the task, create necessary structures
 * to store the dependency information in the global hash_table and 
 * also put the independent tasks in _roots_, the global variable 
 * keeping all tasks without input dependency, TDG execution starts with _roots_
 * */
void gomp_task_handle_tdg_depend (struct gomp_task *task, struct gomp_task *parent,
                                  size_t depend_size, long arg_size, long arg_align,
                                  void **depend)
{
  size_t ndepend = 0;
  size_t nout = 0;
  hash_entry_type ent;
  size_t i;
  struct gomp_task *local_task = NULL;

  if (depend_hash_g == NULL && depend)
    depend_hash_g = htab_create (2 * ndepend > 12 ? 2 * ndepend : 12);

  /* local_task will be freed in <build_TDG_from_hash>, tdg_task and tdg_node will remain in memory for
     later execution of the TDG.*/
  if (depend)
  {
    local_task = (struct gomp_task *)gomp_malloc (sizeof(struct gomp_task) +
                                            depend_size + arg_size + arg_align - 1);

    // store @local_task, to be freed in free_hash
    struct data_to_free *task_to_free =  (struct data_to_free *)gomp_malloc (sizeof(struct data_to_free));
    task_to_free -> to_free = local_task;
    if (prev_g)
    {
      prev_g->next = task_to_free;
      prev_g = task_to_free;
    }else{
      beginning_g = task_to_free;
      prev_g = task_to_free;
    }


    memcpy (local_task, task, sizeof(struct gomp_task) + depend_size + arg_size + arg_align - 1);      // copy the task to local_task, set fn_data to the correct place

    local_task->fn_data = (char *)(((uintptr_t)(local_task + 1) + depend_size + arg_align - 1) &
                         ~(uintptr_t) (arg_align - 1));
    ndepend = (uintptr_t)depend[0];
    nout = (uintptr_t)depend[1];
  }
  struct gomp_tdg_task *local_tdg_task = (struct gomp_tdg_task *)gomp_malloc (sizeof (struct gomp_tdg_task) +
                                                    arg_size + arg_align -1);
  // most of the node's attributes are 0, leading us to allocate with calloc or memset
  struct gomp_tdg_node *local_tdg_node = (struct gomp_tdg_node *)gomp_malloc (sizeof (struct gomp_tdg_node));
  memset (local_tdg_node, 0, sizeof(struct gomp_tdg_node));

  // setting local_tdg_task, local_tdg_node
  init_tdg_node (local_tdg_node, local_tdg_task);

  init_tdg_task (local_tdg_task, local_tdg_node, task);

  char *arg = (char *)(((uintptr_t)(local_tdg_task+1) + arg_align - 1) &
                         ~(uintptr_t) (arg_align - 1));
  memcpy (arg, task->fn_data, arg_size);
  local_tdg_task->fn_data = arg;

  if (!local_task)
  {
      // if the task is independent on any variable, we only create tdg_node and tdg_task with the
      // correct data, then insert it roots
      struct root_list *root_node = (struct root_list *)gomp_malloc (sizeof(struct root_list));
      root_node->task_u = (union task_union_t *)local_tdg_task;
      root_node->next = roots;
      roots = root_node;
      return;
  }


  local_task->tdg = local_tdg_node;
  /* num_dependees is already updated in gomp_task_handle_depend,
     increments would have been done twice if not setting it to 0 */
  local_task->num_dependees = 0;

  for (i = 0; i < ndepend; i++)
  {
    local_task->depend[i].addr = depend[2 + i];
    local_task->depend[i].next = NULL;
    local_task->depend[i].prev = NULL;
    local_task->depend[i].task = local_task;
    local_task->depend[i].is_in = i >= nout;
    local_task->depend[i].redundant = false;
    local_task->depend[i].redundant_out = false;

    hash_entry_type *slot = htab_find_slot (&depend_hash_g,
                                            &local_task->depend[i], INSERT);
    hash_entry_type out = NULL, last = NULL;
    if (*slot)
    {
      /* If multiple depends on the same task are the same, all but the
         first one are redundant.  As inout/out come first, if any of them
         is inout/out, it will win, which is the right semantics.  */
      if ((*slot)->task == local_task)
      {
        local_task->depend[i].redundant = true;
        continue;
      }
      for (ent = *slot; ent; ent = ent->next)
      {
        if (ent->redundant_out)
          break;

        last = ent;

        /* depend(in:...) doesn't depend on earlier depend(in:...).  */
        if (i >= nout && ent->is_in)
          continue;

        if (!ent->is_in)
          out = ent;

        /* The following test1 should not create
           or delete transitive dependencies, in cases:
        *  T1 (out:x1) -> T2 (in:x1) -> T3 (out:x1) will also create
           T1->T3 as dependency, which is unnecessary
           */

        if (i < nout && ent->is_in) // test1
          ent->redundant_out = true;
        

        struct gomp_task *tsk = ent->task;
        if (tsk->dependers == NULL)
	{
          tsk->dependers = gomp_malloc (sizeof (struct gomp_dependers_vec)
				        + 6 * sizeof (struct gomp_task *));
	  tsk->dependers->n_elem = 1;
    num_output_g++;
	  tsk->dependers->allocated = 6;
	  tsk->dependers->elem[0] = local_task;
	  local_task->num_dependees++;
	  continue;
	}
	/* We already have some other dependency on tsk from earlier
	   depend clause.  */
	else if (tsk->dependers->n_elem
		       && (tsk->dependers->elem[tsk->dependers->n_elem - 1]
			   == local_task))
		continue;
	else if (tsk->dependers->n_elem == tsk->dependers->allocated)
	{
	  tsk->dependers->allocated = tsk->dependers->allocated * 2 + 2;
	  tsk->dependers = gomp_realloc (tsk->dependers,
				    sizeof (struct gomp_dependers_vec)
				    + (tsk->dependers->allocated
				       * sizeof (struct gomp_task *)));
	}
	tsk->dependers->elem[tsk->dependers->n_elem++] = local_task;
  num_output_g++;
	local_task->num_dependees++;
      }
      local_task->depend[i].next = *slot;
      (*slot)->prev = &local_task->depend[i];
    }
    *slot = &local_task->depend[i];

      /* There is no need to store more than one depend({,in}out:) task per
	 address in the hash table chain for the purpose of creation of
	 deferred tasks, because each out depends on all earlier outs, thus it
	 is enough to record just the last depend({,in}out:).  For depend(in:),
	 we need to keep all of the previous ones not terminated yet, because
	 a later depend({,in}out:) might need to depend on all of them.  So, if
	 the new task's clause is depend({,in}out:), we know there is at most
	 one other depend({,in}out:) clause in the list (out).  For
	 non-deferred tasks we want to see all outs, so they are moved to the
	 end of the chain, after first redundant_out entry all following
	 entries should be redundant_out.  */
      if (!local_task->depend[i].is_in && out)
	{
	  if (out != last)
	    {
	      out->next->prev = out->prev;
	      out->prev->next = out->next;
	      out->next = last->next;
	      out->prev = last;
	      last->next = out;
	      if (out->next)
		out->next->prev = out;
	    }
	  out->redundant_out = true;
	}
    }

    local_tdg_node->cnt = local_task->num_dependees;
    local_tdg_node->nin = local_task->num_dependees;

    if (local_task->num_dependees == 0)
    {
      struct root_list *root_node = (struct root_list *)gomp_malloc (sizeof(struct root_list));
      root_node->task_u = (union task_union_t *)local_tdg_task;
      root_node->next = roots;
      roots = root_node;
    }
}

int clear_transitive_edges (struct gomp_task *task)
{
  #ifdef _STATIC_TDG_CHECKING_
    if (!task->dependers)
      gomp_fatal ("in build_TDG_from_hash::clear_transitive_edges, task->dependers should not be empty");
  #endif

  int deleted = 0;
  bool next = false;
  /* decreasing order because T1->T3 is unnecessary only if
     T1->T2, T2->T3 exist, therefore, T3 is created after T2,
     according to insertion order to "T1->dependers", T3 is always
     behind T2 */
  for (int i = task->dependers->n_elem-1; i > 0; i--)
  {
    struct gomp_tdg_node *tdg_node = task->dependers->elem[i]->tdg;
    int id = tdg_node->id;
    for (int j = i-1; j >= 0; j--)
    {
      struct gomp_task *tsk = task->dependers->elem[j];
      if (tsk->dependers)
      {
        for (int k = 0; k < tsk->dependers->n_elem; k++)
        {
          if (tsk->dependers->elem[k]->tdg->id == id)
          {
            task->dependers->n_elem--;
            task->dependers->elem[i] = NULL;
            /* tdg having ID also decrements its input counter
            the gomp_task's num_dependees not needed to modify */
            tdg_g[curr_tdg_g][id].nin --;
            tdg_g[curr_tdg_g][id].cnt --;
            deleted ++;
            next = true;
            break;
          }
        }
        if (next)
          break;
      }
    }
  }
  return deleted;
}


void build_TDG_from_hash ()
{
  printf ("recording...\n");
  
  /* initializing the id of the tdg to use, need to be in an initializer such as GOMP_set_tdg_id,
     but since record_tdg is launched without call to this function, we initialise curr_tdg_g here */
  curr_tdg_g = 0;

  if (!tdg_outs_g)
    tdg_outs_g = (unsigned short **)gomp_malloc (sizeof(unsigned short *) * 1);
  if (!tdg_g)
    tdg_g = (struct gomp_tdg_node **)gomp_malloc (sizeof(struct gomp_tdg_node *) * 1);
  if (!tdg_ntasks_g)
  {
    tdg_ntasks_g = (unsigned int*) gomp_malloc (sizeof(unsigned int) * 1);
    tdg_ntasks_g[0] = num_tasks_g;
  }

  /* we will be testing if element == NULL, better set the memory to 0 */
  tdg_outs_g[curr_tdg_g] = (unsigned short *)gomp_malloc (sizeof(unsigned short) * num_output_g);
  tdg_g[curr_tdg_g] = (struct gomp_tdg_node *)gomp_malloc_cleared(num_tasks_g * sizeof(struct gomp_tdg_node));

  size_t i,j;
  hash_entry_type ent;
  unsigned int node_in_tdg = 0;
  unsigned int tdg_id;
  struct gomp_task *task;

  unsigned int i_output = 0;

  for (i = 0; i < depend_hash_g->size; i++)
  {
    ent = depend_hash_g->entries[i];
    if (ent)
    {
      for (; ent; ent = ent->next)
      {
        task = ent->task;
        tdg_id = task->tdg->id;
        if (!tdg_g[curr_tdg_g][tdg_id].task)
        {
          task->tdg->offout = (unsigned short) i_output;
           //previous clear_transitive_edges might have changed nin and cnt, save and update are necessary
          char num_inputs = tdg_g[curr_tdg_g][tdg_id].nin;
          signed char cnt = tdg_g[curr_tdg_g][tdg_id].cnt;    
          memcpy (&tdg_g[curr_tdg_g][tdg_id], task->tdg, sizeof(struct gomp_tdg_node));
          tdg_g[curr_tdg_g][tdg_id].nin += num_inputs;
          tdg_g[curr_tdg_g][tdg_id].cnt += cnt;

          node_in_tdg++;

          if (!task->dependers)
          {
            tdg_g[curr_tdg_g][tdg_id].nout = 0;
            continue;
          }
          else
          {
            int num_edges_deleted = 0;
            num_edges_deleted = clear_transitive_edges (ent->task);
            num_output_g = num_output_g - num_edges_deleted;
            tdg_g[curr_tdg_g][tdg_id].nout = task->dependers->n_elem;
          }

          for (j=0; j < task->dependers->n_elem; j++)
            if (task->dependers->elem[j])
              tdg_outs_g[curr_tdg_g][i_output++] = task->dependers->elem[j]->tdg->id;

          /* task and task->dependers cannot be freed here because other
           * task, such as T0 in the depend_hash_g might consult task->dependers
           * later */
          // free (task->dependers); //*TODO*: free it in free_hash
          free (task->tdg);
          task->tdg = &tdg_g[curr_tdg_g][tdg_id];
          tdg_g[curr_tdg_g][tdg_id].task->tdg = &tdg_g[curr_tdg_g][tdg_id];
        }
        if (node_in_tdg >= num_tasks_g)
          break;
      }
    }
    if (node_in_tdg >= num_tasks_g)
      break;
  }
  free_hash ();
}

/*
void GOMP_record_tdg (void (*fn) (void *), void *data)
{
  // is_first_$num should be calculated, so far only one TDG
  unsigned int is_first = *(unsigned int *)data;
  if (is_first)
  {
    record_g = true;
    fn(data);
    record_g = false;
  }
  GOMP_exec_tdg();
}
*/
int
gomp_tdg_get_unresolved_dependencies (struct gomp_tdg_node *tdg)
{
  int n_dep=(int)(tdg->cnt);
  // int n_dep = (int) __atomic_load_n(&tdg->cnt, MEMMODEL_ACQUIRE);
#ifdef DEBUG_GOMP_MAP
    if(!n_dep)
        printf("[gomp_get_unresolved_dependencies] - Task id %lu ready to execute\n",tdg->id);
    else
        printf("[gomp_get_unresolved_dependencies] - Task id %lu with %d dependencies unsolved. Waiting...\n",tdg->id,n_dep);
#endif
    return n_dep;
}

void gomp_update_tdg(struct gomp_tdg_task *task, struct gomp_tdg_node *tdg)
{
  tdg->task = task;

  /* Why not tdg->cnt = tdg->nin?
     Ans: cnt can be updated before this method is called
     by the corresponding TASK. E.g, when first task finishes,
     its tdg can update the dependecy information of tdg_2,
     tdg_3, which are dependent on it. But task_2 and task_3
     might not be created yet. By simply doing tdg->cnt = tdg->nin,
     we'll lose these updates of dependecy. */
  tdg->cnt += 1 + tdg->nin;
}

/* return the number of tasks become ready to execute (no more input dependencies unsatisfied)
   after *task* finishes its execution */
int gomp_update_tdg_dependency(struct gomp_tdg_task *task)
{
  int i;
  unsigned int tasks_ready = 0;

  // we do not want to update this value, since the task should be ready to execute again.
  // task->tdg->cnt = -1;      // calling task has finished

  unsigned int num_tasks_depend_on_this_task = (int) task->tdg->nout;
  unsigned short index_tdg_node_depends_on_this_node;
  struct gomp_thread *thr = gomp_thread ();
  struct gomp_team *team = thr->ts.team;
  struct gomp_task *parent = task->parent;
  struct gomp_tdg_node *tdg;

  for(i=0; i<num_tasks_depend_on_this_task; i++)
  {
    index_tdg_node_depends_on_this_node = tdg_outs_g[curr_tdg_g][task->tdg->offout + i];

    // if a descendant task is created and now ready to execute, insert it to team->task_queue
    if ((--tdg_g[curr_tdg_g][index_tdg_node_depends_on_this_node].cnt == 0)
        &&(tdg_g[curr_tdg_g][index_tdg_node_depends_on_this_node].task))
    {
      tdg = &tdg_g[curr_tdg_g][index_tdg_node_depends_on_this_node];

      tdg->cnt = tdg->nin;
      /* copied from gomp_task_run_post_handle_dependers */
      if (parent)
      {
        /* Mandatory, since after first execution, kind is set to TIED,
           so the task is not interpreted as waiting to execute */
        tdg->task->kind = GOMP_TASK_WAITING;

        priority_queue_gen_insert (PQ_CHILDREN, &parent->children_queue,
          (union task_union_t *)tdg->task, task->priority,
          PRIORITY_INSERT_BEGIN,
          /*adjust_parent_depends_on=*/true,
          task->parent_depends_on,
          GOMP_STATIC_TDG);
        if (parent->taskwait)
        {
          if (parent->taskwait->in_taskwait)
          {
            /* One more task has had its dependencies met.
            Inform any waiters.  */
            parent->taskwait->in_taskwait = false;
            gomp_sem_post (&parent->taskwait->taskwait_sem);
          }
          else if (parent->taskwait->in_depend_wait)
          {
            /* One more task has had its dependencies met.
            Inform any waiters.  */
            parent->taskwait->in_depend_wait = false;
            gomp_sem_post (&parent->taskwait->taskwait_sem);
          }
        }
      }

      priority_queue_gen_insert (PQ_TEAM, &team->task_queue,
			     (union task_union_t *)tdg->task, tdg->task->priority,
           PRIORITY_INSERT_END,
           /*adjust_parent_depends_on=*/false,
           tdg->task->parent_depends_on,
           GOMP_STATIC_TDG);
      ++team->task_count;
      ++team->task_queued_count;
      ++tasks_ready;
    }
  }

  if(tasks_ready > 1)           // if = 1, current thread will do the job
    gomp_team_barrier_set_task_pending (&team->barrier);

  return tasks_ready;
}

/* uncomplete yet, lack some arguments */
void GOMP_fill_task_data(unsigned instance_id, void (*fn) (void *), void *data,
                         long arg_size, long arg_align, int priority)
{
  num_tasks_g++;
  struct gomp_thread *thr = gomp_thread ();
  //struct gomp_team *team = thr->ts.team;
  struct gomp_task *parent = thr->task;
  struct gomp_tdg_node *tdg = gomp_get_tdg (instance_id);
  struct gomp_tdg_task *tdg_task = gomp_malloc (sizeof (struct gomp_tdg_task) + arg_size + arg_align - 1);

  char *arg = (char *) (((uintptr_t) (tdg_task + 1) + arg_align - 1)
                                & ~(uintptr_t) (arg_align - 1));

  memcpy (arg, data, arg_size);

  tdg_task->tdg = tdg;
  tdg_task->priority = priority;
  tdg_task->parent_depends_on = false;                                    // only false now, to set to true if parent_depends_on, but in this case, dependency handled by TDG
  tdg_task->kind = GOMP_TASK_WAITING;
  tdg_task->in_tied_task = false;
  tdg_task->fn = fn;
  tdg_task->fn_data = arg;
  tdg_task->parent = parent;

  // update tdg->task and tdg->cnt
  gomp_update_tdg(tdg_task, tdg);

  /* modified for multiple root graph (such as axpy) */
  if (tdg->cnt == 0)
  {
    struct root_list *root_node = (struct root_list *)gomp_malloc (sizeof(struct root_list));
    root_node->task_u = (union task_union_t *)tdg_task;
    root_node->next = roots;
    roots = root_node;
  }
}

void GOMP_exec_tdg (void (*fn)(void *), void *data)
{
  struct gomp_thread *thr = gomp_thread ();
  struct gomp_team *team = thr->ts.team;
  struct gomp_task *parent = thr->task;
  int task_to_start = 0;

  if (!tdg_htab_g)
    tdg_htab_g = init_tdg_func_hash_tab();

  if (fn && !tdg_hash_find (tdg_htab_g, fn))
  {
    record_g = true;
    fn (data);
    record_g = false;
  }

  //struct root_list *to_free = NULL;
  struct root_list *tmp = roots;
  if (depend_hash_g && !tdg_g)
    build_TDG_from_hash ();

  gomp_mutex_lock (&team->task_lock);
  while(roots)
  {
      /* We cannot free the roots if we want to execute the TDG later on
      if (to_free)
          free (to_free);
      */
      //struct gomp_tdg_node *tdg = gomp_get_tdg (roots->id);
      union task_union_t *task = (union task_union_t *)roots->task_u;


    #if _STATIC_TDG_CHECKING_
      // if roots is a tdg_task and it has unsolved dependencies, something went wrong
      if (task->tdg_task.tdg && task->tdg_task.tdg->cnt != 0)
        gomp_fatal ("The root has dependecy unsolved?");
    #endif

    /* Mandatory, since after first execution, kind is set to TIED,
       so the task is not interpreted as waiting to execute */
      task->task.kind = GOMP_TASK_WAITING;

      /* function only executed by 1 thread, region not locked.
         Consider to lock in case of need  */
      if (parent)
        priority_queue_gen_insert (PQ_CHILDREN, &parent->children_queue,
                                   task, task->tdg_task.priority,
                                   PRIORITY_INSERT_BEGIN,
                                   /*adjust_parent_depends_on=*/false,
                                   task->tdg_task.parent_depends_on,
                                   GOMP_STATIC_TDG);

      priority_queue_gen_insert (PQ_TEAM, &team->task_queue,
                                 task, task->tdg_task.priority,
                                 PRIORITY_INSERT_END,
                                 /*adjust_parent_depends_on=*/false,
                                 task->tdg_task.parent_depends_on,
                                 GOMP_STATIC_TDG);
      ++team->task_count;
      ++team->task_queued_count;
      ++task_to_start;

      //to_free = roots;
      roots = roots->next;
  }

  //if (to_free)
  //    free (to_free);
  roots = tmp;
  gomp_team_barrier_set_task_pending (&team->barrier);

  gomp_mutex_unlock (&team->task_lock);
  /* wake up all threads */
  if (task_to_start)
    gomp_team_barrier_wake (&team->barrier, 0);
}

/* return the number of input dependency */
char gomp_tdg_num_input_depend(struct gomp_tdg_node *tdg)
{
  return tdg->nin;
}

/* return the gomp_tdg structure of the corresponding task */
struct gomp_tdg_node *gomp_get_tdg(unsigned int task_instance_id)
{
    int tdg_index = tdg_index_lookup(task_instance_id);

    if (tdg_index == -1) {
        //dump_gomp_tdg();
        gomp_fatal("[get_tdg_index]: Task instance %u not found ",task_instance_id);
    }

    return &(tdg_g[curr_tdg_g][tdg_index]);
}

void GOMP_init_tdg(unsigned int num_tdgs, struct gomp_tdg_node **tdg, unsigned short **tdg_ins,
		   unsigned short **tdg_outs, unsigned int *tdg_ntasks, unsigned int *maxI,
		   unsigned int *maxT)
{

	if (gomp_tdg_t == TDG_DYNAMIC_SCHED_DYNAMIC)
        	gomp_fatal("[GOMP_init_tdg]: OMP_TDG is set to dynamic, but static TDG initialization has been called.\n");

	curr_tdg_g = 0;
	num_tdgs_g = num_tdgs;
	tdg_g = tdg;
	tdg_ins_g = tdg_ins;
	tdg_outs_g = tdg_outs;
	tdg_ntasks_g = tdg_ntasks;
	maxI_g = maxI;
	maxT_g = maxT;
}

void GOMP_set_tdg_id(unsigned int tdg_id)
{
	if(tdg_id > num_tdgs_g)
		gomp_fatal("tdg_id above number of tdgs");

	curr_tdg_g = tdg_id;

	int i;
	// setting tdg nodes' attributes
	for(i=0; i<tdg_ntasks_g[curr_tdg_g]; i++)
	{
		tdg_g[curr_tdg_g][i].cnt = -1;										// state not created
		tdg_g[curr_tdg_g][i].task_counter = 0;
		tdg_g[curr_tdg_g][i].task_counter_end = 0;
		tdg_g[curr_tdg_g][i].runtime_counter = 0;
		tdg_g[curr_tdg_g][i].taskpart_counter = 0;
	}
}

void
GOMP_unset_tdg_id(void)
{
    /* unused for now */
#ifdef DEBUG_GOMP_MAP
    printf("[GOMP_unset_tdg_id] - Setting back the TDG identifier %d\n", curr_tdg);
#endif
}
