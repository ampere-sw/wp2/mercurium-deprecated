#ifndef TDG_H
#define TDG_H 1

#define STATIC_TDG  (gomp_tdg_t == TDG_STATIC_SCHED_STATIC \
                    || gomp_tdg_t == TDG_STATIC_SCHED_DYNAMIC)

#define TDG_HASH_SIZE 0x10

/* testing static tdg functions */
#define _STATIC_TDG_CHECKING_ 1

struct gomp_tdg_node {
    unsigned long int id;            // Task instance ID
    struct gomp_tdg_task *task;      // pointer to actual task
    //short offin;                     // Starting position within the tomp_tdg_ins structure
    //short offout;                    // Starting position within the tomp_tdg_outs structure
    unsigned short offin;                     // Starting position within the tomp_tdg_ins structure
    unsigned short offout;                    // Starting position within the tomp_tdg_outs structure

    char nin;                        // Number of input dependencies
    char nout;                       // Number of output dependencies
    signed char cnt;                 // Number of dependent tasks:
                                     // (1) cnt == -1 Task executed or not created
                                     // (2) cnt == 0 task being executed
                                     // (3) cnt >0 task waiting for 'cnt' dependent tasks
    int map;                         // Thread assigned to the task by the static scheduler
    long task_counter;               // Private counter assigned to each task instance to compute execution time while executing in parallel
    long task_counter_end;           // Private counter assigned to each task instance that contains the final time of a task
    long runtime_counter;            // Private counter assigned to each task instance to compute the overhead of the runtime
    long taskpart_counter;           // Private counter assigned to each task part executed just before the corresponding task
    struct gomp_tdg_node *next_waiting_tdg; //to fit mcxx implementation
    unsigned int pragma_id;          // Identifier of the task contruct from which this task instance has been created
    void* data;                      // Data structure containing the parameters to be passed to the task outlined function
};

struct root_list {
    union task_union_t *task_u;
    struct root_list *next;          // pointer to the next node
};

/*  this structure is created to store the addresses of
 *  gomp_task structures to free.
 *  The reason why we use this is:
 *    1. We don't know the size of this array beforehand, we can either
 *       use dynamical reallocation array or linked-list.
 *    2. This structure could be generalized by having void *to_free. And then
 *       we would implement different free_function (to_free) according to the
 *       type of to_free, since it could contain heap memory in it, that needs
 *       to be freed separately.
 *    3. Unlike the original GOMP, we cannot free task->dependers once they are
 *       parsed (see BUILD_TDG_FROM_HASH in tdg.c). This "clumsy" mechanism
 *       prevent us from running into potential double-free erros and memory leakage.
 */
struct data_to_free {
    struct gomp_task *to_free;
    struct data_to_free *next;
};

struct gomp_data_store
{
  void (*fn) (void *);
  void *fn_data;
};

#ifdef TDG_C

typedef struct tdg_func_hash_entry
{
  void *func_pointer;
  struct tdg_func_hash_entry *next;
}tdg_hash_entry;

typedef struct tdg_func_hash_table
{
  unsigned int size;
  tdg_hash_entry *entries[];
}tdg_hash_table;

static tdg_hash_table *init_tdg_func_hash_tab()
{
  tdg_hash_table *htab = (tdg_hash_table *)malloc (sizeof (tdg_hash_table) + TDG_HASH_SIZE * sizeof(tdg_hash_entry));
  htab -> size = TDG_HASH_SIZE;  // for simplicity, the size is fixed
  memset (htab->entries, 0, htab->size * sizeof (tdg_hash_entry *));
  return htab;
}

/* simple hash function, hash(x) = (x >> 4) % TDG_HASH_SIZE:
   -> Fast: Yes
   -> Deterministic: Yes
   -> Uniform distribution: Depends on memory distribution
   -> Collision: Many. TDG_HASH_SIZE is currently small, not prime
   Collisions are handled by separate chaining */
static unsigned int tdg_hash (void *fn)
{
  uintptr_t p = (uintptr_t)fn;
  return (p >> 4) % TDG_HASH_SIZE;
}

static tdg_hash_entry *tdg_entry_create (void *fn)
{
  tdg_hash_entry *ret = (tdg_hash_entry *)malloc(sizeof(tdg_hash_entry));
  ret -> func_pointer = fn;
  ret -> next = NULL;
  return ret;
}

static bool tdg_hash_find (tdg_hash_table *htab, void *fn)
{
  bool found = false;
  unsigned int index = tdg_hash (fn);
  tdg_hash_entry *slot = htab->entries[index];
  tdg_hash_entry *new_entry;

  if (slot == 0)
  {
    new_entry = tdg_entry_create (fn);
    htab->entries[index] = new_entry;
    goto ret;
  }else{
      do{
        if (slot->func_pointer == fn)
        {
          found = true;
          goto ret;
        }
        slot = slot->next;
      }while(slot -> next != 0);

      /* if we are here, hash(entry) has a collision, and entry
         is not in the hash table. We insert it here */
      new_entry = tdg_entry_create (fn);
      slot->next = new_entry;
  }

  ret:
    return found;
}

#endif  // TDG_C

enum GOMP_dep_check
{
  GOMP_DYNAMIC_TDG = 1,
  GOMP_STATIC_TDG = 2
};

enum gomp_tdg_type
{
  TDG_DYNAMIC_SCHED_DYNAMIC,
  TDG_STATIC_SCHED_DYNAMIC,
  TDG_STATIC_SCHED_STATIC
};

extern enum gomp_tdg_type gomp_tdg_t;

/* global variables declared in tdg.c */
extern unsigned int *tdg_ntasks_g;
extern int curr_tdg_g;
extern struct gomp_tdg_node **tdg_g;


struct gomp_tdg_node *gomp_get_tdg(unsigned int);

struct gomp_tdg_task;


int gomp_update_tdg_dependency(struct gomp_tdg_task *);
int gomp_tdg_get_unresolved_dependencies (struct gomp_tdg_node *);
char gomp_tdg_num_input_depend(struct gomp_tdg_node *);
void gomp_update_tdg (struct gomp_tdg_task *, struct gomp_tdg_node *);
void GOMP_record_tdg (void (*fn) (void *), void *data);

// Dicotomic search of task instance id within the TDG structure
static inline int
tdg_index_lookup(unsigned id)
{
    int low = 0;
    int high = tdg_ntasks_g[curr_tdg_g]-1;
    int mid;

    while (low <= high)
    {
        mid = low + ((high - low) / 2);
        if (id == tdg_g[curr_tdg_g][mid].id)
          return mid;
        else if (id < tdg_g[curr_tdg_g][mid].id)
            high = mid - 1;
        else
            low = mid + 1;
    }
  return -1;
}



/* tdg.c */
void GOMP_init_tdg (unsigned int, struct gomp_tdg_node **, unsigned short **, unsigned short **, unsigned int *,
		    unsigned int *, unsigned int *);

void GOMP_exec_tdg (void (*)(void *), void *);

void GOMP_set_tdg_id (unsigned int);

void GOMP_unset_tdg_id(void);

void GOMP_fill_task_data(unsigned, void (*) (void *), void *, long, long, int);

#endif   // TDG_H
